<?php
    require_once 'vendor/autoload.php';

    use ProjetSynthese\DAO\DAOFactory;
    use ProjetSynthese\Model\Status;
    
    $stat = DAOFactory::getStatusDAO()->getById(1);
    $usr = DAOFactory::getUserDAO()->getById(1);
    $role = DAOFactory::getRoleDAO()->getById(1);
    $book1 = DAOFactory::getBookDAO()->getById(16);
    $page = DAOFactory::getPageDAO()->getById(1);
    $choice = DAOFactory::getChoiceDAO()->getById(1);
    $status = new Status(0,Status::STATUS_IN_WRITING);
    DAOFactory::getStatusDAO()->create($status);
    $book2 = DAOFactory::getBookDAO()->getByAuthor($usr)[0];
    ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<ul>
    <h1>Test</h1>
    <h2>DAO</h2>
    <h3>GetById</h3>
    <li><?php echo $stat; ?></li>
    <li><?php echo $usr; ?></li>
    <li><?php echo $role; ?></li>
    <li><?php echo $book1; ?></li>
    <li><?php echo $page; ?></li>
    <li><?php echo $choice; ?></li>
    <li><?php echo DAOFactory::getCommentDAO()->getByBook($book1)[0]?></li>
    <li><?php echo DAOFactory::getCommentDAO()->getByBook($book2)[0]?></li>
    <li><?php echo DAOFactory::getCommentDAO()->getByBook($book2)[1]?></li>
</ul>

</body>
</html>
