<?php
require '../vendor/autoload.php';
use ProjetSynthese\DAO\DAOFactory;
use ProjetSynthese\Model\Book;
use ProjetSynthese\Model\Status;

/*
 * Requête qui récupère le login
 */

$userList = DAOFactory::getUserDAO()->getAll();
$userName = ($_REQUEST["name"]);
$resListUser = array();
foreach ($userList as $user) {
    if (preg_match("/^".$userName."*/",$user->getLogin())) {
        array_push($resListUser,$user->getIdUser() );
    }
    echo json_encode($resListUser);
};

/*
 * Requête qui récupère le titre du livre
 */
$bookList = DAOFactory::getBookDAO()->getAll();
$bookTitle = ($_REQUEST["book"]);
$resListBook = array();
foreach($bookList as $book) {
    if (preg_match("/".$bookTitle."/", $book->getTitle())) {
        array_push($resListBook, $book->getIdBook());
    }
    echo json_encode($resListBook);
}