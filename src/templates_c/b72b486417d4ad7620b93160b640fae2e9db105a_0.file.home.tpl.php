<?php
/* Smarty version 3.1.33, created on 2019-06-17 01:07:24
  from '/opt/lampp/htdocs/www/projetsynthese/src/templates/page/home.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d06cbac19f536_58766278',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b72b486417d4ad7620b93160b640fae2e9db105a' => 
    array (
      0 => '/opt/lampp/htdocs/www/projetsynthese/src/templates/page/home.tpl',
      1 => 1560726441,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d06cbac19f536_58766278 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14939462665d06cbac192ca2_80283658', "css");
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19520702355d06cbac199d36_13638252', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16843492235d06cbac19e6f4_63991064', 'script');
?>




<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, "../canvas/model.tpl");
}
/* {block "css"} */
class Block_14939462665d06cbac192ca2_80283658 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'css' => 
  array (
    0 => 'Block_14939462665d06cbac192ca2_80283658',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['cssDir']->value;?>
home.css"/><?php
}
}
/* {/block "css"} */
/* {block 'content'} */
class Block_19520702355d06cbac199d36_13638252 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_19520702355d06cbac199d36_13638252',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <div class="main header-main">
        <h1> Bienvenue <?php echo $_smarty_tpl->tpl_vars['login']->value;?>
 </h1>
    </div>
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"
                                 data-interval="8000">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                                </ol>

                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    <div class="item active" id="ecrivain">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 hidden-xs">
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div>
                                                    <h2>Vous voulez créer votre propre histoire ?</h2>
                                                    <p> Ce site n'est pas un simple site de lecture en ligne, il vous
                                                        permets de chosir comment votre aventure va évoluer à travers
                                                        des choix pouvant changer la destinée
                                                        des personnages dans vos histoires.</p>

                                                    <p>L'aventure à portée de main. Voici l'occasion de devenir le plus
                                                        grand des héros ou le plus infâme démon... Vous seul déciderez
                                                        de la route à suivre, des risques à courir et des créatures à
                                                        combattre. Bonne chance...
                                                    </p>
                                                    <p>Notre outil de création et d'édition de livre est à votre
                                                        disposition, ce bouton ci-dessous vous permettra de créer votre
                                                        propre univers, n'attendez pas ! lancez-vous !!!</p>
                                                    <h3>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['srcDir']->value;?>
creator">
                                                        <button class="btnCar btn btn-primary btn-lg">Créer votre
                                                            aventure <span class="glyphicon glyphicon-pencil"></span>
                                                        </button></a>
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item" id="lecture">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 hidden-xs">
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div>
                                                    <h2>Vous voulez vous lancez dans une aventure ?</h2>
                                                    <p> Des aventures sont disponibles sur notre site, lancez vous dans
                                                        une aventure existante ou créer-une qui vous convient</p>

                                                    <p>Votre destinée vous attends, vous seul avez le choix de sauver ce
                                                        monde, ou bien de laisser libre cours à votre rage et de le
                                                        réduire en cendres.</p>
                                                    <p>Tous les livres sur ce site sont disponibles gratuitement,
                                                        profitez en !</p>
                                                    <h3>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['srcDir']->value;?>
category">
                                                        <button class="btnCar btn btn-primary btn-lg">Commencer votre
                                                            aventure <span class="glyphicon glyphicon-book"></span>
                                                        </button></a>
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item" id="tribunal">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 hidden-xs">
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div>
                                                    <h2>Un avis ?</br> Noter, commenter ou voter !</h2>
                                                    <p>Une remarque ? (constructive de préférence), n'hésitez pas à en
                                                        en faire part pour permettre de s'améliorer. </p>

                                                    <p>Echanger avec la communauté et débattez sur vos livres et
                                                        passages favoris ou moins appréciés.</p>
                                                    <p>Un système de notation est également présent sur notre site afin
                                                        de voir plus facilement les autres avis sur une oeuvre ou de
                                                        donner le votre.</p>
                                                    <h3>
                                                        <button class="btnCar btn btn-primary btn-lg">S'exprimer <span
                                                                    class="glyphicon glyphicon-thumbs-up"></span>
                                                        </button>
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item" id="support">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 hidden-xs">
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div>
                                                    <h2>Nouveautés</h2>
                                                    <p> Ce site est en constante évolution, nos développeurs travaillent
                                                        sur des fonctionnalités qui seront prochainement déployés pour
                                                        que votre expérience s'améliore encore.</p>

                                                    <p>Si vous avez des remarques sur d'éventuelles amélioration ou
                                                        certains points qui vous ont plus ou que vous n'avez pas aimé
                                                        faites nous en part ! </p>
                                                    <p>Eventuelle redirection ou lien vers nous contacter en bas de page
                                                        ?</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <!-- Controls -->
                                <a class="left carousel-control" href="#carousel-example-generic" role="button"
                                   data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#carousel-example-generic" role="button"
                                   data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                                                                        <!--
                        <h1>Nouveautés</h1>
                        <p> Dernières notes de patch</p>
                        <ul>
                            <li>Patch 8.2 : Amélioration du CSS</li>
                            <li>Patch 7.0.1 : Mise en place du Smarty</li>
                            <li>05/02/2019 - Mise à jour d'équilibrage et corrections de bugs</li>
                            <p>C'est l'occasion d'apporter des changements majeurs à Aatrox et à Tahm Kench, des
                                champions qui comptaient sur certains éléments de leur kit pour
                                avoir l'ascendant dans certaines situations de jeu, mais qui souffraient beaucoup dans
                                les autres cas. Soraka est également de la fête, avec des changements
                                qui visent à refaire d'elle un support qui s'occupe essentiellement de ses alliés. Nous
                                avons aussi mis à jour les effets visuels d'un petit groupe de
                                champions, pour leur donner un look plus moderne, à la hauteur ce qui est fait pour les
                                nouveaux champions.
                            </p>
                            <li>Patch 7.0.1 : Mise en place du Smarty</li>
                            <p>C'est l'occasion d'apporter des changements majeurs à Aatrox et à Tahm Kench, des
                                champions qui comptaient sur certains éléments de leur kit pour
                                avoir l'ascendant dans certaines situations de jeu, mais qui souffraient beaucoup dans
                                les autres cas. Soraka est également de la fête, avec des changements
                                qui visent à refaire d'elle un support qui s'occupe essentiellement de ses alliés. Nous
                                avons aussi mis à jour les effets visuels d'un petit groupe de
                                champions, pour leur donner un look plus moderne, à la hauteur ce qui est fait pour les
                                nouveaux champions.
                            </p>
                        </ul>
                        -->
                                                                                                
                                                                                                                                                                                                                                                                                                                                                                    </div>

                    <div id="add-books" class="row">

                    </div>

                </div>
            </div>
        </div>
    </div>
<?php
}
}
/* {/block 'content'} */
/* {block 'script'} */
class Block_16843492235d06cbac19e6f4_63991064 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'script' => 
  array (
    0 => 'Block_16843492235d06cbac19e6f4_63991064',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['jsDir']->value;?>
home.js"><?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'script'} */
}
