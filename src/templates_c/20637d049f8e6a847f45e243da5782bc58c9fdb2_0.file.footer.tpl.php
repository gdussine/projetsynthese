<?php
/* Smarty version 3.1.33, created on 2019-06-15 03:25:08
  from 'C:\xampp\htdocs\www\projetsynthese\src\templates\element\footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d0448f44be8b2_90181540',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '20637d049f8e6a847f45e243da5782bc58c9fdb2' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\projetsynthese\\src\\templates\\element\\footer.tpl',
      1 => 1560518161,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d0448f44be8b2_90181540 (Smarty_Internal_Template $_smarty_tpl) {
?><footer class="page-footer font-small blue pt-4">
    <div class="retour-en-haut text-center py-3">
    </div>
    <div class="footer-content container-fluid text-center text-md-left">
        <div class="row">
            <div class="col-md-6 mt-md-0 mt-3">
                <h5 class="text-uppercase">Livres dont vous êtes le héros</h5>
                <p class="text-description">Les « livres dont vous êtes le héros » sont un genre de romans qui ont
                    la
                    particularité d’être interactif, le lecteur
                    peut effectuer des choix pendant la lecture qui changent le déroulement de l’histoire.
                </p>
            </div>
            <div class="col-md-3 mb-md-0 mb-3">
                <h5 class="text-uppercase">Suivez-nous</h5>

                <ul class="list-unstyled">
                    <li>
                        <a href="#!">Facebook</a>
                    </li>
                    <li>
                        <a href="#!">Twitter</a>
                    </li>
                    <li>
                        <a href="#!">Instagram</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 mb-md-0 mb-3">
                <h5 class="text-uppercase">Besoin d'aide</h5>

                <ul class="list-unstyled">
                    <li>
                        <a href="#!">Qui somme nous ?</a>
                    </li>
                    <li>
                        <a href="#!">FAQ</a>
                    </li>
                    <li>
                        <a href="#!">contactez-nous</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="footer-copyright text-center py-3">© 2019 Copyright:
        <a href="https://mdbootstrap.com/education/bootstrap/"> Livresdontvousetesleheros.com</a>
    </div>
</footer>
<?php }
}
