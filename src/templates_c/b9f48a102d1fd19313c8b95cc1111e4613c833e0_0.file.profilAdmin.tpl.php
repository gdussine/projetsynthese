<?php
/* Smarty version 3.1.33, created on 2019-06-16 17:29:18
  from 'C:\xampp\htdocs\www\projetsynthese\src\templates\page\profilAdmin.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d06604eae51e5_33718950',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b9f48a102d1fd19313c8b95cc1111e4613c833e0' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\projetsynthese\\src\\templates\\page\\profilAdmin.tpl',
      1 => 1560694515,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d06604eae51e5_33718950 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17093365425d06604eadfec1_19507474', 'css');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6013467575d06604eae3c39_61221016', 'content');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2068788885d06604eae47d3_27227597', 'script');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "../canvas/model.tpl");
}
/* {block 'css'} */
class Block_17093365425d06604eadfec1_19507474 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'css' => 
  array (
    0 => 'Block_17093365425d06604eadfec1_19507474',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <link rel="stylesheet" media="screen and (min-width:721px)" href="<?php echo $_smarty_tpl->tpl_vars['cssDir']->value;?>
profilAdmin.css"/>
<?php
}
}
/* {/block 'css'} */
/* {block 'content'} */
class Block_6013467575d06604eae3c39_61221016 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_6013467575d06604eae3c39_61221016',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <section class="titre">
        <div class="container">
            <div class="col-lg-12">
                <h3 class="text-center">Bienvenue <?php echo $_smarty_tpl->tpl_vars['username']->value;?>
 !</h3>
                <p><?php echo $_smarty_tpl->tpl_vars['countBook']->value;?>
 livres écris</p>
            </div>
        </div>
    </section>
    <section class="action">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="fenetre">
                        <h3 class="text-center">Supprimer un livre</h3>

                        <ul>
                            <li><p class="text-center">Vous souhaitez supprimer un livre ?</p></li>
                            <li><input id="inputBook" class="text-center"  type="text" placeholder="Titre du livre"></li>
                        </ul>

                    </div>
                </div>


                <div class="col-lg-offset-2 col-lg-5">
                    <div class="fenetre">
                        <h3 class="text-center">Supprimer un utilisateur</h3>
                        <ul>
                            <li><p class="text-center">Vous souhaitez supprimer un utilisateur ?</p></li>
                            <li><input id="inputUser" class="text-center" onkeyup="chooseUser(event)" type="text" placeholder="Nom de l'utilisateur"></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <table>
                        <tr>
                            <td class="name">Lucas</td>
                            <td class="mail">jps@gmail.com</td>
                            <td class="delete">&times;</td>
                        </tr>

                        <tr>
                            <td class="name">Lucas</td>
                            <td class="mail">jps@gmail.com</td>
                            <td class="delete">&times;</td>
                        </tr>

                        <tr>
                            <td class="name">Lucas</td>
                            <td class="mail">jps@gmail.com</td>
                            <td class="delete">&times;</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
<?php
}
}
/* {/block 'content'} */
/* {block 'script'} */
class Block_2068788885d06604eae47d3_27227597 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'script' => 
  array (
    0 => 'Block_2068788885d06604eae47d3_27227597',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['jsDir']->value;?>
profilAdmin.js"><?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'script'} */
}
