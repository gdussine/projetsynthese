<?php
/* Smarty version 3.1.33, created on 2019-06-15 03:03:29
  from 'C:\xampp\htdocs\www\projetsynthese\src\templates\canvas\skeleton.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d0443e1c8b4f7_42915640',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '331389a5f378b10f47ca3231d97eaf069117ec49' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\projetsynthese\\src\\templates\\canvas\\skeleton.tpl',
      1 => 1560560555,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d0443e1c8b4f7_42915640 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18742596365d0443e1c88316_87172808', 'head');
?>

</head>
<body>
<main>
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11980003115d0443e1c89f98_25654179', 'navbar');
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10072128955d0443e1c8a5a2_37160124', 'content');
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8582962815d0443e1c8aaf4_88873460', 'footer');
?>

</main>
<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3010375235d0443e1c8b034_09347885', 'script');
?>

</body>
</html><?php }
/* {block 'head'} */
class Block_18742596365d0443e1c88316_87172808 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'head' => 
  array (
    0 => 'Block_18742596365d0443e1c88316_87172808',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'head'} */
/* {block 'navbar'} */
class Block_11980003115d0443e1c89f98_25654179 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'navbar' => 
  array (
    0 => 'Block_11980003115d0443e1c89f98_25654179',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'navbar'} */
/* {block 'content'} */
class Block_10072128955d0443e1c8a5a2_37160124 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_10072128955d0443e1c8a5a2_37160124',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_8582962815d0443e1c8aaf4_88873460 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_8582962815d0443e1c8aaf4_88873460',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'footer'} */
/* {block 'script'} */
class Block_3010375235d0443e1c8b034_09347885 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'script' => 
  array (
    0 => 'Block_3010375235d0443e1c8b034_09347885',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'script'} */
}
