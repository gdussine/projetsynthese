<?php
/* Smarty version 3.1.33, created on 2019-06-16 17:30:00
  from '/opt/lampp/htdocs/www/projetsynthese/src/templates/page/profilAdmin.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d066078c81f57_48427295',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f4915462d695de2f700963210359428e486a033c' => 
    array (
      0 => '/opt/lampp/htdocs/www/projetsynthese/src/templates/page/profilAdmin.tpl',
      1 => 1560694686,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d066078c81f57_48427295 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20302305155d066078c448f8_71992164', 'css');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16845010505d066078c77b13_14246129', 'content');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12132853705d066078c7f753_23062028', 'script');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "../canvas/model.tpl");
}
/* {block 'css'} */
class Block_20302305155d066078c448f8_71992164 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'css' => 
  array (
    0 => 'Block_20302305155d066078c448f8_71992164',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <link rel="stylesheet" media="screen and (min-width:721px)" href="<?php echo $_smarty_tpl->tpl_vars['cssDir']->value;?>
profilAdmin.css"/>
<?php
}
}
/* {/block 'css'} */
/* {block 'content'} */
class Block_16845010505d066078c77b13_14246129 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_16845010505d066078c77b13_14246129',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <section class="titre">
        <div class="container">
            <div class="col-lg-12">
                <h3 class="text-center">Bienvenue <?php echo $_smarty_tpl->tpl_vars['username']->value;?>
 !</h3>
                <p><?php echo $_smarty_tpl->tpl_vars['countBook']->value;?>
 livres écris</p>
            </div>
        </div>
    </section>
    <section class="action">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="fenetre">
                        <h3 class="text-center">Supprimer un livre</h3>

                        <ul>
                            <li><p class="text-center">Vous souhaitez supprimer un livre ?</p></li>
                            <li><input id="inputBook" class="text-center"  type="text" placeholder="Titre du livre"></li>
                        </ul>

                    </div>
                </div>


                <div class="col-lg-offset-2 col-lg-5">
                    <div class="fenetre">
                        <h3 class="text-center">Supprimer un utilisateur</h3>
                        <ul>
                            <li><p class="text-center">Vous souhaitez supprimer un utilisateur ?</p></li>
                            <li><input id="inputUser" class="text-center" onkeyup="chooseUser(event)" type="text" placeholder="Nom de l'utilisateur"></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <table>
                        <tr>
                            <td class="name">Lucas</td>
                            <td class="mail">jps@gmail.com</td>
                            <td class="delete">&times;</td>
                        </tr>

                        <tr>
                            <td class="name">Lucas</td>
                            <td class="mail">jps@gmail.com</td>
                            <td class="delete">&times;</td>
                        </tr>

                        <tr>
                            <td class="name">Lucas</td>
                            <td class="mail">jps@gmail.com</td>
                            <td class="delete">&times;</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
<?php
}
}
/* {/block 'content'} */
/* {block 'script'} */
class Block_12132853705d066078c7f753_23062028 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'script' => 
  array (
    0 => 'Block_12132853705d066078c7f753_23062028',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['jsDir']->value;?>
profilAdmin.js"><?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'script'} */
}
