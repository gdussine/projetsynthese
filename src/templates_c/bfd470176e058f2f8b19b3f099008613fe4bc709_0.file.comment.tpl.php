<?php
/* Smarty version 3.1.33, created on 2019-06-17 01:12:03
  from 'C:\xampp\htdocs\www\projetsynthese\src\templates\page\comment.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d06ccc3efcd23_18650118',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bfd470176e058f2f8b19b3f099008613fe4bc709' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\projetsynthese\\src\\templates\\page\\comment.tpl',
      1 => 1560726707,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d06ccc3efcd23_18650118 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21335784475d06ccc3ef2db8_84258446', 'css');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3903181465d06ccc3ef9f29_74999457', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7971811885d06ccc3efc4d8_51236219', 'script');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "../canvas/model.tpl");
}
/* {block 'css'} */
class Block_21335784475d06ccc3ef2db8_84258446 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'css' => 
  array (
    0 => 'Block_21335784475d06ccc3ef2db8_84258446',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['cssDir']->value;?>
comment.css">
    <?php echo '<script'; ?>
 src="https://rawgit.com/jackmoore/autosize/master/dist/autosize.min.js"><?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'css'} */
/* {block 'content'} */
class Block_3903181465d06ccc3ef9f29_74999457 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_3903181465d06ccc3ef9f29_74999457',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div id="idUser" data-idUser=<?php echo $_smarty_tpl->tpl_vars['idUser']->value;?>
 hidden></div>
<div id="idBook" data-idBook=<?php echo $_smarty_tpl->tpl_vars['idBook']->value;?>
 hidden></div>
<div class="container mainPage">
    <div class="row">
        <div class="col-sm-12 menuLecture">
            <h1><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</h1>
        </div>
        <div class="col-sm-12 itemLecture">
            <ul>
                <a href='/www/projetsynthese/src/read/<?php echo $_smarty_tpl->tpl_vars['idBook']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['FirstPageFromBook']->value;?>
'><li>Lecture</li></a>
                <li>Résumé</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <div class="row">
                <div class="text-center cover col-sm-12">
                    <img class="img-responsive coverBook" src='../view/upload/<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
'>
                </div>
                <div class="col-sm-12">
                    <span>Auteur : <?php echo $_smarty_tpl->tpl_vars['author']->value;?>
</span>
                </div>
                <div class="col-sm-12">
                    <span class="etoile">‎★‎★‎★‎★‎★</span>
                </div>
            </div>
        </div>

        <div class="col-sm-8">
            <h2>Commentaires</h2>
            <div class="row">
                
                 <div class="add-comment col-md-12">
                    <div class="profil">
                    <span><?php echo $_smarty_tpl->tpl_vars['login']->value;?>
</span> 
                    </div>
                    <div class="md-form">
                        <p <?php echo $_smarty_tpl->tpl_vars['showInverse']->value;?>
>Vous devez être <a onclick="signIn(event)">connecté</a> pour pouvoir poster un commentaire...</p>
                        <textarea <?php echo $_smarty_tpl->tpl_vars['show']->value;?>
 placeholder="Ajouter un commentaire..." name="comment" form="form" class="md-textarea form-control" rows="1"></textarea>
                    </div>
                    <div class="formulaire">
                    <form id="form" style="float: right;">
                       <button type="button" class="btn-ajouter-commentaire btn btn-primary">Ajouter un commentaire</button>
                    </form>
                       <button class="btn-annuler btn btn-light" style="float: right;" >Annuler</button>
                    </div>
                    
                </div>
            </div>

        </div>
    </div>
</div>
<?php
}
}
/* {/block 'content'} */
/* {block 'script'} */
class Block_7971811885d06ccc3efc4d8_51236219 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'script' => 
  array (
    0 => 'Block_7971811885d06ccc3efc4d8_51236219',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['jsDir']->value;?>
comment.js"><?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'script'} */
}
