<?php
/* Smarty version 3.1.33, created on 2019-06-16 22:08:52
  from '/opt/lampp/htdocs/www/projetsynthese/src/templates/page/category.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d06a1d483c5f0_32948321',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '63c3fc66fdd195ec3351a6d7af800caae2ea9717' => 
    array (
      0 => '/opt/lampp/htdocs/www/projetsynthese/src/templates/page/category.tpl',
      1 => 1560715164,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d06a1d483c5f0_32948321 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>
    
    

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14456510385d06a1d48310a9_39997259', 'css');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3410826295d06a1d4837441_95328842', "content");
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7681829125d06a1d4839e96_58529714', 'script');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, "../canvas/model.tpl");
}
/* {block 'css'} */
class Block_14456510385d06a1d48310a9_39997259 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'css' => 
  array (
    0 => 'Block_14456510385d06a1d48310a9_39997259',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['cssDir']->value;?>
category.css">
<?php
}
}
/* {/block 'css'} */
/* {block "content"} */
class Block_3410826295d06a1d4837441_95328842 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_3410826295d06a1d4837441_95328842',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

     <div class="main header-main">
        <h1> CATÉGORIE </h1>
    </div>
    <div class="main">
    <div class="container">
        <div class="row-main row-left">
            <div class="col-md-2 no-float main-left">
                <div class="main-left-livre div-left">
                    <div class="categ-livre categ-left">LIVRES</div>
                    <div class="slide-left">
                        <ul class="ul-left ul-livre">
                            <li><a href="">Nouveautés</a></li>
                            <li><a href="">Tendances</a></li>
                            <li><a href="">Comedie</a></li>
                            <li><a href="">Drame</a></li>
                            <li><a href="">Action</a></li>
                            <li><a href="">Aventures</a></li>
                            <li><a href="">Thriller</a></li>
                            <li><a href="">Fantastique</a></li>
                            <li><a href="">Science-fiction</a></li>
                            <li><a href="">Horreur</a></li>
                        </ul>
                    </div>
                </div>
                <div class="main-left-manga div-left">
                    <div class="categ-manga categ-left">MANGA</div>
                    <div class="slide-left">
                        <ul class="ul-left ul-manga">
                            <li><a href="">Nouveautés</a></li>
                            <li><a href="">Tendances</a></li>
                            <li><a href="">Comedie</a></li>
                            <li><a href="">Drame</a></li>
                            <li><a href="">Action</a></li>
                            <li><a href="">Aventures</a></li>
                            <li><a href="">Thriller</a></li>
                            <li><a href="">Fantastique</a></li>
                            <li><a href="">Science-fiction</a></li>
                            <li><a href="">Horreur</a></li>
                        </ul>
                    </div>
                </div>
                <div class="main-left-comics div-left">
                    <div class="categ-comics categ-left">COMICS</div>
                    <div class="slide-left">
                        <ul class="ul-left ul-comics">
                            <li><a href="">Nouveautés</a></li>
                            <li><a href="">Tendances</a></li>
                            <li><a href="">Comedie</a></li>
                            <li><a href="">Drame</a></li>
                            <li><a href="">Action</a></li>
                            <li><a href="">Aventures</a></li>
                            <li><a href="">Thriller</a></li>
                            <li><a href="">Fantastique</a></li>
                            <li><a href="">Science-fiction</a></li>
                            <li><a href="">Horreur</a></li>
                        </ul>
                    </div>
                </div>
                <div class="main-left-bds div-left">
                    <div class="categ-bds categ-left">BDS</div>
                    <div class="slide-left">
                        <ul class="ul-left ul-bds">
                            <li><a href="">Nouveautés</a></li>
                            <li><a href="">Tendances</a></li>
                            <li><a href="">Comedie</a></li>
                            <li><a href="">Drame</a></li>
                            <li><a href="">Action</a></li>
                            <li><a href="">Aventures</a></li>
                            <li><a href="">Thriller</a></li>
                            <li><a href="">Fantastique</a></li>
                            <li><a href="">Science-fiction</a></li>
                            <li><a href="">Horreur</a></li>
                        </ul>
                    </div>
                </div>
                <div class="slogan-left categ-left div-left">Lire, écrire, parler, voyager</div>
            </div>
            
            <div class="col-md-10 no-float main-right">
                
                <div class="row">
                    <div class="col-md-12  vcenter main-right-top">
                        <div class="button-toggle">
                            <a class="animer" href="#">
                                <i class="glyphicon glyphicon-triangle-left" aria-hidden="true"></i>
                            </a></div>
                        <p>
                            <a href="#a">A</a> - <a href="#b">B</a> - <a href="#c">C</a> - <a href="#d">D</a> - <a
                                href="#e">E</a> -
                            <a href="#f">F</a> -
                            <a href="#g">G</a> - <a href="#h">H</a> - <a href="#i">I</a> - <a href="#j">J</a> - <a
                                href="#k">K</a> -
                            <a href="#l">L</a> -
                            <a href="#m">M</a> - <a href="#n">N</a> - <a href="#o">O</a> - <a href="#p">P</a> - <a
                                href="#q">Q</a> -
                            <a href="#r">R</a> -
                            <a href="#s">S</a> - <a href="#t">T</a> - <a href="#u">U</a> - <a href="#v">V</a> - <a
                                href="#w">W</a> -
                            <a href="#x">X</a> -
                            <a href="#y">Y</a> - <a href="#z">Z</a>
                        </p>
                    </div>
                </div>

                    <div id="add-books" class="container-livres row">
                    

                </div>
            </div>
        </div>
    </div>
    </div>
    <?php
}
}
/* {/block "content"} */
/* {block 'script'} */
class Block_7681829125d06a1d4839e96_58529714 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'script' => 
  array (
    0 => 'Block_7681829125d06a1d4839e96_58529714',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['jsDir']->value;?>
category.js"><?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'script'} */
}
