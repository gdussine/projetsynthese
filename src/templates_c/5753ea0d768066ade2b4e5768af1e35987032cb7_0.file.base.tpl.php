<?php
/* Smarty version 3.1.33, created on 2019-06-15 02:27:52
  from '/opt/lampp/htdocs/www/projetsynthese/src/templates/canvas/base.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d043b8825e4a1_32019100',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5753ea0d768066ade2b4e5768af1e35987032cb7' => 
    array (
      0 => '/opt/lampp/htdocs/www/projetsynthese/src/templates/canvas/base.tpl',
      1 => 1560557096,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d043b8825e4a1_32019100 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16764829835d043b8825cd43_96326095', 'head');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, "../canvas/skeleton.tpl");
}
/* {block 'css'} */
class Block_19760608055d043b8825d7f1_83288885 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'css'} */
/* {block 'head'} */
class Block_16764829835d043b8825cd43_96326095 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'head' => 
  array (
    0 => 'Block_16764829835d043b8825cd43_96326095',
  ),
  'css' => 
  array (
    0 => 'Block_19760608055d043b8825d7f1_83288885',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <title>GameBook</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <link href="https://fonts.googleapis.com/css?family=Berkshire+Swash&display=swap" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
    <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"><?php echo '</script'; ?>
>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19760608055d043b8825d7f1_83288885', 'css', $this->tplIndex);
?>

<?php
}
}
/* {/block 'head'} */
}
