<?php
/* Smarty version 3.1.33, created on 2019-06-20 00:05:55
  from 'C:\xampp\htdocs\www\projetsynthese\src\templates\page\404.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d0ab1c39f9a48_08404044',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7a7622708271e509a9cfd2ec3f388ba86807dcbc' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\projetsynthese\\src\\templates\\page\\404.tpl',
      1 => 1560981954,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d0ab1c39f9a48_08404044 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14626328735d0ab1c39f4a63_66635235', 'css');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_478584595d0ab1c39f88d6_49924485', 'content');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "../canvas/model.tpl");
}
/* {block 'css'} */
class Block_14626328735d0ab1c39f4a63_66635235 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'css' => 
  array (
    0 => 'Block_14626328735d0ab1c39f4a63_66635235',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['cssDir']->value;?>
erreur.css">
<?php
}
}
/* {/block 'css'} */
/* {block 'content'} */
class Block_478584595d0ab1c39f88d6_49924485 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_478584595d0ab1c39f88d6_49924485',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <div class="col-sm-12 menuLecture">
            <h1 class="NombresDeLivres"></h1>
        </div>
    <div class="main">

        <div class="row row-erreur">
            <div class="col-xs-2 col-md-3"></div>


            <div class="col-xs-4 col-md-3">

                <div class="erreur">
                    <h1>ERREUR <?php echo $_smarty_tpl->tpl_vars['numErr']->value;?>
</h1>
                    <p><?php echo $_smarty_tpl->tpl_vars['msgErr']->value;?>
</p>
                    <p>Retournez sur <a href="<?php echo $_smarty_tpl->tpl_vars['srcDir']->value;?>
home">la page d'accueil du site</a></p>
                </div>
            </div>
            <div class="col-xs-2 col-md-1">

                <div class="img-hero">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['imgDir']->value;?>
background/hero.png" alt="hero">
                </div>
            </div>
            <div class="col-xs-4 col-md-5"></div>
        </div>
    </div>
<?php
}
}
/* {/block 'content'} */
}
