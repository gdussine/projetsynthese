<?php
/* Smarty version 3.1.33, created on 2019-06-16 16:04:05
  from '/opt/lampp/htdocs/www/projetsynthese/src/templates/page/404.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d064c55322f38_52760659',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1f97eb237594960753e5b1bd736f75ff439f0bae' => 
    array (
      0 => '/opt/lampp/htdocs/www/projetsynthese/src/templates/page/404.tpl',
      1 => 1560557096,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d064c55322f38_52760659 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5587347595d064c5531c916_21921278', 'css');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1079166425d064c55320773_53345492', 'content');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "../canvas/model.tpl");
}
/* {block 'css'} */
class Block_5587347595d064c5531c916_21921278 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'css' => 
  array (
    0 => 'Block_5587347595d064c5531c916_21921278',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['cssDir']->value;?>
erreur.css">
<?php
}
}
/* {/block 'css'} */
/* {block 'content'} */
class Block_1079166425d064c55320773_53345492 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_1079166425d064c55320773_53345492',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <div class="main">

        <div class="row">
            <div class="col-xs-2 col-md-3"></div>


            <div class="col-xs-4 col-md-3">

                <div class="erreur">
                    <h3>ERREUR <?php echo $_smarty_tpl->tpl_vars['numErr']->value;?>
</h3>
                    <p><?php echo $_smarty_tpl->tpl_vars['msgErr']->value;?>
</p>
                    <p>Retournez sur <a href="#">la page d'accueil du site</a></p>
                </div>
            </div>
            <div class="col-xs-2 col-md-1">

                <div class="img-hero">
                    <img src="<?php echo $_smarty_tpl->tpl_vars['imgDir']->value;?>
hero.png" alt="hero">
                </div>
            </div>
            <div class="col-xs-4 col-md-5"></div>
        </div>
    </div>
<?php
}
}
/* {/block 'content'} */
}
