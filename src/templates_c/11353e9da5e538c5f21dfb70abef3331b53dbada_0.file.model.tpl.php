<?php
/* Smarty version 3.1.33, created on 2019-06-15 03:25:08
  from 'C:\xampp\htdocs\www\projetsynthese\src\templates\canvas\model.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d0448f43da946_14474542',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '11353e9da5e538c5f21dfb70abef3331b53dbada' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\projetsynthese\\src\\templates\\canvas\\model.tpl',
      1 => 1560518161,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../element/navBar.tpl' => 1,
    'file:../element/footer.tpl' => 1,
  ),
),false)) {
function content_5d0448f43da946_14474542 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13733453805d0448f43d0e74_73717262', 'css');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6044409685d0448f43d4af9_45690002', 'navbar');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2687458715d0448f43d89e8_90444264', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16430114505d0448f43d8f35_16272632', 'footer');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_821073625d0448f43da182_82697277', 'script');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, "../canvas/base.tpl");
}
/* {block 'css'} */
class Block_13733453805d0448f43d0e74_73717262 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'css' => 
  array (
    0 => 'Block_13733453805d0448f43d0e74_73717262',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['cssDir']->value;?>
navBar.css">
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['cssDir']->value;?>
footer.css">
<?php
}
}
/* {/block 'css'} */
/* {block 'navbar'} */
class Block_6044409685d0448f43d4af9_45690002 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'navbar' => 
  array (
    0 => 'Block_6044409685d0448f43d4af9_45690002',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../element/navBar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
/* {/block 'navbar'} */
/* {block 'content'} */
class Block_2687458715d0448f43d89e8_90444264 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_2687458715d0448f43d89e8_90444264',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_16430114505d0448f43d8f35_16272632 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_16430114505d0448f43d8f35_16272632',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../element/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
/* {/block 'footer'} */
/* {block 'script'} */
class Block_821073625d0448f43da182_82697277 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'script' => 
  array (
    0 => 'Block_821073625d0448f43da182_82697277',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['jsDir']->value;?>
navBar.js"><?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'script'} */
}
