<?php
/* Smarty version 3.1.33, created on 2019-06-20 00:19:42
  from 'C:\xampp\htdocs\www\projetsynthese\src\templates\page\writer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d0ab4fe5d10e6_60366042',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '909c9401c8093d79efefc7f9bb0f51b63394e11e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\projetsynthese\\src\\templates\\page\\writer.tpl',
      1 => 1560981486,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d0ab4fe5d10e6_60366042 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11922358115d0ab4fe5cc178_91100729', 'css');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9083410835d0ab4fe5cffb7_21664918', "content");
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9704848655d0ab4fe5d0a49_49950818', 'script');
?>


<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, "../canvas/model.tpl");
}
/* {block 'css'} */
class Block_11922358115d0ab4fe5cc178_91100729 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'css' => 
  array (
    0 => 'Block_11922358115d0ab4fe5cc178_91100729',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['cssDir']->value;?>
writer.css">
<?php
}
}
/* {/block 'css'} */
/* {block "content"} */
class Block_9083410835d0ab4fe5cffb7_21664918 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_9083410835d0ab4fe5cffb7_21664918',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <div class="stockage" data-book="<?php echo $_smarty_tpl->tpl_vars['idBook']->value;?>
" hidden></div>
       <div class="col-sm-12 menuLecture">
            <h1 class="NombresDeLivres">Écriture</h1>
        </div>
    <div class="main">
    <div class="container mainPage">
        <div class="row-main">
            <div class="col-md-2 no-float main-left">
                <div class="text-view">

                    <div id="div-view" class="row">

                        <button type="button" class="btn-ajouter-page btn btn-primary btn-md btn-block"
                                data-field="div-view"
                                data-summernote="summernote"><span class="glyphicon glyphicon-plus"></span></button>

                    </div>
                </div>

            </div>

            <div class="col-md-8 no-float" id="main-center">
                <div id="edit-text">
                                      <div class="editeur-text" id="summernote"></div>
                    <input class="editeur-text" id="choix-1" type="text" placeholder="Entrez le choix n°1...">
                    <select id="page-choix-1"></select>
                    <input class="editeur-text" id="choix-2" type="text" placeholder="Entrez le choix n°2...">
                    <select id="page-choix-2"></select>
                    <input class="editeur-text" id="choix-3" type="text" placeholder="Entrez le choix n°3...">
                    <select id="page-choix-3"></select>
                    <input class="editeur-text" id="choix-4" type="text" placeholder="Entrez le choix n°4...">
                    <select id="page-choix-4"></select>
                </div>
            </div>
            <div class="col-md-2 no-float main-right">
                <div class="text-view">
                    <div class="row div-btn">
                        <button type="button" class="btn-enregistrer-page btn btn-primary btn-md btn-block">Enregistrer
                            la
                            page
                        </button>
                        <button type="button" class="btn-supprimer-page btn btn-danger btn-md btn-block">Supprimer la
                            page
                        </button>
                        <button type="button" class="btn btn-success btn-md btn-block">Enregistrer le livre</button>

                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>
<?php
}
}
/* {/block "content"} */
/* {block 'script'} */
class Block_9704848655d0ab4fe5d0a49_49950818 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'script' => 
  array (
    0 => 'Block_9704848655d0ab4fe5d0a49_49950818',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['jsDir']->value;?>
writer.js"><?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'script'} */
}
