<?php
/* Smarty version 3.1.33, created on 2019-06-15 03:25:08
  from 'C:\xampp\htdocs\www\projetsynthese\src\templates\canvas\base.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d0448f4440bb9_67734163',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8cb1cb7be0dd24fe4386dd8f6183e9e946cce9ea' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\projetsynthese\\src\\templates\\canvas\\base.tpl',
      1 => 1560518161,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d0448f4440bb9_67734163 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4863486285d0448f443fe34_97095685', 'head');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, "../canvas/skeleton.tpl");
}
/* {block 'css'} */
class Block_10801211575d0448f4440369_38857691 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'css'} */
/* {block 'head'} */
class Block_4863486285d0448f443fe34_97095685 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'head' => 
  array (
    0 => 'Block_4863486285d0448f443fe34_97095685',
  ),
  'css' => 
  array (
    0 => 'Block_10801211575d0448f4440369_38857691',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <title>GameBook</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <link href="https://fonts.googleapis.com/css?family=Berkshire+Swash&display=swap" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
    <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"><?php echo '</script'; ?>
>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10801211575d0448f4440369_38857691', 'css', $this->tplIndex);
?>

<?php
}
}
/* {/block 'head'} */
}
