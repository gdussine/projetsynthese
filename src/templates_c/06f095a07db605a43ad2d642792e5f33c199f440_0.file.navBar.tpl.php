<?php
/* Smarty version 3.1.33, created on 2019-06-17 23:19:47
  from 'C:\xampp\htdocs\www\projetsynthese\src\templates\element\navBar.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d0803f3c82f98_80115650',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '06f095a07db605a43ad2d642792e5f33c199f440' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\projetsynthese\\src\\templates\\element\\navBar.tpl',
      1 => 1560805996,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d0803f3c82f98_80115650 (Smarty_Internal_Template $_smarty_tpl) {
?>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="<?php echo $_smarty_tpl->tpl_vars['home']->value;?>
 navbar-brand" href="<?php echo $_smarty_tpl->tpl_vars['srcDir']->value;?>
home">Soyez un héros !</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="<?php echo $_smarty_tpl->tpl_vars['category']->value;?>
"><a href="<?php echo $_smarty_tpl->tpl_vars['srcDir']->value;?>
category">Lecture</a></li>
            <li class="<?php echo $_smarty_tpl->tpl_vars['creator']->value;?>
" <?php echo $_smarty_tpl->tpl_vars['show']->value;?>
 ><a href="<?php echo $_smarty_tpl->tpl_vars['srcDir']->value;?>
creator">Ecriture</a></li>
            <li class="<?php echo $_smarty_tpl->tpl_vars['profiladmin']->value;?>
" <?php echo $_smarty_tpl->tpl_vars['show']->value;?>
><a href="<?php echo $_smarty_tpl->tpl_vars['srcDir']->value;?>
profil">Espace perso</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <form class="navbar-form navbar-left" action="/action_page.php">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
            </form>
            <li>
                <a href="#" id="btnSignUp" onclick="<?php echo $_smarty_tpl->tpl_vars['navBarFuncSignUp']->value;?>
(event)" style="<?php echo $_smarty_tpl->tpl_vars['profil']->value;?>
">
                    <span class="glyphicon glyphicon-user"></span> <?php echo $_smarty_tpl->tpl_vars['navbarBtnSignUp']->value;?>
</a>
            </li>
            <li><a href="#" id="btnSignIn" onclick="<?php echo $_smarty_tpl->tpl_vars['navBarFuncSignIn']->value;?>
(event)"><span
                            class="glyphicon glyphicon-log-in"></span> <?php echo $_smarty_tpl->tpl_vars['navBarBtnSignIn']->value;?>
</a></li>
        </ul>
    </div>
</nav>


<div class="modal fade" id="ModalSignUp" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4><span class="glyphicon glyphicon-home"></span> S'inscrire à NomSite</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="<?php echo $_smarty_tpl->tpl_vars['srcDir']->value;?>
signup">
                    <div class="form-group">
                        <label for="usrname"><span class="glyphicon glyphicon-user"></span> Identifiant</label>
                        <input type="text" class="form-control" name="usrname" placeholder="Entrez votre pseudo">
                    </div>
                    <div class="form-group">
                        <label for="usrname"><span class="glyphicon glyphicon-user"></span> E-mail</label>
                        <input type="text" class="form-control" name="email"
                               placeholder="Entrez votre adresse mail">
                    </div>
                    <div class="form-group">
                        <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> Mot de passe</label>
                        <input type="password" class="form-control" name="psw" placeholder="Entrez votre mot de passe">
                    </div>
                    <div class="form-group">
                        <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> Confirmer le mot de
                            passe</label>
                        <input type="password" class="form-control" name="psw2" placeholder="Entrez votre mot de passe">
                    </div>
                    <button type="submit" class="btn-signin btn btn-block"><span
                                class="glyphicon glyphicon-off"></span>
                        S'inscrire
                    </button>
                </form>
            </div>
            <div class="modal-footer">
                <p>Vous possédez déjà un compte ? <a href="#" id="ModalSignUpToSignIn"
                                                     onclick="signUpToSignIn(event)">Identifiez-vous !</a></p>

            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="ModalSignOut" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4><span class=" glyphicon glyphicon-log-out "></span> Se déconnecter</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="<?php echo $_smarty_tpl->tpl_vars['srcDir']->value;?>
signout">
                    <button type="submit" class="btn-signin btn btn-block"><span
                                class="glyphicon glyphicon-off"></span>
                        Deconnexion
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="ModalSignIn" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4><span class="glyphicon glyphicon-lock"></span> Se connecter à NomSite</h4>
            </div>
            <div class="modal-body">
                <div class="modal-body">
                    <form role="form" method="post" action="<?php echo $_smarty_tpl->tpl_vars['srcDir']->value;?>
signin">
                        <div class="form-group">
                            <label for="usrname"><span class="glyphicon glyphicon-user"></span> Identifiant</label>
                            <input type="text" class="form-control" name="usrname" value=""
                                   placeholder="Entrez votre pseudo">
                        </div>
                        <div class="form-group">
                            <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> Mot de passe<a href="#">
                                    Mot de
                                    passe oublié ?</a></label>
                            <input type="password" class="form-control" value="" name="psw"
                                   placeholder="Entrez votre mot de passe">
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox" checked>Maintenir la connexion</label>
                        </div>
                        <button type="submit" class="btn-signin btn btn-block"><span
                                    class="glyphicon glyphicon-off"></span>
                            Connexion
                        </button>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <p>Vous n'avez pas encore de compte ? <a href="#" id="ModalSignInToSignUp"
                                                         onclick="signUpToSignUp(event)">Créez-en un !</a></p>
            </div>
        </div>
    </div>
</div>


<?php }
}
