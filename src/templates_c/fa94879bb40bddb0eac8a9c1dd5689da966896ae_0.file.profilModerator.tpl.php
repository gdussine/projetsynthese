<?php
/* Smarty version 3.1.33, created on 2019-06-17 23:19:50
  from 'C:\xampp\htdocs\www\projetsynthese\src\templates\page\profilModerator.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d0803f6b605d3_75498356',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fa94879bb40bddb0eac8a9c1dd5689da966896ae' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\projetsynthese\\src\\templates\\page\\profilModerator.tpl',
      1 => 1560806291,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d0803f6b605d3_75498356 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3139536165d0803f6b30784_10764981', 'css');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10704768065d0803f6b35c84_40799854', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19461000895d0803f6b5fdb1_08620825', 'script');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, "../canvas/model.tpl");
}
/* {block 'css'} */
class Block_3139536165d0803f6b30784_10764981 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'css' => 
  array (
    0 => 'Block_3139536165d0803f6b30784_10764981',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['cssDir']->value;?>
profilModerator.css">
<?php
}
}
/* {/block 'css'} */
/* {block 'content'} */
class Block_10704768065d0803f6b35c84_40799854 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_10704768065d0803f6b35c84_40799854',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


<div class="container mainPage">
    <div class="row">
        <div class="col-sm-12 menuHeader">
            <h1>Votre compte <?php echo $_smarty_tpl->tpl_vars['username']->value;?>
</h1>
        </div>
    </div>
       <div>
            <p>  </p>
    </div>
 
    <div class="row">


        <div class="col-sm-12">
    <div id="idUser" data-id="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" hidden> </div> 
        <table class="table table-striped">



        <tbody>
            <tr>
                <th>Utilisateurs inscrits</th>
                <td><?php echo $_smarty_tpl->tpl_vars['countUser']->value;?>
</td>

            </tr>
            <tr>
                <th>Livres</th>
                <td><?php echo $_smarty_tpl->tpl_vars['countBook']->value;?>
</td>

            </tr>
          
        </tbody>
    </table>
        </div>


             <div class="col-sm-12">
           <div class="card">
        <h3 class="card-header text-center font-weight-bold text-uppercase py-4">Utilisateurs</h3>
        <div class="card-body">
            <div id="table" class="table-editable">
                <table class="table table-bordered table-responsive-md table-striped text-center">
                    <thead>
                        <tr>
                            <th class="text-center">Pseudo</th>
                            <th class="text-center">Livres</th>
                        </tr>
                    </thead>


                    <tbody class="tbody"> 
                    </tbody>
                </table>
            </div>
        </div>
    </div>
        </div>



    </div>
</div>
<?php
}
}
/* {/block 'content'} */
/* {block 'script'} */
class Block_19461000895d0803f6b5fdb1_08620825 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'script' => 
  array (
    0 => 'Block_19461000895d0803f6b5fdb1_08620825',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['jsDir']->value;?>
profilModerator.js"><?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'script'} */
}
