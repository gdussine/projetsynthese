<?php
/* Smarty version 3.1.33, created on 2019-06-15 02:27:52
  from '/opt/lampp/htdocs/www/projetsynthese/src/templates/canvas/skeleton.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d043b88265a35_06077896',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd92e2aaf7db27b1e4a747154bca58546f0be392e' => 
    array (
      0 => '/opt/lampp/htdocs/www/projetsynthese/src/templates/canvas/skeleton.tpl',
      1 => 1560557096,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d043b88265a35_06077896 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18459805595d043b88262435_19704879', 'head');
?>

</head>
<body>
<main>
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11646434915d043b88263420_45442518', 'navbar');
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_738366205d043b88263dc3_50390395', 'content');
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13712900315d043b882646b4_88956189', 'footer');
?>

</main>
<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16265177595d043b88265260_31033419', 'script');
?>

</body>
</html><?php }
/* {block 'head'} */
class Block_18459805595d043b88262435_19704879 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'head' => 
  array (
    0 => 'Block_18459805595d043b88262435_19704879',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'head'} */
/* {block 'navbar'} */
class Block_11646434915d043b88263420_45442518 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'navbar' => 
  array (
    0 => 'Block_11646434915d043b88263420_45442518',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'navbar'} */
/* {block 'content'} */
class Block_738366205d043b88263dc3_50390395 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_738366205d043b88263dc3_50390395',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_13712900315d043b882646b4_88956189 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_13712900315d043b882646b4_88956189',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'footer'} */
/* {block 'script'} */
class Block_16265177595d043b88265260_31033419 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'script' => 
  array (
    0 => 'Block_16265177595d043b88265260_31033419',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'script'} */
}
