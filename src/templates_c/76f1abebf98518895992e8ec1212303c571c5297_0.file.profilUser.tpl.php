<?php
/* Smarty version 3.1.33, created on 2019-06-19 23:23:49
  from 'C:\xampp\htdocs\www\projetsynthese\src\templates\page\profilUser.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d0aa7e571c5b7_92734784',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '76f1abebf98518895992e8ec1212303c571c5297' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\projetsynthese\\src\\templates\\page\\profilUser.tpl',
      1 => 1560979420,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d0aa7e571c5b7_92734784 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2224570595d0aa7e55fb477_06078976', 'css');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2164968145d0aa7e56f9332_97267956', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5054712395d0aa7e5718730_60137841', 'script');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, "../canvas/model.tpl");
}
/* {block 'css'} */
class Block_2224570595d0aa7e55fb477_06078976 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'css' => 
  array (
    0 => 'Block_2224570595d0aa7e55fb477_06078976',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['cssDir']->value;?>
profilUser.css">
<?php
}
}
/* {/block 'css'} */
/* {block 'content'} */
class Block_2164968145d0aa7e56f9332_97267956 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_2164968145d0aa7e56f9332_97267956',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


<div class="container mainPage">
    <div class="row">
        <div class="col-sm-12 menuHeader">
            <h1>Votre compte <?php echo $_smarty_tpl->tpl_vars['username']->value;?>
</h1>
        </div>
    </div>
       <div id="success" class="alert alert-success" role="alert">
            <p> votre compte a été modifié </p>
    </div>
 
    <div class="row">


        <div class="col-sm-12">
    <div id="idUser" data-id="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" hidden> </div> 
    <h3 class="h3 title-client card-header text-center font-weight-bold text-uppercase py-4">DÉTAILS PERSONNELS</h3>
    </div>
    <div class="items">
        <form id="myForm" role="form" method="post" action="" enctype="multipart/form-data">
            <div class="form-group">
                <label for='login'>Login</label>
                <input type="text" class="form-control" name="login" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
">
            </div>
            <div class="form-group">
                <label for="email">E-mail</label>
                <input type="mail" class="form-control" name="email" value="<?php echo $_smarty_tpl->tpl_vars['email']->value;?>
">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" value="<?php echo $_smarty_tpl->tpl_vars['password']->value;?>
">
            </div>
            <button type="button" class="edit-btn btn btn-block" name="add"><span class="glyphicon glyphicon-pencil"></span>
                Modifier
            </button>
        </form>
    </div>
        </div>


             <div class="col-sm-12">
           <div class="card">
        <h3 class="card-header text-center font-weight-bold text-uppercase py-4">VOS LIVRES</h3>
        <div class="card-body">
            <div id="table" class="table-editable">
                <table class="table table-bordered table-responsive-md table-striped text-center">
                    <thead>
                        <tr>
                            <th class="text-center">Titre</th>
                            <th class="text-center">Image</th>
                            <th class="text-center">Modifier</th>
                            <th class="text-center">Supprimer</th>
                        </tr>
                    </thead>


                    <tbody> 
                    </tbody>
                </table>
            </div>
        </div>
    </div>
        </div>



    </div>
</div>
<?php
}
}
/* {/block 'content'} */
/* {block 'script'} */
class Block_5054712395d0aa7e5718730_60137841 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'script' => 
  array (
    0 => 'Block_5054712395d0aa7e5718730_60137841',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['jsDir']->value;?>
profilUser.js"><?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'script'} */
}
