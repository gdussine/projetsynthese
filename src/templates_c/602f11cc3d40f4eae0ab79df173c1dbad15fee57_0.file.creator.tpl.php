<?php
/* Smarty version 3.1.33, created on 2019-06-17 01:56:21
  from '/opt/lampp/htdocs/www/projetsynthese/src/templates/page/creator.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d06d72506f377_78382198',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '602f11cc3d40f4eae0ab79df173c1dbad15fee57' => 
    array (
      0 => '/opt/lampp/htdocs/www/projetsynthese/src/templates/page/creator.tpl',
      1 => 1560729320,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d06d72506f377_78382198 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19689574905d06d725035a61_91823025', 'css');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18741354705d06d725069611_80640645', "content");
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8627876775d06d72506d196_77212608', 'script');
?>


<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, "../canvas/model.tpl");
}
/* {block 'css'} */
class Block_19689574905d06d725035a61_91823025 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'css' => 
  array (
    0 => 'Block_19689574905d06d725035a61_91823025',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['cssDir']->value;?>
creator.css">
<?php
}
}
/* {/block 'css'} */
/* {block "content"} */
class Block_18741354705d06d725069611_80640645 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_18741354705d06d725069611_80640645',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <div class="modal fade" id="confirm-delete" role="dialog">
      <div class="modal-dialog">
          <div class="modal-content">
          
              <div class="modal-header" style="padding:20px 30spx;">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4><span class="glyphicon glyphicon-trash"></span> SUPPRIMER</h4>
                </div>
          
              <div class="modal-body">
                  <p>Êtes-vous sûr de vouloir supprimer ce livre ?</p>
              </div>
              
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                  <button type="button" class="btn-modal-delete btn btn-danger btn-ok" data-dismiss="modal" data-id="">supprimer</button>
              </div>
          </div>
      </div>
  </div>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4><span class="glyphicon glyphicon-book"></span> Création livre</h4>
            </div>
            <div class="modal-body">
              <div class="modal-body">
                <form id="myForm">
                    <div class="form-group">
                        <label for="titre"><span class="glyphicon glyphicon-pencil"></span> Titre</label>
                        <input id='titre' type="text" class="form-control" name="titre" placeholder="Entrez le titre de votre livre">
                    </div>
                    <div class="form-group">
                        <label for="image"><span class="glyphicon glyphicon-picture"></span> Image</label>
                        <input type="file" class="form-control" name="image"  accept="image/jpg">
                    </div>


                    <button class="addBook btn btn-signin btn-block" data-dismiss="modal"><span
                            class="glyphicon glyphicon-ok"></span>
                        Créer
                    </button>
                </form>
                    </div>
            </div>
            <div class="modal-footer-create modal-footer">
                <p class="NombresDeLivres"></p>
            </div>
        </div>
    </div>
</div>
<div id="author" data-author=<?php echo $_smarty_tpl->tpl_vars['author']->value;?>
 hidden></div>
   <div class="col-sm-12 menuLecture">
            <h1 class="NombresDeLivres"></h1>
        </div>
<div class="main">
<div class="container">
 <div class="row liste-livre">
        <div id="btn-ajouter-livre" class="col-sm-6 col-md-6 col-lg-4 no-float livre">
            <div class="btn-add btn-ajouter">
                <button type="button" class="btn-supprimer-livre btn btn-default btn-lg" data-toggle="modal" data-target="#myModal">
                    <i class="btn-ajouter-livre glyphicon glyphicon-plus" aria-hidden="true"></i>
                  </button>
            </div>
          </div>
    </div>
  </div>
  </div>
</div>
<?php
}
}
/* {/block "content"} */
/* {block 'script'} */
class Block_8627876775d06d72506d196_77212608 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'script' => 
  array (
    0 => 'Block_8627876775d06d72506d196_77212608',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['jsDir']->value;?>
creator.js"><?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'script'} */
}
