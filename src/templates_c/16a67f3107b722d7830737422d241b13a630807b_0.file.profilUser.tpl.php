<?php
/* Smarty version 3.1.33, created on 2019-06-17 22:48:52
  from '/opt/lampp/htdocs/www/projetsynthese/src/templates/page/profilUser.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d07fcb4c426b2_65848988',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '16a67f3107b722d7830737422d241b13a630807b' => 
    array (
      0 => '/opt/lampp/htdocs/www/projetsynthese/src/templates/page/profilUser.tpl',
      1 => 1560803748,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d07fcb4c426b2_65848988 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19789911405d07fcb4c303e9_40661483', 'css');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20521141045d07fcb4c3a667_55546696', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21266415545d07fcb4c40867_20840743', 'script');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, "../canvas/model.tpl");
}
/* {block 'css'} */
class Block_19789911405d07fcb4c303e9_40661483 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'css' => 
  array (
    0 => 'Block_19789911405d07fcb4c303e9_40661483',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['cssDir']->value;?>
profilUser.css">
<?php
}
}
/* {/block 'css'} */
/* {block 'content'} */
class Block_20521141045d07fcb4c3a667_55546696 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_20521141045d07fcb4c3a667_55546696',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


<div class="container mainPage">
    <div class="row">
        <div class="col-sm-12 menuHeader">
            <h1>Votre compte <?php echo $_smarty_tpl->tpl_vars['username']->value;?>
</h1>
        </div>
    </div>
       <div id="success" class="alert alert-success" role="alert">
            <p> votre compte a été modifier </p>
    </div>
 
    <div class="row">


        <div class="col-sm-12">
    <div id="idUser" data-id="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" hidden> </div> 
    <h3 class="h3 title-client card-header text-center font-weight-bold text-uppercase py-4">DÉTAILS PERSONNELS</h3>
    </div>
    <div class="items">
        <form id="myForm" role="form" method="post" action="" enctype="multipart/form-data">
            <div class="form-group">
                <label for='login'>Login</label>
                <input type="text" class="form-control" name="login" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
">
            </div>
            <div class="form-group">
                <label for="email">E-mail</label>
                <input type="mail" class="form-control" name="email" value="<?php echo $_smarty_tpl->tpl_vars['email']->value;?>
">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" value="<?php echo $_smarty_tpl->tpl_vars['password']->value;?>
">
            </div>
            <button type="button" class="edit-btn btn btn-block" name="add"><span class="glyphicon glyphicon-pencil"></span>
                Modifier
            </button>
        </form>
    </div>
        </div>


             <div class="col-sm-12">
           <div class="card">
        <h3 class="card-header text-center font-weight-bold text-uppercase py-4">VOS LIVRES</h3>
        <div class="card-body">
            <div id="table" class="table-editable">
                <table class="table table-bordered table-responsive-md table-striped text-center">
                    <thead>
                        <tr>
                            <th class="text-center">Titre</th>
                            <th class="text-center">Image</th>
                            <th class="text-center">Modifier</th>
                            <th class="text-center">Supprimer</th>
                        </tr>
                    </thead>


                    <tbody> 
                    </tbody>
                </table>
            </div>
        </div>
    </div>
        </div>



    </div>
</div>
<?php
}
}
/* {/block 'content'} */
/* {block 'script'} */
class Block_21266415545d07fcb4c40867_20840743 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'script' => 
  array (
    0 => 'Block_21266415545d07fcb4c40867_20840743',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['jsDir']->value;?>
profilUser.js"><?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'script'} */
}
