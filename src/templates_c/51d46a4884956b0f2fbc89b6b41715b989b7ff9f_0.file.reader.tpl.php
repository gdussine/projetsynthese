<?php
/* Smarty version 3.1.33, created on 2019-06-18 21:51:32
  from 'C:\xampp\htdocs\www\projetsynthese\src\templates\page\reader.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d0940c404c002_28174696',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '51d46a4884956b0f2fbc89b6b41715b989b7ff9f' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\projetsynthese\\src\\templates\\page\\reader.tpl',
      1 => 1560887486,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d0940c404c002_28174696 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11068771415d0940c3ccb2e2_08501287', 'css');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15842584645d0940c3e26db3_96818077', 'content');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "../canvas/model.tpl");
}
/* {block 'css'} */
class Block_11068771415d0940c3ccb2e2_08501287 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'css' => 
  array (
    0 => 'Block_11068771415d0940c3ccb2e2_08501287',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['cssDir']->value;?>
reader.css">
	<link href="https://fonts.googleapis.com/css?family=Audiowide&display=swap" rel="stylesheet">
<?php
}
}
/* {/block 'css'} */
/* {block 'content'} */
class Block_15842584645d0940c3e26db3_96818077 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_15842584645d0940c3e26db3_96818077',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


<div class="container mainPage">
    <div class="row">
        <div class="col-sm-12 menuLecture">
            <h1><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</h1>
        </div>
        <div class="col-sm-12 itemLecture">
            <ul>
                <a href='/www/projetsynthese/src/category'><li>Fermer le livre</li></a>
                <a href='/www/projetsynthese/src/read/<?php echo $_smarty_tpl->tpl_vars['idBook']->value;?>
'><li>Commentaires</li></a>
                <li>Résumé</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <div class="row">
                <div class="text-center cover col-sm-12">
                    <img class="img-responsive coverBook" src="<?php echo $_smarty_tpl->tpl_vars['imgDir']->value;
echo $_smarty_tpl->tpl_vars['idBook']->value;?>
.jpg">
                </div>
                <div class="col-sm-12">
                    <span>Auteur : <?php echo $_smarty_tpl->tpl_vars['author']->value;?>
</span>
                </div>
                <div class="col-sm-12">
                    <span class="etoile">‎★‎★‎★‎★‎★</span>
                </div>
            </div>
        </div>

        <div class="col-sm-8">
            <div class="page">
                <div class="text"><p>
                        <?php echo $_smarty_tpl->tpl_vars['text']->value;?>

                    </p></div>
                <div class="choix">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listChoice']->value, 'choice');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['choice']->value) {
?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['choice']->value->getDestinationPage()->getIdPage();?>
">
                        <button  type="button" class="btn-choix" onclick=""><?php echo $_smarty_tpl->tpl_vars['choice']->value->getDescription();?>

                        </button></a>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
            </div>

        </div>
    </div>
</div>
<?php
}
}
/* {/block 'content'} */
}
