<?php
/* Smarty version 3.1.33, created on 2019-06-15 02:27:52
  from '/opt/lampp/htdocs/www/projetsynthese/src/templates/canvas/model.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d043b88255748_90692992',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '62bac77270d5656946a77617b8c9a61905e8e960' => 
    array (
      0 => '/opt/lampp/htdocs/www/projetsynthese/src/templates/canvas/model.tpl',
      1 => 1560557096,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../element/navBar.tpl' => 1,
    'file:../element/footer.tpl' => 1,
  ),
),false)) {
function content_5d043b88255748_90692992 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8929928905d043b8822d994_76749249', 'css');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17934303405d043b88236bb3_84452851', 'navbar');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2538349765d043b88251350_35143106', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15597954445d043b88252483_56110995', 'footer');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9757058655d043b88254398_61400613', 'script');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, "../canvas/base.tpl");
}
/* {block 'css'} */
class Block_8929928905d043b8822d994_76749249 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'css' => 
  array (
    0 => 'Block_8929928905d043b8822d994_76749249',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['cssDir']->value;?>
navBar.css">
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['cssDir']->value;?>
footer.css">
<?php
}
}
/* {/block 'css'} */
/* {block 'navbar'} */
class Block_17934303405d043b88236bb3_84452851 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'navbar' => 
  array (
    0 => 'Block_17934303405d043b88236bb3_84452851',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../element/navBar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
/* {/block 'navbar'} */
/* {block 'content'} */
class Block_2538349765d043b88251350_35143106 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_2538349765d043b88251350_35143106',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_15597954445d043b88252483_56110995 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_15597954445d043b88252483_56110995',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../element/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
/* {/block 'footer'} */
/* {block 'script'} */
class Block_9757058655d043b88254398_61400613 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'script' => 
  array (
    0 => 'Block_9757058655d043b88254398_61400613',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['jsDir']->value;?>
navBar.js"><?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'script'} */
}
