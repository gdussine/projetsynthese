<?php
require '../vendor/autoload.php';
use ProjetSynthese\DAO\DAOFactory;
use ProjetSynthese\Model\Comment;
use ProjetSynthese\Model\Status;
use ProjetSynthese\Model\User;


//Sauvegarde du Livre en Ajax
$user = null;
if ($_POST["idUser"]>0) {
    $user = DAOFactory::getUserDAO()->getById($_POST["idUser"]);
}
$book = DAOFactory::getBookDAO()->getById($_POST["idBook"]);
if ($_POST["comment"] != "") {
    $datetime = new DateTime();
    $datetime->format('Y-m-d H:i:s');
    $comment = new Comment($user, $book, $_POST["comment"], $datetime);
    DAOFactory::getCommentDAO()->create($comment);
}
$comments = DAOFactory::getCommentDAO()->getByBook($book);
$listComments = [];
foreach ($comments as $comment) {
    $pseudo = $comment->getAuthor()->getLogin();
    $commentInArray = null;
    $commentInArray = [
        'pseudo' => $pseudo,
        'comment' => $comment->getComment(),
        'date' =>  date_format($comment->getDate(), 'Y-m-d H:i:s'),

    ];
    array_push($listComments, $commentInArray);
}
echo json_encode($listComments);
