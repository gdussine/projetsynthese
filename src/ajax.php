<?php
require '../vendor/autoload.php';
use ProjetSynthese\DAO\DAOFactory;
use ProjetSynthese\Model\Book;
use ProjetSynthese\Model\Status;


//Sauvegarde du Livre en Ajax
if (isset($_POST["titre"]) || isset($_POST["author"])) {
    $user = DAOFactory::getUserDAO()->getById($_POST['author']);
    if (isset($_POST["titre"])) {
        $newBook = new Book(-1, $_POST["titre"], "", $user, new Status(1, Status::STATUS_IN_WRITING));
        DAOFactory::getBookDAO()->create($newBook);
        $folder = "view/upload/";
        $filename = $_FILES['image']['name'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        $imageName = strval(DAOFactory::getBookDAO()->getLastId());
        $image = $imageName . "." . "jpg";
        $path = $folder . $image;
        $target_file = $folder . basename($_FILES["image"]["name"]);
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
        $allowed = array('jpeg', 'png', 'jpg');
        move_uploaded_file($_FILES['image']['tmp_name'], $path);
        $idBook = $newBook->getIdBook();
    }
    if (isset($_POST["delete"])) {
        $id = $_POST["delete"];
        $deleteBook = DAOFactory::getBookDAO()->getById($_POST["delete"]);
        unlink("view/upload/".$id.'.jpg');
        DAOFactory::getBookDAO()->delete($deleteBook);
    }
    $books = DAOFactory::getBookDAO()->getByAuthor($user);
    $listBooks = [];
    foreach ($books as $book) {
        $bookInArray = null;
        $bookInArray = [
            'idBook' => $book->getIdBook(),
            'title' => ucfirst($book->getTitle()),
            'abstract' => $book->getAbstract(),
            'status' => $book->getStatus()->getIdStatus()
        ];
        array_push($listBooks, $bookInArray);
    }
    echo json_encode($listBooks);
}
