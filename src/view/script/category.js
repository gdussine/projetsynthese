$(document).ready(function () {

    $(".div-left").click(function () {
        $(".slide-left", this).slideToggle("slow");
    });

    $(".animer").click(function () {
        $(".main-left").animate({
            width: 'toggle'
        });
        $(this).find('i').toggleClass('glyphicon-triangle-left').toggleClass(
            'glyphicon-triangle-right');
    });


    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            var liste = $.parseJSON(this.responseText);
            liste.forEach(element => {
                let newPage = $("<div/>")
                    .addClass("col-sm-6 col-md-6 col-lg-4")
                    .html("<div class='liste-livres col-lg-12'>"+
                        "<div id='livre'>"+
                            "<div class='img-livre'>"+
                                "<img class='image-liste-livres img-responsive' src='view/upload/" + element.idBook + ".jpg'"+
                                    "alt='image 1'>"+
                                "<p class='description-livre'></p>"+
                            "</div>"+
                            "<h3 class='nomliste-livres'>" + element.title + "</h3>"+
                            "<h5>Publiée par " + element.login + "</h5>"+
                            "<div class='btn-lecture'><a href='/www/projetsynthese/src/read/"+element.idBook+"/"+element.idFirstPage    +"'><button id='add_row' type='button'"+
                                    "class='btn-ajouter btn btn-primary' data-id="+element.idBook +"'>"+
                                    "Lecture"+
                                "</button></a>"+
                           "</div>"+
                        "</div>"+
                    "</div>"+
                "</div>");
                $("#add-books").append(newPage);

            });
        }
    };
    xmlhttp.open("POST", "ajaxCategory.php", true);
    xmlhttp.send();

});