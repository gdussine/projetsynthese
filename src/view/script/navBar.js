function signUp(event) {
    $("#ModalSignUp").modal();
}
function signOut(event) {
    $("#ModalSignOut").modal();
}
function signIn(event) {
    $("#ModalSignIn").modal();
}
function signUpToSignUp(event) {
    $("#ModalSignIn").modal('toggle');
    $("#ModalSignUp").modal();
}
function signUpToSignIn(event) {
    $("#ModalSignUp").modal('toggle');
    $("#ModalSignIn").modal();
}