$(document).ready(function () {
    var livre = [];
    var lastEventAdd = true;
    var inputTitre = document.getElementById("titre-page");
    var inputChoix1 = document.getElementById("choix-1");
    var inputChoix2 = document.getElementById("choix-2");
    var inputChoix3 = document.getElementById("choix-3");
    var inputChoix4 = document.getElementById("choix-4");
    var elementSelected = $('#83');
    var indexPage = 0;
    var idBook = $('.stockage').attr('data-book');
    for (let i = 1; i < 5; i++) {
        $('#page-choix-' + i).append("<option value='" + -1 + "'>##</option>")
    }
    loadLivre(idBook);

    function viderInput() {
        for (let i = 1; i < 5; i++) {
            $('#page-choix-' + i).val('-1');
            $('#choix-' + i).val('');
        }
    }

    function loadLivre(id) {

        formData = new FormData();
        formData.append('idBook', id);
        formData.append('loadBook', 1);
        let xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                var liste = $.parseJSON(this.responseText);
                $('#summernote').summernote('reset');
                liste.forEach(function (element) {
                    let newPage = $("<div/>")
                        .addClass("page col-xs-12 col-md-12 col-lg-12 focus")
                        .html("<input type='radio' name='list' class='checkbox-page' id='" + element.idPage + "'><label for='" + element.idPage + "'>Page ID " + element.idPage + "</label>");
                    $("#div-view .btn-ajouter-page:last").before(newPage);
                    $("#div-view #" + element.idPage + "").prop("checked", true);
                    $(inputTitre).val('');
                    $(document.getElementById("main-center")).css('visibility', 'visible');
                    $(".btn-supprimer-page").show();
                    $(".btn-enregistrer-page").show();
                    for (let i = 1; i < 5; i++) {
                        $('#page-choix-' + i).append("<option value='" + element.idPage + "'>" + element.idPage + "</option>")
                    }
                });
                elementSelected = $("#div-view #"+liste[liste.length-1].idPage).prop("checked", true);
                loadTextPage(liste[liste.length - 1].idPage);
                loadChoixPage(liste[liste.length - 1].idPage);
            }
        };
        xmlhttp.open("POST", "../ajaxWriter.php", true);
        xmlhttp.send(formData);


    }

    function loadChoixPage(id) {
        formData = new FormData();
        formData.append('idPage', id);
        formData.append('loadChoix', 1);
        let xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                var liste = $.parseJSON(this.responseText);
                let i = 1;
                liste.forEach(function (element) {
                    let input = $('#choix-' + i);
                    input.val(element.description);
                    let select = $('#page-choix-' + i);
                    select.val(element.idDestinationPage);
                    i++;
                })
            }
        };
        xmlhttp.open("POST", "../ajaxWriter.php", false);
        xmlhttp.send(formData);
    }


    function loadTextPage(id) {
        formData = new FormData();
        formData.append('loadPage', 1);
        formData.append('idPage', id);
        let xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                var text = this.responseText;
                $('#summernote').summernote('code', text);
            }
        };
        xmlhttp.open("POST", "../ajaxWriter.php", false );
        xmlhttp.send(formData);
    }

    function saveChoicePage(id, txt1, txt2, txt3, txt4, page1, page2, page3, page4) {
        formData = new FormData();
        formData.append('saveChoice', 1);
        formData.append('idPage', id);
        formData.append('textChoix1', txt1);
        formData.append('textChoix2', txt2);
        formData.append('textChoix3', txt3);
        formData.append('textChoix4', txt4);
        formData.append('idPageChoix1', page1);
        formData.append('idPageChoix2', page2);
        formData.append('idPageChoix3', page3);
        formData.append('idPageChoix4', page4);
        let xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                var rep = this.responseText;
            }
        };
        xmlhttp.open("POST", "../ajaxWriter.php", true);
        xmlhttp.send(formData);
    }

    function saveTextPage(id, text) {

        formData = new FormData();
        formData.append('savePage', 1);
        formData.append('idPage', id);
        formData.append('text', text);
        let xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                var rep = this.responseText;
            }
        };
        xmlhttp.open("POST", "../ajaxWriter.php", true);
        xmlhttp.send(formData);
    }

    $(document.getElementById("main-center")).css('visibility', 'hidden');

    $(".btn-supprimer-page").hide()
    $(".btn-enregistrer-page").hide()

    $('#summernote').summernote({
        height: 550,
        maxHeight: 385,
        placeholder: "Saisissez le contenu de votre page ici...",
        toolbar: [
            ['view', ['undo', 'redo']],
            ['style', ['bold', 'italic', 'underline']],
            ['font', ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['view', ['fullscreen', 'codeview']]
        ]
    });


    $(".btn-ajouter-page").click(function () {
        let idBefore = $(elementSelected).attr('id');
        if($('#div-view .checkbox-page').length>0){
            saveTextPage(idBefore, $('#summernote').summernote('code'));
            saveChoicePage(idBefore, $('#choix-1').val(),
                $('#choix-2').val(), $('#choix-3').val(),
                $('#choix-4').val(), $('#page-choix-1').val(),
                $('#page-choix-2').val(), $('#page-choix-3').val(), $('#page-choix-4').val());
        }
        viderInput();
        let formData = new FormData();
        formData.append('newPage', 1);
        formData.append('idBook', idBook);
        formData.append('num', 5);
        let xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                var rep = this.responseText;
                let newPage = $("<div/>").addClass("page col-xs-12 col-md-12 col-lg-12 focus")
                    .html("<input type='radio' name='list' class='checkbox-page' id='" + rep + "'><label for='" + rep + "'> Page ID "+rep+"</label>");
                $("#div-view .btn-ajouter-page:last").before(newPage);
                elementSelected = $("#div-view #" + rep + "").prop("checked", true);
                $('#summernote').summernote('reset');
                $(inputTitre).val('');
                $(document.getElementById("main-center")).css('visibility', 'visible');
                for (let i = 1; i < 5; i++) {
                    $('#page-choix-' + i).append("<option value='" + rep + "'>" + rep + "</option>")
                }
                $(".btn-supprimer-page").show();
                $(".btn-enregistrer-page").show();
            }
        };
        xmlhttp.open("POST", "../ajaxWriter.php", true);
        xmlhttp.send(formData);

    });

    $(document).on('change', 'input:radio', function () {

        let idSelected = $(this).attr('id');
        let idBefore = $(elementSelected).attr('id');
        let a = saveTextPage(idBefore, $('#summernote').summernote('code'));
        let b = saveChoicePage(idBefore, $('#choix-1').val(),
            $('#choix-2').val(), $('#choix-3').val(),
            $('#choix-4').val(), $('#page-choix-1').val(),
            $('#page-choix-2').val(), $('#page-choix-3').val(), $('#page-choix-4').val());
        viderInput();
        loadChoixPage(idSelected);
        loadTextPage(idSelected);
        elementSelected = $(this);
    });


    $(".btn-supprimer-page").click(function () {
        $('.page input:checked').each(function () {
            let idSelected = $(this).attr('id')
            $(this).parent().remove()
            formData = new FormData();
            formData.append('deletePage', 1);
            formData.append('idPage', idSelected);
            let xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    var rep = this.responseText;
                    elementSelected = ""
                    $('#summernote').summernote('reset');
                }
            }
            xmlhttp.open("POST", "../ajaxWriter.php", true);
            xmlhttp.send(formData);
            viderInput();
            for(let i = 1; i<5; i++){
                let op = $('#page-choix-'+i+" option[value="+idSelected+"]");
                op.remove();
            }
        });
    });


    $(".btn-enregistrer-page").click(function () {
        let element = elementSelected;
        let id = $(element).attr('id')
        let getTitre = inputTitre.value
        let getChoix1 = inputChoix1.value
        let getChoix2 = inputChoix2.value
        let getChoix3 = inputChoix3.value
        let getChoix4 = inputChoix4.value
        let getPage = $('#summernote').summernote('code');
        if (!livre[id]) {
            livre[id] = {
                id: id,
                titre: getTitre,
                text: getPage,
                choix1: getChoix1,
                choix2: getChoix2,
                choix3: getChoix3,
                choix4: getChoix4,
            }
        } else {
            livre[id].titre = getTitre;
            livre[id].choix1 = getChoix1;
            livre[id].choix2 = getChoix2;
            livre[id].choix3 = getChoix3;
            livre[id].choix4 = getChoix4;
            livre[id].text = $('#summernote').summernote('code');
        }
    });

})
;