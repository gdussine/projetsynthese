$(document).ready(function () {
    var idUser = $("#idUser").attr("data-idUser");
    var idBook = $("#idBook").attr("data-IdBook");
    console.log(idUser);
    console.log(idBook);
    formData = new FormData(document.getElementById('form'));
    if(idBook){
    formData.append('idUser', idUser);
    }
    formData.append('idBook', idBook);
    if (idUser.length == 0) {
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var liste = $.parseJSON(this.responseText);
                console.log(liste);
                liste.forEach(element => {
                    let newPage = $("<div/>")
                        .addClass("comment col-md-12")
                        .html("<div class='profil'>" +
                            "<span>" + element.pseudo + "</span> / " +
                            "<span>" + element.date + "</span>" +
                            "</div>" +
                            "<div class='text'>" +
                            "<p>" + element.comment + "</p>" +
                            "</div>" +
                            "<div>" +
                            "<button class='icon-button-up'><span class='glyphicon glyphicon-thumbs-up'></span></button>" +
                            "<span>0</span>" +
                            "<button class='icon-button-down'><span class='glyphicon glyphicon-thumbs-down'></span></button>" +
                            "<span>0</span>" +
                            "</div>" +
                            "</div>");

                    $(".add-comment").after(newPage);

                });

            }
        };
        xmlhttp.open("POST", "/www/projetsynthese/src/ajaxComment.php", true)
        xmlhttp.send(formData);
    }
    autosize($('textarea'));
    $(".formulaire").hide();

    $(document).on('click', function (e) {
        $(".formulaire").hide();
    });

    $('textarea').on('click', function (e) {
        $(".formulaire").show();
        e.stopPropagation();
    });

    $(document).on('click', '.btn-ajouter-commentaire', function (event) {
        var idUser = $("#idUser").attr("data-idUser");
        var idBook = $("#idBook").attr("data-IdBook");
        console.log(idUser);
        console.log(idBook);
        formData = new FormData(document.getElementById('form'));
        formData.append('idUser', idUser);
        formData.append('idBook', idBook);
        if (idUser.length == 0) {
            return;
        } else {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    var liste = $.parseJSON(this.responseText);
                    newComment = liste[liste.length - 1];
                    let newPage = $("<div/>")
                        .addClass("comment col-md-12")
                        .html("<div class='profil'>" +
                            "<span>" + newComment.pseudo + "</span> / " +
                            "<span>" + newComment.date + "</span>" +
                            "</div>" +
                            "<div class='text'>" +
                            "<p>" + newComment.comment + "</p>" +
                            "</div>" +
                            "<div>" +
                            "<button class='icon-button-up'><span class='glyphicon glyphicon-thumbs-up'></span></button>" +
                            "<span>0</span>" +
                            "<button class='icon-button-down'><span class='glyphicon glyphicon-thumbs-down'></span></button>" +
                            "<span>0</span>" +
                            "</div>" +
                            "</div>");

                    $(".add-comment").after(newPage);



                }
            };
            xmlhttp.open("POST", "/www/projetsynthese/src/ajaxComment.php", true)
            xmlhttp.send(formData);
        }
    })
})