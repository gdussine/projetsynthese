$(document).ready(function () {
    $("#success").hide();
    var idUser = $("#idUser").attr("data-id");
    formData = new FormData();
    formData.append('idUser', idUser);
    if (idUser.length == 0) {
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var liste = $.parseJSON(this.responseText);
                liste.forEach(element => {
                    let newPage = $("<tr/>")
                        .addClass('tr' + element.idBook)
                        .html("<form method='POST' id='form" + element.idBook + "'></form>"+
                            "<input  id='oldImage" + element.idBook + "'  form='form" + element.idBook + "' type='hidden' class='form-control' name='oldImage' value='" + element.idBook + "'>" +
                            "<td class='titre pt-3-half' contenteditable='true'> <input id='inputTitre" + element.idBook + "' form='form" + element.idBook + "' type='text' class='form-control' name='titre' value='" + element.title + "'></td>" +
                            "<td id='inputfile' class='pt-3-half' contenteditable='true'>" +
                            "<label for='image" + element.idBook + "'>" +
                            "<span class='glyphicon glyphicon-folder-open' aria-hidden='true'>" +
                            "<input type='file' form='form" + element.idBook + "' id='image" + element.idBook + "' style='display:none; cursor: pointer;'  name='image'>" +
                            "</span>" +
                            "</label>" +
                            "</td>" +
                            "<td>" +
                            "<button id='update' data-id='" + element.idBook + "' form='form" + element.idBook + "' type='button' name='update' class='btn btn-primary btn-md'><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span></button>" +
                            "</td>" +
                            "<td>" +
                            "<span class='table-remove'><button id='delete' data-id='" + element.idBook + "' form='form" + element.idBook + "' type='button' name='delete' class='btn btn-danger btn-md'><span class='glyphicon glyphicon-minus-sign' aria-hidden='true'></span></button></span>" +
                            "</td>" +
                            "</tr>");

                    $('tbody').append(newPage);

                });
            }
        };
        xmlhttp.open("POST", "/www/projetsynthese/src/ajaxProfilUser.php", true)
        xmlhttp.send(formData);
    }

    $(document).on('click', '.edit-btn', function (event) {
        var idUser = $("#idUser").attr("data-id");
        var edit = 'edit';
        formData = new FormData(document.getElementById('myForm'));
        formData.append('idUser', idUser);
        formData.append('edit', edit);
        if (idUser.length == 0) {
            return;
        } else {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    $("#success").show();
                    window.setTimeout(function () {
                        $("#success").fadeTo(500, 0).slideUp(500, function () {
                            $(this).remove();
                        });
                    }, 4000);
                }
            };
            xmlhttp.open("POST", "/www/projetsynthese/src/ajaxProfilUser.php", true)
            xmlhttp.send(formData);
        }
    })

    $(document).on('click', '#update', function (event) {
        var idUser = $("#idUser").attr("data-id");
        var update = "update";
        var idBook = $(this).attr("data-id");
        console.log(idBook);
        
        formData = new FormData(document.getElementById('form'+idBook));
        formData.append('idUser', idUser);
        formData.append('idBook', idBook);
        formData.append('update', update);
        if (idBook.length == 0) {
            return;
        } else {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                        console.log(this.responseText);
                }
            };
            xmlhttp.open("POST", "/www/projetsynthese/src/ajaxProfilUser.php", true)
            xmlhttp.send(formData);
        }
    })

    $(document).on('click', '#delete', function (event) {
        var idUser = $("#idUser").attr("data-id");
        var delet = "delete";
        var idBook = $(this).attr("data-id");
        console.log(idBook);
        
        formData = new FormData(document.getElementById('form'+idBook));
        formData.append('idUser', idUser);
        formData.append('idBook', idBook);
        formData.append('delete', delet);
        if (idBook.length == 0) {
            return;
        } else {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    $('.tr' + idBook).remove();
                }
            };
            xmlhttp.open("POST", "/www/projetsynthese/src/ajaxProfilUser.php", true)
            xmlhttp.send(formData);
        }
    })
})