$(document).ready(function () {
    var author = $("#author").attr("data-author");
    sizeOfBook = null;
    formData = new FormData();
    formData.append('author', author);
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var liste = $.parseJSON(this.responseText);
            sizeOfBook = liste.length;
            liste.forEach(element => {
                let newPage = $("<div/>")
                    .addClass(element.idBook + " col-sm-6 col-md-6 col-lg-4 no-float livre")
                    .html("<div id='" + element.idBook + "' class='contenu-livre'>" +
                        "<div class='top-livre'>" +
                        "<img class='image img-responsive' src='view/upload/" + element.idBook + ".jpg' alt='image 1'>" +
                        "</div>" +
                        "<div class='div-title'><p class='title'>" + element.title + "</p></div>" +
                        "<div class='bottom-livre'>" +
                        "<div class='bottom-livre-left col-md-6'>" +
                        "<a href='/www/projetsynthese/src/write/"+element.idBook+"'> <button type='button' class='btn-bottom-livre btn btn-default btn-lg'>" +
                        "<i class='blue glyphicon glyphicon-pencil' aria-hidden='true'></i>" +
                        "</button></a>" +
                        "</div>" +
                        "<div class='bottom-livre-right col-md-6'>" +
                        "<button type='button' data-id='" + element.idBook + "' class='btn-delete btn-bottom-livre btn btn-default btn-lg' data-toggle='modal' data-target='#confirm-delete'>" +
                        "<i class='red glyphicon glyphicon-trash' aria-hidden='true'></i>" +
                        "</button>" +
                        "</div>" +
                        "</div>" +
                        "</div>");

                $("#btn-ajouter-livre").after(newPage);

            });
            $('.NombresDeLivres').text('Vous avez actuellement '+sizeOfBook+' livres')

        }

    };
    xmlhttp.open("POST", "ajax.php", true);
    xmlhttp.send(formData);

    $(document).on('click', '.addBook', function (event) {
        var title = $('#titre').val();
        var author = $("#author").attr("data-author");
        formData = new FormData(document.getElementById('myForm'));
        formData.append('author', author);
        if (title.length == 0) {
            return;
        } else {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    var liste = $.parseJSON(this.responseText);
                    sizeOfBook++;
                    newBook = liste[liste.length - 1];
                    let newPage = $("<div/>")
                        .addClass(newBook.idBook + " col-sm-6 col-md-4 col-lg-4 no-float livre")
                        .html("<div id='" + newBook.idBook + "' class='contenu-livre'>" +
                        "<div class='top-livre'>" +
                        "<img class='image img-responsive' src='view/upload/" + newBook.idBook + ".jpg' alt='image 1'>" +
                        "</div>" +
                        "<div class='div-title'><p class='title'>" + newBook.title + "</p></div>" +
                        "<div class='bottom-livre'>" +
                        "<div class='bottom-livre-left col-md-6'>" +
                        "<a href='/www/projetsynthese/src/write/"+newBook.idBook+"'><button type='button' class='btn-bottom-livre btn btn-default btn-lg'>" +
                        "<i class='blue glyphicon glyphicon-pencil' aria-hidden='true'></i>" +
                        "</button></a>" +
                        "</div>" +
                        "<div class='bottom-livre-right col-md-6'>" +
                        "<button type='button' data-id='" + newBook.idBook + "' class='btn-delete btn-bottom-livre btn btn-default btn-lg' data-toggle='modal' data-target='#confirm-delete'>" +
                        "<i class='red glyphicon glyphicon-trash' aria-hidden='true'></i>" +
                        "</button>" +
                        "</div>" +
                        "</div>" +
                        "</div>");
                    $("#btn-ajouter-livre").after(newPage);
                    $('.NombresDeLivres').text('Vous avez actuellement '+sizeOfBook+' livres')


                }
            };
            xmlhttp.open("POST", "ajax.php", true);
            xmlhttp.send(formData);
        }
    })

    $(document).on('click', '.btn-delete', function (event) {
        id = $(this).attr("data-id");
        console.log(id);
        $('.btn-modal-delete').attr('data-id', id);

    })



    $(document).on('click', '.btn-modal-delete', function (event) {
        var id = $(this).attr("data-id");
        var author = $("#author").attr("data-author");
        formData = new FormData();
        formData.append('author', author);
        formData.append('delete', id);
        if (id.length == 0) {
            return;
        } else {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    sizeOfBook--;
                    $('.' + id).remove();
                    $('.NombresDeLivres').text('Vous avez actuellement '+sizeOfBook+' livres')

                }
            };
            xmlhttp.open("POST", "ajax.php", true);
            xmlhttp.send(formData);
        }
    })
})