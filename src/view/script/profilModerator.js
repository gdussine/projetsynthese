$(document).ready(function () {
    var idUser = $("#idUser").attr("data-id");
    formData = new FormData();
    formData.append('idUser', idUser);
    if (idUser.length == 0) {
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var liste = $.parseJSON(this.responseText);
                console.log(liste);
                liste.forEach(element => {
                    console.log(element.login);
                    console.log(element.idUser);
                    let newPage = $("<tr/>")
                        .addClass('tr' + element.idBook)
                        .html("<form method='POST' id='form" + element.idBook + "'></form>"+
                            "<td class='titre pt-3-half' contenteditable='true'> <input id='inputTitre" + element.idBook + "' form='form" + element.idBook + "' type='text' class='form-control' name='titre' value='" + element.login + "'></td>" +
                            "<td>" +
                            "<a href='/www/projetsynthese/src/profil/"+element.idUser+"'><button id='update' data-id='" + element.idBook + "' form='form" + element.idBook + "' type='button' name='update' class='btn btn-primary btn-md'><span class='glyphicon glyphicon-book' aria-hidden='true'></span></button></a>" +
                            "</td>" +
                        
                            "</tr>");

                    $('.tbody').append(newPage);

                });
            }
        };
        xmlhttp.open("POST", "/www/projetsynthese/src/ajaxProfilModerator.php", true)
        xmlhttp.send(formData);
    }

})