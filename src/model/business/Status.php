<?php

namespace ProjetSynthese\Model;

use ReflectionClass;

class Status
{
    /* @var int $idStatus */
    private $idStatus;
    /* @var string $lbStatus */
    private $lbStatus;

    const STATUS_VALID = 'Valide';
    const STATUS_INVALID = 'Invalide';
    const STATUS_WAITING = 'En attente de validation';
    const STATUS_IN_WRITING = 'En cours de rédaction';

    /**
     * Status constructor.
     * @param int $idStatus
     * @param string $lbStatus
     */
    public function __construct(int $idStatus, string $lbStatus)
    {
        $this->idStatus = $idStatus;
        $this->setLbStatus($lbStatus);
    }

    public static function isStatusLbValid(string $lib){
        foreach ((new ReflectionClass(__CLASS__))->getConstants() as $const) {
            if ($lib == $const)
                return true;
        }
        return false;
    }

    /**
     * @return int
     */
    public function getIdStatus(): int
    {
        return $this->idStatus;
    }

    /**
     * @param int $idStatus
     */
    public function setIdStatus(int $idStatus): void
    {
        $this->idStatus = $idStatus;
    }

    /**
     * @return string
     */
    public function getLbStatus(): string
    {
        return $this->lbStatus;
    }

    /**
     * @param string $lbStatus
     */
    public function setLbStatus(string $lbStatus): void
    {
        if (self::isStatusLbValid($lbStatus))
            $this->lbStatus = $lbStatus;
        else
            throw new InvalidArgumentException("Libellé incorrect");
    }

    public function __toString()
    {

        return "Status : " . $this->idStatus . " - " . $this->lbStatus;
    }

}
