<?php

namespace ProjetSynthese\Model;

class User
{
    /* @var int */
    protected $idUser;
    /* @var string */
    protected $login;
    /* @var string */
    protected $password;
    /* @var string */
    protected $email;
    /* @var Role */
    protected $role;

    const GUEST_ID=0;
    const GUEST_LOGIN="Invité";
    const GUEST_PASSWORD="";
    const GUEST_EMAIL="";

    public  static function getGuest(){
        return new User(self::GUEST_ID,self::GUEST_LOGIN,self::GUEST_PASSWORD,self::GUEST_EMAIL, new Role(0,Role::ROLE_GUEST) );
    }

    public function __construct(int $idUser, string  $login, string $password, string $email,Role $role)
    {

        $this->idUser = $idUser;
        $this->login = $login;
        $this->password = $password;
        $this->email = $email;
        $this->role = $role;
    }

    /**
     * @return int
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * @param int $id
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $mail
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
    
    public function isAdmin():bool {
        return $this->role->isRoleAdmin();
    }

    /**
     * @return Role
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param Role $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    public function __toString()
    {

        return "User : ".$this->getIdUser()." - ".$this->getLogin();
    }



}