<?php

namespace ProjetSynthese\Model;

class Page
{
    /**
     * @var int $idPage
     */
    private $idPage;
    /**
     * @var int $numPage
     */
    private $numPage;
    /**
     * @var Book $book
     */
    private $book;
    /**
     * @var string $text
     */
    private $text;

    /**
     * Page constructor.
     * @param int $idPage
     * @param int $numPage
     * @param Book $book
     * @param string $text
     */
    public function __construct(int $idPage, int $numPage, Book $book, string $text)
    {
        $this->idPage = $idPage;
        $this->numPage = $numPage;
        $this->book = $book;
        $this->text = $text;
    }

    /**
     * @return int
     */
    public function getIdPage(): int
    {
        return $this->idPage;
    }

    /**
     * @param int $idPage
     */
    public function setIdPage(int $idPage): void
    {
        $this->idPage = $idPage;
    }

    /**
     * @return int
     */
    public function getNumPage(): int
    {
        return $this->numPage;
    }

    /**
     * @param int $numPage
     */
    public function setNumPage(int $numPage): void
    {
        $this->numPage = $numPage;
    }

    /**
     * @return Book
     */
    public function getBook(): Book
    {
        return $this->book;
    }

    /**
     * @param Book $book
     */
    public function setBook(Book $book): void
    {
        $this->book = $book;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }

    public function __toString() : string
    {
        return "Page : ".$this->getIdPage()." - ".$this->getText();
    }

}
