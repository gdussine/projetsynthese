<?php

namespace ProjetSynthese\Model;

class Book
{
    /* @var int */
    protected $idBook;
    /* @var string */
    protected $title;
    /* @var string */
    protected $abstract;
    /* @var User */
    protected $author;
    /* @var Status */
    protected $status;

    /**
     * Book constructor.
     * @param int $idBook
     * @param string $title
     * @param string $abstract
     * @param User $author
     * @param Status $status
     */
    public function __construct(int $idBook, string $title, string $abstract, User $author, Status $status)
    {
        $this->idBook = $idBook;
        $this->title = $title;
        $this->abstract = $abstract;
        $this->author = $author;
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getIdBook(): int
    {
        return $this->idBook;
    }

    /**
     * @param int $idBook
     */
    public function setIdBook(int $idBook): void
    {
        $this->idBook = $idBook;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getAbstract(): string
    {
        return $this->abstract;
    }

    /**
     * @param string $abstract
     */
    public function setAbstract(string $abstract): void
    {
        $this->abstract = $abstract;
    }

    /**
     * @return User
     */
    public function getAuthor(): User
    {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor(User $author): void
    {
        $this->author = $author;
    }

    /**
     * @return Status
     */
    public function getStatus(): Status
    {
        return $this->status;
    }

    /**
     * @param Status $status
     */
    public function setStatus(Statut $status): void
    {
        $this->status = $status;
    }

    public function __toString()
    {
        return "Book : ".$this->getIdBook()." - ".$this->getTitle();
    }




}