<?php

namespace ProjetSynthese\Model;

use DateTime;

class Comment
{
    /* @var User */
    protected $author;
    /* @var Book */
    protected $book;
    /* @var string */
    protected $comment;
    /* @var DateTime */
    protected $date;

    /**
     * Comment constructor.
     * @param User $author
     * @param Book $book
     * @param string $comment
     * @param DateTime $date
     */

    public function __construct(User $author, Book $book, string $comment, DateTime $date)
    {
        $this->author = $author;
        $this->book = $book;
        $this->comment = $comment;
        $this->date = $date;
    }

    /**
     * @return User
     */
    public function getAuthor(): User
    {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor(User $author): void
    {
        $this->author = $author;
    }

    /**
     * @return Book
     */
    public function getBook(): Book
    {
        return $this->book;
    }

    /**
     * @param Book $book
     */
    public function setBook(Book $book): void
    {
        $this->book = $book;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment(string $comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return DateTime
     */
    public function getDate(): DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     */
    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function __toString()
    {
        return "Comment : ".$this->getDate()->format('Y-m-d H-i-s')." - ".$this->getComment();
    }
}