<?php

namespace ProjetSynthese\Model;

class Choice
{
    /**
     * @var int $idChoice
     */
    private $idChoice;
    /**
     * @var string $description
     */
    private $description;
    /**
     * @var Page $SourcePage
     */
    private $sourcePage;
    /**
     * @var Page $destinationPage
     */
    private $destinationPage;

    /**
     * Choice constructor.
     * @param int $idChoice
     * @param string $description
     * @param Page $sourcePage
     * @param $destinationPage
     */
    public function __construct(int $idChoice, string $description, Page $sourcePage, $destinationPage)
    {
        $this->idChoice = $idChoice;
        $this->description = $description;
        $this->sourcePage = $sourcePage;
        $this->destinationPage = $destinationPage;
    }

    /**
     * @return int
     */
    public function getIdChoice(): int
    {
        return $this->idChoice;
    }

    /**
     * @param int $idChoice
     */
    public function setIdChoice(int $idChoice): void
    {
        $this->idChoice = $idChoice;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return Page
     */
    public function getSourcePage(): Page
    {
        return $this->sourcePage;
    }

    /**
     * @param Page $sourcePage
     */
    public function setSourcePage(Page $sourcePage): void
    {
        $this->sourcePage = $sourcePage;
    }

    /**
     * @return Page
     */
    public function getDestinationPage()
    {
        return $this->destinationPage;
    }

    /**
     * @param Page $destinationPage
     */
    public function setDestinationPage(Page $destinationPage): void
    {
        $this->destinationPage = $destinationPage;
    }


    public function __toString()
    {
        return "Choice : ".$this->getIdChoice()." - ".$this->getDescription();
    }


}
