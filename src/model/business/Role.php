<?php

namespace ProjetSynthese\Model;

use ReflectionClass;

class Role
{

    /* @var int */
    protected $idRole;
    /* @var string */
    protected $lbRole;

    const ROLE_GUEST = 'Invité';
    const ROLE_USER = 'Utilisateur';
    const ROLE_ID_USER = 1;
    const ROLE_ID_MODERATOR = 2;
    const ROLE_ID_ADMIN = 3;
    const ROLE_MODERATOR = 'Modérateur';
    const ROLE_ADMIN = 'Administrateur';

    /**
     * Role constructor.
     * @param int $idRole
     * @param string $lbRole
     */
    public function __construct(int $idRole, string $lbRole)
    {
        $this->idRole = $idRole;
        $this->setLbRole($lbRole);
    }

    public static function isLbRoleValid(string $lib){
        foreach ((new ReflectionClass(__CLASS__))->getConstants() as $const) {
            if ($lib == $const)
                return true;
        }
        return false;
    }

    /**
     * @return int
     */
    public function getIdRole(): int
    {
        return $this->idRole;
    }

    /**
     * @param int $idRole
     */
    public function setIdRole(int $idRole): void
    {
        $this->idRole = $idRole;
    }

    /**
     * @return string
     */
    public function getLbRole(): string
    {
        return $this->lbRole;
    }

    public function isRoleAdmin() {
        return $this->idRole==Role::ROLE_ID_ADMIN;
    }

    /**
     * @param string $lbRole
     * @throws InvalidArgumentException
     */
    public function setLbRole(string $lbRole): void
    {

        if (self::isLbRoleValid($lbRole))
            $this->lbRole = $lbRole;
        else
            throw new InvalidArgumentException("Libellé incorrect");
    }

    public function __toString()
    {

        return "Role : ".$this->getIdRole()." - ".$this->getLbRole();
    }


}
