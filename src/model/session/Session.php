<?php

namespace ProjetSynthese\Session;

use ProjetSynthese\Model\User;

class Session
{
    /**
     * @var User user
     */
    private $user;
    /**
     * @var int $currentState
     */
    private $currentState;

    const LOGGED=1;
    const NOT_LOGGED=0;

    /**
     * Session constructor.
     * @param User $user
     * @param int $currentState
     */
    public function __construct($user, $currentState)
    {
        $this->user = $user;
        $this->currentState = $currentState;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function isLogged():bool {
        return $this->currentState==Session::LOGGED;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getCurrentState()
    {
        return $this->currentState;
    }

    /**
     * @param mixed $currentState
     */
    public function setCurrentState($currentState): void
    {
        $this->currentState = $currentState;
    }

    public static function getGuestSession(): Session
    {
        return new Session(User::getGuest(), self::NOT_LOGGED);
    }

    public function loggout():void
    {
        $this->setUser(User::getGuest());
        $this->setCurrentState(Session::NOT_LOGGED);
    }

    public function __toString()
    {
        return "Session : ".$this->user->getLogin()." ".$this->currentState;
    }
}