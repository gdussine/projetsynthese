<?php


namespace ProjetSynthese\Controller;

use ProjetSynthese\Session\Session;

class ControllerError implements Controller
{
    private $smarty;

    public function __construct(SmartyPlus $smarty)
    {
        $this->smarty = $smarty;
        if(!isset($_SESSION['session'])){
            $_SESSION['session'] = Session::getGuestSession();
        }
        $this->smarty->assign("numErr", 404);
        $this->smarty->assign("msgErr", "Cette page n'existe pas..");
        new ControllerNavBar($this->smarty, $_SESSION['session']);

    }

    public function setErr(int  $numErr, string $msgErr){
        $this->smarty->assign("numErr", $numErr);
        $this->smarty->assign("msgErr", $msgErr);
    }

    public function display(){
        $this->smarty->display('page/404.tpl');
    }
}