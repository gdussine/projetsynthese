<?php

namespace ProjetSynthese\Controller;
use ProjetSynthese\Session\Session;
use Smarty;
class SmartyPlus extends Smarty
{
    const STYLE_PATH = '/www/projetsynthese/src/view/style/';
    const SCRIPT_PATH ='/www/projetsynthese/src/view/script/';
    const IMG_PATH = '/www/projetsynthese/src/view/upload/';
    const SRC_PATH = '/www/projetsynthese/src/';

    public function __construct()
    {
        parent::__construct();
        $this->template_dir = __DIR__.'/../templates/';
        $this->compile_dir = __DIR__.'/../templates_c/';
        $this->config_dir = __DIR__.'/../configs/';
        $this->cache_dir = __DIR__.'/../cache/';
        $this->assign('cssDir', self::STYLE_PATH);
        $this->assign('jsDir', self::SCRIPT_PATH);
        $this->assign('imgDir', self::IMG_PATH);
        $this->assign('srcDir', self::SRC_PATH);

    }

    public function constructNavBar($session)
    {
    }

}