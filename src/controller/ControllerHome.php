<?php


namespace ProjetSynthese\Controller;


use ProjetSynthese\Session\Session;

class ControllerHome extends ControllerPage
{

    public function __construct(SmartyPlus $smarty)
    {
        parent::__construct($smarty);
        $this->smarty->assign('home', 'active');
        if (!isset($_SESSION['session'])) {
            $_SESSION['session'] = Session::getGuestSession();
        }
        $session = $_SESSION['session'];
        if ($session->getCurrentState() == Session::LOGGED) {
            $this->smarty->registerObject('user', $_SESSION['session']->getUser());
            $this->smarty->assign('login', $_SESSION['session']->getUser()->getLogin());
            
        } else {
            $this->smarty->assign('login', 'Visiteur');
        }
    }

    public function display(){
        $this->smarty->display('page/home.tpl');
    }
}