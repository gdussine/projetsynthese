<?php


namespace ProjetSynthese\Controller;


use ProjetSynthese\DAO\DAOFactory;
use ProjetSynthese\Model\{User, Role};
use ProjetSynthese\Session\Session;

class ControllerProfil extends ControllerPage
{

    public function __construct(SmartyPlus $smarty)
    {
        parent::__construct($smarty);
        // Espace perso user
        if($_SESSION['session']->getUser()->getRole()->getIdRole() === Role::ROLE_ID_USER){
            $user = $_SESSION['session']->getUser();
            $smarty->assign("id",$user->getIdUser());
            $smarty->assign("username",$user->getLogin());
            $smarty->assign("email",$user->getEmail());
            $smarty->assign("password",$user->getPassword());

            $smarty->assign("countBook", sizeof(DAOFactory::getBookDAO()->getByAuthor($user)));
            $this->smarty->assign('profiladmin', 'active');
        }
        // Espace perso moderateur et administrateur
        elseif ($_SESSION['session']->getUser()->getRole()->getIdRole() === Role::ROLE_ID_MODERATOR || $_SESSION['session']->getUser()->getRole()->getIdRole() === Role::ROLE_ID_ADMIN){
            $user = $_SESSION['session']->getUser();
            $smarty->assign("id",$user->getIdUser());
            $smarty->assign("username",$user->getLogin());
            $smarty->assign("email",$user->getEmail());
            $smarty->assign("password",$user->getPassword());
            $smarty->assign("countUser", sizeof(DAOFactory::getUserDAO()->getAll()));
            $smarty->assign("countBook", sizeof(DAOFactory::getBookDAO()->getAll()));
            $this->smarty->assign('profiladmin', 'active');
        }



   }

    public function display(){
        if ($_SESSION['session']->isLogged() &&  $_SESSION['session']->getUser()->getRole()->getIdRole() === Role::ROLE_ID_USER){
            $this->smarty->display('page/profilUser.tpl');
        }
        elseif ($_SESSION['session']->isLogged() &&  $_SESSION['session']->getUser()->getRole()->getIdRole() === Role::ROLE_ID_ADMIN){
            $this->smarty->display('page/profilModerator.tpl');
        }
        elseif ($_SESSION['session']->isLogged() &&  $_SESSION['session']->getUser()->getRole()->getIdRole() === Role::ROLE_ID_MODERATOR){
            $this->smarty->display('page/profilModerator.tpl');
        }
        else {
            header("Location: /www/projetsynthese/src/index");
        } 


    }
}