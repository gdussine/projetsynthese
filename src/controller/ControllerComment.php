<?php

namespace ProjetSynthese\Controller;
use ProjetSynthese\DAO\DAOFactory;
use ProjetSynthese\Session\Session;

class  ControllerComment extends ControllerPage
{
    public function __construct($smarty, $idBook)
    {
        parent::__construct($smarty);
        $book = DAOFactory::getBookDAO()->getById($idBook);
        $comments = DAOFactory::getCommentDAO()->getByBook($book);
        $FirstPageFromBook = DAOFactory::getPageDAO()->getFirstPageFromBook($book)->getIdPage();
        $this->smarty->assign('FirstPageFromBook', $FirstPageFromBook);
        $this->smarty->assign("idUser", $_SESSION["session"]->getUser()->getIdUser());
        $this->smarty->assign("login", $_SESSION["session"]->getUser()->getlogin());
        $this->smarty->assign('title', $book->getTitle());
        $this->smarty->assign('idBook', $book->getIdBook());
        $this->smarty->assign('author', $book->getAuthor()->getLogin());
        $this->smarty->assign('image', $book->getIdBook().'.jpg');
        $this->smarty->assign('comment', $comments);
    }

    public function display()
    {
        $this->smarty->display('page/comment.tpl');
    }
}
