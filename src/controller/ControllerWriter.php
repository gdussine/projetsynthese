<?php


namespace ProjetSynthese\Controller;


use ProjetSynthese\Session\Session;

class ControllerWriter implements Controller
{
    private $smarty;

    public function __construct(SmartyPlus $smarty, $idBook)
    {
        $this->smarty = $smarty;
        if(!isset($_SESSION['session'])){
            $_SESSION['session'] = Session::getGuestSession();
        }
        $smarty->assign("idBook", $idBook);
        new ControllerNavBar($this->smarty, $_SESSION['session']);

    }

    public function display(){
        $this->smarty->display('page/writer.tpl');
    }
}