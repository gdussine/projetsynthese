<?php


namespace ProjetSynthese\Controller;

use ProjetSynthese\DAO\DAOFactory;
use ProjetSynthese\Session\Session;

class ControllerSignIn implements Controller
{
    private $smarty;

    public function __construct(SmartyPlus $smarty)
    {
        $this->smarty = $smarty;
        if (isset($_POST['usrname']) && isset($_POST['psw'])) {
            $user = DAOFactory::getUserDAO()->getByLog($_POST['usrname'], $_POST['psw']);
            if ($user === 0) {
                $_SESSION['session'] = Session::getGuestSession();
                $_SESSION['session']->getUser()->setLogin($_POST['usrname']);
                $this->smarty->assign('login', 'Visiteur');
            } else {
                $_SESSION["session"] = new Session($user, Session::LOGGED);
                $this->smarty->assign('login', $_SESSION['session']->getUser()->getLogin());

            }
        } else {
            $_SESSION["session"] = Session::getGuestSession();
            $this->smarty->assign('login', 'Visiteur');


        }
        new ControllerIndex($this->smarty);
        
    }

    public function display()
    {
        $this->smarty->display('page/home.tpl');
    }

}