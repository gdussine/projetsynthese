<?php


namespace ProjetSynthese\Controller;


interface Controller
{
    public function display();

}