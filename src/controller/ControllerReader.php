<?php


namespace ProjetSynthese\Controller;


use ProjetSynthese\DAO\DAOFactory;
use ProjetSynthese\Model\Page;
use ProjetSynthese\Session\Session;

class ControllerReader extends ControllerPage
{
    public function __construct(SmartyPlus $smarty)
    {
        parent::__construct($smarty);
    }

    public function setPage(Page $page){
        $listChoice = DAOFactory::getChoiceDAO()->getBySourcePage($page);
        $this->smarty->assign("author", $page->getBook()->getAuthor()->getLogin());
        $this->smarty->assign("title", $page->getBook()->getTitle());
        $this->smarty->assign("listChoice", $listChoice);
        $this->smarty->assign("text", $page->getText());
        $this->smarty->assign('idBook', $page->getBook()->getIdBook());


    }

    public function display(){
        $this->smarty->display('page/reader.tpl');
    }
}