<?php


namespace ProjetSynthese\Controller;


use ProjetSynthese\DAO\DAOFactory;
use ProjetSynthese\Model\User;
use ProjetSynthese\Session\Session;

class ControllerProfilModerator extends ControllerPage
{

    public function __construct(SmartyPlus $smarty)
    {
        parent::__construct($smarty);
        $user = $_SESSION['session']->getUser();
        $smarty->assign("id",$user->getIdUser());
        $smarty->assign("username",$user->getLogin());
        $smarty->assign("email",$user->getEmail());
        $smarty->assign("password",$user->getPassword());
        $smarty->assign("countUser", sizeof(DAOFactory::getUserDAO()->getAll()));
        $smarty->assign("countBook", sizeof(DAOFactory::getBookDAO()->getAll()));
        $this->smarty->assign('profiladmin', 'active');

    }

    public function display(){
        if ($_SESSION['session']->isLogged() &&  $_SESSION['session']->getUser()->isAdmin()){
            $this->smarty->display('page/profilModerator.tpl');
        }
        else {
            header("Location: ../index");
        }


    }
}