<?php


namespace ProjetSynthese\Controller;


use ProjetSynthese\Session\Session;

class ControllerNavBar implements Controller
{
    /**
     * @var SmartyPlus $smarty ;
     */
    private $smarty;

    /**
     * ControllerNavBar constructor.
     * @param SmartyPlus $smarty
     * @throws \SmartyException
     */
    public function __construct(SmartyPlus $smarty)
    {
        $this->smarty = $smarty;
        if (!isset($_SESSION['session'])) {
            $_SESSION['session'] = Session::getGuestSession();
        }
        $session = $_SESSION['session'];
        if ($session->getCurrentState() == Session::LOGGED) {
            $this->smarty->registerObject('user', $_SESSION['session']->getUser());
            $this->smarty->assign('navbarBtnSignUp', $_SESSION['session']->getUser()->getLogin());
            $this->smarty->assign('navBarBtnSignIn', 'Sign Out');
            $this->smarty->assign('navBarFuncSignUp', 'profil');
            $this->smarty->assign('navBarFuncSignIn', 'signOut');
            $this->smarty->assign('profil', 'cursor:default; pointer-events:none;');
            $this->smarty->assign('show', '');
            $this->smarty->assign('showInverse', 'style="display:none;"');

            
        } else {
            $this->smarty->assign('navbarBtnSignUp', 'Sign Up');
            $this->smarty->assign('navBarBtnSignIn', 'Sign In');
            $this->smarty->assign('navBarFuncSignUp', 'signUp');
            $this->smarty->assign('navBarFuncSignIn', 'signIn');
            $this->smarty->assign('profil', '');
            $this->smarty->assign('show', 'style="display:none;"');
            $this->smarty->assign('showInverse', '');

          
        }
        $this->smarty->assign('home', '');
        $this->smarty->assign('creator', '');
        $this->smarty->assign('category', '');
        $this->smarty->assign('profiladmin', '');
    }

    public function display()
    {

    }

}