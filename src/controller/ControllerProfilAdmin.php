<?php

namespace ProjetSynthese\Controller;

use ProjetSynthese\DAO\DAOFactory;
use ProjetSynthese\Model\Role;
use ProjetSynthese\Model\User;
use ProjetSynthese\Controller\ControllerPage;
use ProjetSynthese\Session\Session;
use ProjetSynthese\Controller\SmartyPlus;


class ControllerProfilAdmin extends ControllerPage

{
    private $id;

    public function __construct(SmartyPlus $smarty, $id)
    {
        parent::__construct($smarty);
        $this->id = $id;
        // Espace perso user
        if ($_SESSION['session']->getUser()->getRole()->getIdRole() > Role::ROLE_ID_USER) {
            $user = $_SESSION['session']->getUser();
            $userView = DAOFactory::getUserDao()->getById($id);
            if ($userView != null && $userView->getRole()->getIdRole() == Role::ROLE_ID_USER) {
                $smarty->assign("id", $userView->getIdUser());
                $smarty->assign("username", $userView->getLogin());
                $smarty->assign("email", $userView->getEmail());
                $smarty->assign("password", $userView->getPassword());
                $smarty->assign("countBook", sizeof(DAOFactory::getBookDAO()->getByAuthor($userView)));
                $this->smarty->assign('profiladmin', 'active');
            }
        }
    }
    public
    function display()
    {
        if ($_SESSION['session']->getUser()->getRole()->getIdRole() > Role::ROLE_ID_USER) {
            $userView = DAOFactory::getUserDao()->getById($this->id);
            if ($userView != null && $userView->getRole()->getIdRole() == Role::ROLE_ID_USER) {
                $this->smarty->display('page/profilUser.tpl');
                die();
            }
        }
        header('Location: /www/projetsynthese/src/index');
    }
}
