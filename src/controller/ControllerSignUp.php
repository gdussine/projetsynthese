<?php


namespace ProjetSynthese\Controller;

use ProjetSynthese\DAO\DAOFactory;
use ProjetSynthese\Model\Role;
use ProjetSynthese\Session\Session;
use ProjetSynthese\Model\User;

class ControllerSignUp implements Controller
{
    private $smarty;

    public function __construct(SmartyPlus $smarty)
    {
        $this->smarty = $smarty;
        if (isset($_POST['usrname']) && isset($_POST['email']) && isset($_POST['psw']) && isset($_POST['psw2'])) {
            if (($_POST['psw']) == ($_POST['psw2'])) {
                $role = DAOFactory::getRoleDAO()->getById(Role::ROLE_ID_USER);
                $user = new User(-1, $_POST['usrname'],$_POST['email'], $_POST['psw'], $role);
                DAOFactory::getUserDAO()->create($user);
                //verfication mail à faire
                $_SESSION["session"] = new Session($user, Session::LOGGED);
            }
        }
        
        new ControllerIndex($smarty);
    }

    public
    function display()
    {
        $this->smarty->display('page/home.tpl');
    }
}