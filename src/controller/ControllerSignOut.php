<?php

namespace ProjetSynthese\Controller;

use ProjetSynthese\Session\Session;

class ControllerSignOut implements Controller
{
    private $smarty;

    public function __construct(SmartyPlus $smarty)
    {
        session_destroy();
        $this->smarty = $smarty;
        $_SESSION['session'] = Session::getGuestSession();
        new ControllerIndex($this->smarty);
        $this->smarty->assign('login', 'Visiteur');
        
    }

    public function display(){
        $this->smarty->display('page/home.tpl');
    }
}
