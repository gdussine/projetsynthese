<?php


namespace ProjetSynthese\Controller;

use ProjetSynthese\Session\Session;
use ProjetSynthese\DAO\DAOFactory;

class ControllerCategory implements Controller
{
    private $smarty;

    public function __construct(SmartyPlus $smarty)
    {
        
        $this->smarty = $smarty;
        if(!isset($_SESSION['session'])){
            $_SESSION['session'] = Session::getGuestSession();
        }

        new ControllerNavBar($this->smarty, $_SESSION['session']);
        $this->smarty->assign('category', 'active');

        
    }

    public function display(){
        $this->smarty->display('page/category.tpl');
    }
}