<?php


namespace ProjetSynthese\Controller;


use ProjetSynthese\Session\Session;

abstract class ControllerPage implements Controller
{
    protected $smarty;

    public function __construct(SmartyPlus $smarty)
    {
        $this->smarty = $smarty;
        if(!isset($_SESSION['session'])){
            $_SESSION['session'] = Session::getGuestSession();
        }
        new ControllerNavBar($this->smarty, $_SESSION['session']);
    }

}