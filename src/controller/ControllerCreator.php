<?php


namespace ProjetSynthese\Controller;

use ProjetSynthese\Session\Session;
use ProjetSynthese\DAO\DAOFactory;
use ProjetSynthese\Model\Book;

class ControllerCreator implements Controller
{
    private $smarty;

    public function __construct(SmartyPlus $smarty)
    {
        
        $this->smarty = $smarty;
        if(!isset($_SESSION['session'])){
            $_SESSION['session'] = Session::getGuestSession();
        }

        $books = DAOFactory::getBookDAO()->getByAuthor($_SESSION["session"]->getUser());
        $this->smarty->assign("author", $_SESSION["session"]->getUser()->getIdUser());
        new ControllerNavBar($this->smarty, $_SESSION['session']);
        $this->smarty->assign('creator', 'active');

        
    }

    public function display(){
        $this->smarty->display('page/creator.tpl');
    }
}