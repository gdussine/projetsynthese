<?php


namespace ProjetSynthese\Controller;


use ProjetSynthese\Session\Session;

class ControllerIndex extends ControllerPage
{

    public function __construct(SmartyPlus $smarty)
    {
        parent::__construct($smarty);
    }

    public function display(){
        $this->smarty->display('page/accueil.tpl');
    }
}