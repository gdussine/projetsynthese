<?php
require '../vendor/autoload.php';
session_start();

use ProjetSynthese\Controller\{
    ControllerHome,
    ControllerIndex,
    ControllerSignIn,
    ControllerSignUp,
    ControllerSignOut,
    SmartyPlus,
    ControllerError,
    ControllerReader,
    ControllerWriter,
    ControllerCategory,
    ControllerCreator,
    ControllerProfilUser,
    ControllerProfil,
    ControllerProfilModerator,
    ControllerProfilAdmin,
    ControllerComment

};
use ProjetSynthese\DAO\DAOFactory;
use Phroute\Phroute\RouteCollector;
use Phroute\Phroute\Dispatcher;


$collector = new RouteCollector();

$collector->get('home', function () {
    (new ControllerHome(new SmartyPlus()))->display();
    die;
});

$collector->get('index', function () {
    (new ControllerHome(new SmartyPlus()))->display();
    die;
});
$collector->get('profil/admin', function () {
    (new ControllerProfilAdmin(new SmartyPlus()))->display();
    die;
});

$collector->get('signin', function () {
    (new ControllerSignIn(new SmartyPlus()))->display();
    die;
});
$collector->get('signup', function () {
    (new ControllerSignUp(new SmartyPlus()))->display();
    die;
});
$collector->get('signout', function () {
    (new ControllerSignOut(new SmartyPlus()))->display();
    die;
});
$collector->get('creator', function () {
    (new ControllerCreator(new SmartyPlus()))->display();
    die;
});
$collector->get('category', function () {
    (new ControllerCategory(new SmartyPlus()))->display();
    die;
});
$collector->get('profil', function () {

    (new ControllerProfil(new SmartyPlus()))->display();
    die;
});
$collector->get('profil/{id:\d+}', function ($id) {
    
    (new ControllerProfilAdmin(new SmartyPlus(),$id))->display();
    die;
});
$collector->get('profil/moderator', function () {
    (new ControllerProfilModerator(new SmartyPlus()))->display();
    die;
});
$collector->group(array('prefix' => 'read'), function (RouteCollector $collector) {
    $collector->get('{book:\d+}', function ($book) {
        $controller = (new ControllerComment(new SmartyPlus(), $book));
        $controller->display();
        die;
    });
    $collector->get('{book:\d+}/{page:\d+}', function ($book, $page) {
        $controller = (new ControllerReader(new SmartyPlus()));
        $controller->setPage(DAOFactory::getPageDAO()->getById($page));
        $controller->display();
        die;
    });
});
$collector->group(array('prefix' => 'write'), function (RouteCollector $collector) {
    $collector->get('{book:\d+}', function ($book) {
        $controller = (new ControllerWriter(new SmartyPlus(), $book));
        $controller->display();
        die;
    });
});
$collector->get('error/{code:\d+}', function ($code) {
    (new ControllerError(new SmartyPlus()))->display();
    die;
});



$uri = substr($_SERVER['REQUEST_URI'], 24);
if ($uri == '' || $uri == 'index.php') {
    $uri = 'home';
}
$dispatcher = new Dispatcher($collector->getData());
try{
    $dispatcher->dispatch('GET', $uri);
}catch (Exception $e){
    $dispatcher->dispatch('GET', 'error/500');
}

//$url = '';
//if (isset($_GET['url'])) {
//    $url = $_GET['url'];
//}
//try {
//    switch ($url) {
//        case '':
//        case '/':
//        case 'index':
//        case 'index.php':
//            (new ControllerIndex(new SmartyPlus()))->display();
//            die;
//        case 'signin':
//            (new ControllerSignIn(new SmartyPlus()))->display();
//            die;
//        case 'signup':
//            (new ControllerSignUp(new SmartyPlus()))->display();
//            die;
//        case 'signout':
//            (new ControllerSignOut(new SmartyPlus()))->display();
//            die;
//        case 'read':
//            $idPage = $_GET["page"];
//            $idBook = $_GET["book"];
//            $controller = new ControllerReader(new SmartyPlus());
//            $controller->setPage(DAOFactory::getPageDAO()->getById($idPage));
//            $controller->display();
//            die;
//        case 'write':
//            (new ControllerWriter(new SmartyPlus()))->display();
//            die;
//        case 'creator':
//            (new ControllerCreator(new SmartyPlus()))->display();
//            die;
//        default:
//            (new ControllerError(new SmartyPlus()))->display();
//            die;
//    }
//} catch (Exception $e) {
//    $ce = new ControllerError(new SmartyPlus());
//    $ce->setErr(($e->getCode() == 0 ? 500 : $e->getCode()), $e->getMessage());
//    $ce->display();
//}
