<?php
require '../vendor/autoload.php';
use ProjetSynthese\DAO\DAOFactory;
use ProjetSynthese\Model\Book;
use ProjetSynthese\Model\Status;
use ProjetSynthese\Model\User;

/**
 * Renvoie tous les livres
 */
$books = DAOFactory::getBookDAO()->getAll();

$listBooks = [];
foreach ($books as $book) {
    $bookInArray = null;
    $user = DAOFactory::getUserDAO()->getById($book->getAuthor()->getIdUser());
    $userLogin = $user->getLogin();
    if (DAOFactory::getBookDAO()->getNbPageFromBook($book) >0) {
        $bookInArray = [
            'idBook' => $book->getIdBook(),
            'title' => ucfirst($book->getTitle()),
            'abstract' => $book->getAbstract(),
            'status' => $book->getStatus()->getIdStatus(),
            'login' => $userLogin,
            'idFirstPage' => DAOFactory::getPageDAO()->getFirstPageFromBook($book)->getIdPage()
        ];
        array_push($listBooks, $bookInArray);
    }
}
echo json_encode($listBooks);

