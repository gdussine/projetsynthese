{* Smarty *}

{*
 * NavBar
*}

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="{$home} navbar-brand" href="{$srcDir}home">Soyez un héros !</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="{$category}"><a href="{$srcDir}category">Lecture</a></li>
            <li class="{$creator}" {$show} ><a href="{$srcDir}creator">Ecriture</a></li>
            <li class="{$profiladmin}" {$show}><a href="{$srcDir}profil">Espace perso</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <form class="navbar-form navbar-left" action="/action_page.php">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
            </form>
            <li>
                <a href="#" id="btnSignUp" onclick="{$navBarFuncSignUp}(event)" style="{$profil}">
                    <span class="glyphicon glyphicon-user"></span> {$navbarBtnSignUp}</a>
            </li>
            <li><a href="#" id="btnSignIn" onclick="{$navBarFuncSignIn}(event)"><span
                            class="glyphicon glyphicon-log-in"></span> {$navBarBtnSignIn}</a></li>
        </ul>
    </div>
</nav>

{*
 * Modal Sign Up
*}

<div class="modal fade" id="ModalSignUp" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4><span class="glyphicon glyphicon-home"></span> S'inscrire à NomSite</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="{$srcDir}signup">
                    <div class="form-group">
                        <label for="usrname"><span class="glyphicon glyphicon-user"></span> Identifiant</label>
                        <input type="text" class="form-control" name="usrname" placeholder="Entrez votre pseudo">
                    </div>
                    <div class="form-group">
                        <label for="usrname"><span class="glyphicon glyphicon-user"></span> E-mail</label>
                        <input type="text" class="form-control" name="email"
                               placeholder="Entrez votre adresse mail">
                    </div>
                    <div class="form-group">
                        <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> Mot de passe</label>
                        <input type="password" class="form-control" name="psw" placeholder="Entrez votre mot de passe">
                    </div>
                    <div class="form-group">
                        <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> Confirmer le mot de
                            passe</label>
                        <input type="password" class="form-control" name="psw2" placeholder="Entrez votre mot de passe">
                    </div>
                    <button type="submit" class="btn-signin btn btn-block"><span
                                class="glyphicon glyphicon-off"></span>
                        S'inscrire
                    </button>
                </form>
            </div>
            <div class="modal-footer">
                <p>Vous possédez déjà un compte ? <a href="#" id="ModalSignUpToSignIn"
                                                     onclick="signUpToSignIn(event)">Identifiez-vous !</a></p>

            </div>
        </div>
    </div>
</div>

{*
 * Modal Sign Out
*}

<div class="modal fade" id="ModalSignOut" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4><span class=" glyphicon glyphicon-log-out "></span> Se déconnecter</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="{$srcDir}signout">
                    <button type="submit" class="btn-signin btn btn-block"><span
                                class="glyphicon glyphicon-off"></span>
                        Deconnexion
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>

{*
 * Modal Sign In
*}

<div class="modal fade" id="ModalSignIn" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4><span class="glyphicon glyphicon-lock"></span> Se connecter à NomSite</h4>
            </div>
            <div class="modal-body">
                <div class="modal-body">
                    <form role="form" method="post" action="{$srcDir}signin">
                        <div class="form-group">
                            <label for="usrname"><span class="glyphicon glyphicon-user"></span> Identifiant</label>
                            <input type="text" class="form-control" name="usrname" value="{*$login*}"
                                   placeholder="Entrez votre pseudo">
                        </div>
                        <div class="form-group">
                            <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> Mot de passe<a href="#">
                                    Mot de
                                    passe oublié ?</a></label>
                            <input type="password" class="form-control" value="" name="psw"
                                   placeholder="Entrez votre mot de passe">
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox" checked>Maintenir la connexion</label>
                        </div>
                        <button type="submit" class="btn-signin btn btn-block"><span
                                    class="glyphicon glyphicon-off"></span>
                            Connexion
                        </button>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <p>Vous n'avez pas encore de compte ? <a href="#" id="ModalSignInToSignUp"
                                                         onclick="signUpToSignUp(event)">Créez-en un !</a></p>
            </div>
        </div>
    </div>
</div>


