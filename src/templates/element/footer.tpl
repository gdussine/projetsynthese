<footer class="page-footer font-small blue pt-4">
    <div class="retour-en-haut text-center py-3">
    </div>
    <div class="footer-content container-fluid text-center text-md-left">
        <div class="row">
            <div class="col-md-6 mt-md-0 mt-3">
                <h5 class="text-uppercase">Livres dont vous êtes le héros</h5>
                <p class="text-description">Les « livres dont vous êtes le héros » sont un genre de romans qui ont
                    la
                    particularité d’être interactif, le lecteur
                    peut effectuer des choix pendant la lecture qui changent le déroulement de l’histoire.
                </p>
            </div>
            <div class="col-md-3 mb-md-0 mb-3">
                <h5 class="text-uppercase">Suivez-nous</h5>

                <ul class="list-unstyled">
                    <li>
                        <a href="#!">Facebook</a>
                    </li>
                    <li>
                        <a href="#!">Twitter</a>
                    </li>
                    <li>
                        <a href="#!">Instagram</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 mb-md-0 mb-3">
                <h5 class="text-uppercase">Besoin d'aide</h5>

                <ul class="list-unstyled">
                    <li>
                        <a href="#!">Qui somme nous ?</a>
                    </li>
                    <li>
                        <a href="#!">FAQ</a>
                    </li>
                    <li>
                        <a href="#!">contactez-nous</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="footer-copyright text-center py-3">© 2019 Copyright:
        <a href="https://mdbootstrap.com/education/bootstrap/"> Livresdontvousetesleheros.com</a>
    </div>
</footer>
