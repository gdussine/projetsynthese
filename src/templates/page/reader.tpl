{extends file="../canvas/model.tpl"}
{block name=css append}
    <link rel="stylesheet" href="{$cssDir}reader.css">
	<link href="https://fonts.googleapis.com/css?family=Audiowide&display=swap" rel="stylesheet">
{/block}
{block name=content}

<div class="container mainPage">
    <div class="row">
        <div class="col-sm-12 menuLecture">
            <h1>{$title}</h1>
        </div>
        <div class="col-sm-12 itemLecture">
            <ul>
                <a href='/www/projetsynthese/src/category'><li>Fermer le livre</li></a>
                <a href='/www/projetsynthese/src/read/{$idBook}'><li>Commentaires</li></a>
                <li>Résumé</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <div class="row">
                <div class="text-center cover col-sm-12">
                    <img class="img-responsive coverBook" src="{$imgDir}{$idBook}.jpg">
                </div>
                <div class="col-sm-12">
                    <span>Auteur : {$author}</span>
                </div>
                <div class="col-sm-12">
                    <span class="etoile">‎★‎★‎★‎★‎★</span>
                </div>
            </div>
        </div>

        <div class="col-sm-8">
            <div class="page">
                <div class="text"><p>
                        {$text}
                    </p></div>
                <div class="choix">
                    {foreach from=$listChoice item=$choice}
                    <a href="{$choice->getDestinationPage()->getIdPage()}">
                        <button  type="button" class="btn-choix" onclick="">{$choice->getDescription()}
                        </button></a>
                    {/foreach}
                </div>
            </div>

        </div>
    </div>
</div>
{/block}