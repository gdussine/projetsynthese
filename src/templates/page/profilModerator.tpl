{extends file="../canvas/model.tpl"}
{block name=css append}
    <link rel="stylesheet" href="{$cssDir}profilModerator.css">
{/block}
{block name=content}

<div class="container mainPage">
    <div class="row">
        <div class="col-sm-12 menuHeader">
            <h1>Votre compte {$username}</h1>
        </div>
    </div>
       <div>
            <p>  </p>
    </div>
 
    <div class="row">


        <div class="col-sm-12">
    <div id="idUser" data-id="{$id}" hidden> </div> 
        <table class="table table-striped">



        <tbody>
            <tr>
                <th>Utilisateurs inscrits</th>
                <td>{$countUser}</td>

            </tr>
            <tr>
                <th>Livres</th>
                <td>{$countBook}</td>

            </tr>
          
        </tbody>
    </table>
        </div>


             <div class="col-sm-12">
           <div class="card">
        <h3 class="card-header text-center font-weight-bold text-uppercase py-4">Utilisateurs</h3>
        <div class="card-body">
            <div id="table" class="table-editable">
                <table class="table table-bordered table-responsive-md table-striped text-center">
                    <thead>
                        <tr>
                            <th class="text-center">Pseudo</th>
                            <th class="text-center">Livres</th>
                        </tr>
                    </thead>


                    <tbody class="tbody"> 
                    </tbody>
                </table>
            </div>
        </div>
    </div>
        </div>



    </div>
</div>
{/block}
{block name=script append}
    <script src="{$jsDir}profilModerator.js"></script>
{/block}
