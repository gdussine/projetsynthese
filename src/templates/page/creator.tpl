{* Smarty *}

{extends file="../canvas/model.tpl"}
{block name=css append}
    <link rel="stylesheet" href="{$cssDir}creator.css">
{/block}
{block name="content"}
  <div class="modal fade" id="confirm-delete" role="dialog">
      <div class="modal-dialog">
          <div class="modal-content">
          
              <div class="modal-header" style="padding:20px 30spx;">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4><span class="glyphicon glyphicon-trash"></span> SUPPRIMER</h4>
                </div>
          
              <div class="modal-body">
                  <p>Êtes-vous sûr de vouloir supprimer ce livre ?</p>
              </div>
              
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                  <button type="button" class="btn-modal-delete btn btn-danger btn-ok" data-dismiss="modal" data-id="">supprimer</button>
              </div>
          </div>
      </div>
  </div>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4><span class="glyphicon glyphicon-book"></span> Création livre</h4>
            </div>
            <div class="modal-body">
              <div class="modal-body">
                <form id="myForm">
                    <div class="form-group">
                        <label for="titre"><span class="glyphicon glyphicon-pencil"></span> Titre</label>
                        <input id='titre' type="text" class="form-control" name="titre" placeholder="Entrez le titre de votre livre">
                    </div>
                    <div class="form-group">
                        <label for="image"><span class="glyphicon glyphicon-picture"></span> Image</label>
                        <input type="file" class="form-control" name="image"  accept="image/jpg">
                    </div>


                    <button class="addBook btn btn-signin btn-block" data-dismiss="modal"><span
                            class="glyphicon glyphicon-ok"></span>
                        Créer
                    </button>
                </form>
                    </div>
            </div>
            <div class="modal-footer-create modal-footer">
                <p class="NombresDeLivres"></p>
            </div>
        </div>
    </div>
</div>
<div id="author" data-author={$author} hidden></div>
   <div class="col-sm-12 menuLecture">
            <h1 class="NombresDeLivres"></h1>
        </div>
<div class="main">
<div class="container">
 <div class="row liste-livre">
        <div id="btn-ajouter-livre" class="col-sm-6 col-md-6 col-lg-4 no-float livre">
            <div class="btn-add btn-ajouter">
                <button type="button" class="btn-supprimer-livre btn btn-default btn-lg" data-toggle="modal" data-target="#myModal">
                    <i class="btn-ajouter-livre glyphicon glyphicon-plus" aria-hidden="true"></i>
                  </button>
            </div>
          </div>
    </div>
  </div>
  </div>
</div>
{/block}
{block name=script append}
    <script src="{$jsDir}creator.js"></script>
{/block}

