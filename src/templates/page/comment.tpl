{extends file="../canvas/model.tpl"}
{block name=css append}
    <link rel="stylesheet" href="{$cssDir}comment.css">
    <script src="https://rawgit.com/jackmoore/autosize/master/dist/autosize.min.js"></script>
{/block}
{block name=content}
<div id="idUser" data-idUser={$idUser} hidden></div>
<div id="idBook" data-idBook={$idBook} hidden></div>
<div class="container mainPage">
    <div class="row">
        <div class="col-sm-12 menuLecture">
            <h1>{$title}</h1>
        </div>
        <div class="col-sm-12 itemLecture">
            <ul>
                <a href='/www/projetsynthese/src/read/{$idBook}/{$FirstPageFromBook}'><li>Lecture</li></a>
                <li>Résumé</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <div class="row">
                <div class="text-center cover col-sm-12">
                    <img class="img-responsive coverBook" src='../view/upload/{$image}'>
                </div>
                <div class="col-sm-12">
                    <span>Auteur : {$author}</span>
                </div>
                <div class="col-sm-12">
                    <span class="etoile">‎★‎★‎★‎★‎★</span>
                </div>
            </div>
        </div>

        <div class="col-sm-8">
            <h2>Commentaires</h2>
            <div class="row">
                
                 <div class="add-comment col-md-12">
                    <div class="profil">
                    <span>{$login}</span> 
                    </div>
                    <div class="md-form">
                        <p {$showInverse}>Vous devez être <a onclick="signIn(event)">connecté</a> pour pouvoir poster un commentaire...</p>
                        <textarea {$show} placeholder="Ajouter un commentaire..." name="comment" form="form" class="md-textarea form-control" rows="1"></textarea>
                    </div>
                    <div class="formulaire">
                    <form id="form" style="float: right;">
                       <button type="button" class="btn-ajouter-commentaire btn btn-primary">Ajouter un commentaire</button>
                    </form>
                       <button class="btn-annuler btn btn-light" style="float: right;" >Annuler</button>
                    </div>
                    
                </div>
            </div>

        </div>
    </div>
</div>
{/block}
{block name=script append}
    <script src="{$jsDir}comment.js"></script>
{/block}