    
    {* Smarty *}

{extends file="../canvas/model.tpl"}
{block name=css append}
    <link rel="stylesheet" href="{$cssDir}category.css">
{/block}
{block name="content"}
     <div class="main header-main">
        <h1> CATÉGORIE </h1>
    </div>
    <div class="main">
    <div class="container">
        <div class="row-main row-left">
            <div class="col-md-2 no-float main-left">
                <div class="main-left-livre div-left">
                    <div class="categ-livre categ-left">LIVRES</div>
                    <div class="slide-left">
                        <ul class="ul-left ul-livre">
                            <li><a href="">Nouveautés</a></li>
                            <li><a href="">Tendances</a></li>
                            <li><a href="">Comedie</a></li>
                            <li><a href="">Drame</a></li>
                            <li><a href="">Action</a></li>
                            <li><a href="">Aventures</a></li>
                            <li><a href="">Thriller</a></li>
                            <li><a href="">Fantastique</a></li>
                            <li><a href="">Science-fiction</a></li>
                            <li><a href="">Horreur</a></li>
                        </ul>
                    </div>
                </div>
                <div class="main-left-manga div-left">
                    <div class="categ-manga categ-left">MANGA</div>
                    <div class="slide-left">
                        <ul class="ul-left ul-manga">
                            <li><a href="">Nouveautés</a></li>
                            <li><a href="">Tendances</a></li>
                            <li><a href="">Comedie</a></li>
                            <li><a href="">Drame</a></li>
                            <li><a href="">Action</a></li>
                            <li><a href="">Aventures</a></li>
                            <li><a href="">Thriller</a></li>
                            <li><a href="">Fantastique</a></li>
                            <li><a href="">Science-fiction</a></li>
                            <li><a href="">Horreur</a></li>
                        </ul>
                    </div>
                </div>
                <div class="main-left-comics div-left">
                    <div class="categ-comics categ-left">COMICS</div>
                    <div class="slide-left">
                        <ul class="ul-left ul-comics">
                            <li><a href="">Nouveautés</a></li>
                            <li><a href="">Tendances</a></li>
                            <li><a href="">Comedie</a></li>
                            <li><a href="">Drame</a></li>
                            <li><a href="">Action</a></li>
                            <li><a href="">Aventures</a></li>
                            <li><a href="">Thriller</a></li>
                            <li><a href="">Fantastique</a></li>
                            <li><a href="">Science-fiction</a></li>
                            <li><a href="">Horreur</a></li>
                        </ul>
                    </div>
                </div>
                <div class="main-left-bds div-left">
                    <div class="categ-bds categ-left">BDS</div>
                    <div class="slide-left">
                        <ul class="ul-left ul-bds">
                            <li><a href="">Nouveautés</a></li>
                            <li><a href="">Tendances</a></li>
                            <li><a href="">Comedie</a></li>
                            <li><a href="">Drame</a></li>
                            <li><a href="">Action</a></li>
                            <li><a href="">Aventures</a></li>
                            <li><a href="">Thriller</a></li>
                            <li><a href="">Fantastique</a></li>
                            <li><a href="">Science-fiction</a></li>
                            <li><a href="">Horreur</a></li>
                        </ul>
                    </div>
                </div>
                <div class="slogan-left categ-left div-left">Lire, écrire, parler, voyager</div>
            </div>
            
            <div class="col-md-10 no-float main-right">
                
                <div class="row">
                    <div class="col-md-12  vcenter main-right-top">
                        <div class="button-toggle">
                            <a class="animer" href="#">
                                <i class="glyphicon glyphicon-triangle-left" aria-hidden="true"></i>
                            </a></div>
                        <p>
                            <a href="#a">A</a> - <a href="#b">B</a> - <a href="#c">C</a> - <a href="#d">D</a> - <a
                                href="#e">E</a> -
                            <a href="#f">F</a> -
                            <a href="#g">G</a> - <a href="#h">H</a> - <a href="#i">I</a> - <a href="#j">J</a> - <a
                                href="#k">K</a> -
                            <a href="#l">L</a> -
                            <a href="#m">M</a> - <a href="#n">N</a> - <a href="#o">O</a> - <a href="#p">P</a> - <a
                                href="#q">Q</a> -
                            <a href="#r">R</a> -
                            <a href="#s">S</a> - <a href="#t">T</a> - <a href="#u">U</a> - <a href="#v">V</a> - <a
                                href="#w">W</a> -
                            <a href="#x">X</a> -
                            <a href="#y">Y</a> - <a href="#z">Z</a>
                        </p>
                    </div>
                </div>

                    <div id="add-books" class="container-livres row">
                    

                </div>
            </div>
        </div>
    </div>
    </div>
    {/block}
{block name=script append}
    <script src="{$jsDir}category.js"></script>
{/block}
