{extends file="../canvas/model.tpl"}
{block name=css append}
    <link rel="stylesheet" media="screen and (min-width:721px)" href="{$cssDir}profilAdmin.css"/>
{/block}

{block name=content}
    <section class="titre">
        <div class="container">
            <div class="col-lg-12">
                <h3 class="text-center">Bienvenue {$username} !</h3>
                <p>{$countBook} livres écris</p>
            </div>
        </div>
    </section>
    <section class="action">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="fenetre">
                        <h3 class="text-center">Supprimer un livre</h3>

                        <ul>
                            <li><p class="text-center">Vous souhaitez supprimer un livre ?</p></li>
                            <li><input id="inputBook" class="text-center"  type="text" placeholder="Titre du livre"></li>
                        </ul>

                    </div>
                </div>


                <div class="col-lg-offset-2 col-lg-5">
                    <div class="fenetre">
                        <h3 class="text-center">Supprimer un utilisateur</h3>
                        <ul>
                            <li><p class="text-center">Vous souhaitez supprimer un utilisateur ?</p></li>
                            <li><input id="inputUser" class="text-center" onkeyup="chooseUser(event)" type="text" placeholder="Nom de l'utilisateur"></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <table>
                        <tr>
                            <td class="name">Lucas</td>
                            <td class="mail">jps@gmail.com</td>
                            <td class="delete">&times;</td>
                        </tr>

                        <tr>
                            <td class="name">Lucas</td>
                            <td class="mail">jps@gmail.com</td>
                            <td class="delete">&times;</td>
                        </tr>

                        <tr>
                            <td class="name">Lucas</td>
                            <td class="mail">jps@gmail.com</td>
                            <td class="delete">&times;</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
{/block}

{block name=script append}
    <script src="{$jsDir}profilAdmin.js"></script>
{/block}