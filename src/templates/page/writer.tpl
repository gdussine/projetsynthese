{* Smarty *}

{extends file="../canvas/model.tpl"}
{block name=css append}
    <link rel="stylesheet" href="{$cssDir}writer.css">
{/block}
{block name="content"}
    <div class="stockage" data-book="{$idBook}" hidden></div>
       <div class="col-sm-12 menuLecture">
            <h1 class="NombresDeLivres">Écriture</h1>
        </div>
    <div class="main">
    <div class="container mainPage">
        <div class="row-main">
            <div class="col-md-2 no-float main-left">
                <div class="text-view">

                    <div id="div-view" class="row">

                        <button type="button" class="btn-ajouter-page btn btn-primary btn-md btn-block"
                                data-field="div-view"
                                data-summernote="summernote"><span class="glyphicon glyphicon-plus"></span></button>

                    </div>
                </div>

            </div>

            <div class="col-md-8 no-float" id="main-center">
                <div id="edit-text">
                                      <div class="editeur-text" id="summernote"></div>
                    <input class="editeur-text" id="choix-1" type="text" placeholder="Entrez le choix n°1...">
                    <select id="page-choix-1"></select>
                    <input class="editeur-text" id="choix-2" type="text" placeholder="Entrez le choix n°2...">
                    <select id="page-choix-2"></select>
                    <input class="editeur-text" id="choix-3" type="text" placeholder="Entrez le choix n°3...">
                    <select id="page-choix-3"></select>
                    <input class="editeur-text" id="choix-4" type="text" placeholder="Entrez le choix n°4...">
                    <select id="page-choix-4"></select>
                </div>
            </div>
            <div class="col-md-2 no-float main-right">
                <div class="text-view">
                    <div class="row div-btn">
                        <button type="button" class="btn-enregistrer-page btn btn-primary btn-md btn-block">Enregistrer
                            la
                            page
                        </button>
                        <button type="button" class="btn-supprimer-page btn btn-danger btn-md btn-block">Supprimer la
                            page
                        </button>
                        <button type="button" class="btn btn-success btn-md btn-block">Enregistrer le livre</button>

                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>
{/block}
{block name=script append}
    <script src="{$jsDir}writer.js"></script>
{/block}

