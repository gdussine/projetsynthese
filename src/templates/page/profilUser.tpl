{extends file="../canvas/model.tpl"}
{block name=css append}
    <link rel="stylesheet" href="{$cssDir}profilUser.css">
{/block}
{block name=content}

<div class="container mainPage">
    <div class="row">
        <div class="col-sm-12 menuHeader">
            <h1>Votre compte {$username}</h1>
        </div>
    </div>
       <div id="success" class="alert alert-success" role="alert">
            <p> votre compte a été modifié </p>
    </div>
 
    <div class="row">


        <div class="col-sm-12">
    <div id="idUser" data-id="{$id}" hidden> </div> 
    <h3 class="h3 title-client card-header text-center font-weight-bold text-uppercase py-4">DÉTAILS PERSONNELS</h3>
    </div>
    <div class="items">
        <form id="myForm" role="form" method="post" action="" enctype="multipart/form-data">
            <div class="form-group">
                <label for='login'>Login</label>
                <input type="text" class="form-control" name="login" value="{$username}">
            </div>
            <div class="form-group">
                <label for="email">E-mail</label>
                <input type="mail" class="form-control" name="email" value="{$email}">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" value="{$password}">
            </div>
            <button type="button" class="edit-btn btn btn-block" name="add"><span class="glyphicon glyphicon-pencil"></span>
                Modifier
            </button>
        </form>
    </div>
        </div>


             <div class="col-sm-12">
           <div class="card">
        <h3 class="card-header text-center font-weight-bold text-uppercase py-4">VOS LIVRES</h3>
        <div class="card-body">
            <div id="table" class="table-editable">
                <table class="table table-bordered table-responsive-md table-striped text-center">
                    <thead>
                        <tr>
                            <th class="text-center">Titre</th>
                            <th class="text-center">Image</th>
                            <th class="text-center">Modifier</th>
                            <th class="text-center">Supprimer</th>
                        </tr>
                    </thead>


                    <tbody> 
                    </tbody>
                </table>
            </div>
        </div>
    </div>
        </div>



    </div>
</div>
{/block}
{block name=script append}
    <script src="{$jsDir}profilUser.js"></script>
{/block}
