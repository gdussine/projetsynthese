{* Smarty *}
{extends file="../canvas/model.tpl"}
{block name=css append}
    <link rel="stylesheet" href="{$cssDir}erreur.css">
{/block}
{block name=content}
  <div class="col-sm-12 menuLecture">
            <h1 class="NombresDeLivres"></h1>
        </div>
    <div class="main">

        <div class="row row-erreur">
            <div class="col-xs-2 col-md-3"></div>


            <div class="col-xs-4 col-md-3">

                <div class="erreur">
{*                    <h1>  <!-- <img class="img-erreur" src="../images/erreur.png" alt="erreur">-->404 !</h1> *}
{*                    </br> *}
                    <h1>ERREUR {$numErr}</h1>
                    <p>{$msgErr}</p>
                    <p>Retournez sur <a href="{$srcDir}home">la page d'accueil du site</a></p>
                </div>
            </div>
            <div class="col-xs-2 col-md-1">

                <div class="img-hero">
                        <img src="{$imgDir}background/hero.png" alt="hero">
                </div>
            </div>
            <div class="col-xs-4 col-md-5"></div>
        </div>
    </div>
{/block}