{* Smarty *}
<!DOCTYPE html>
<html lang="en">

<head>
    <title>le site</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../css/categorie.css">
    <link rel="stylesheet" href="../../../css/modalSignIn.css">
    <link rel="stylesheet" href="../../../css/modalSignUp.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <a id="hautdepage"></a>
</head>
<body>
<header>
    {include file="navBar.tpl"}
</header>
<div class="container">
    <div class="row-main">
        <div class="col-md-2 no-float main-left">
            <ul class="ul-left">
                <li class="li-left"><a href="#nouveautés">Nouveautés</a></li>
                <li class="li-left"><a href="#tendances">Tendances</a></li>
            </ul>
        </div>


        <div class="col-md-10 no-float main-right">
            <div class="row">
                <div class="col-md-12  vcenter main-right-top">
                    <p>
                        <a href="#a">A</a> - <a href="#b">B</a> - <a href="#c">C</a> - <a href="#d">D</a> - <a
                                href="#e">E</a> -
                        <a href="#f">F</a> -
                        <a href="#g">G</a> - <a href="#h">H</a> - <a href="#i">I</a> - <a href="#j">J</a> - <a
                                href="#k">K</a> -
                        <a href="#l">L</a> -
                        <a href="#m">M</a> - <a href="#n">N</a> - <a href="#o">O</a> - <a href="#p">P</a> - <a
                                href="#q">Q</a> -
                        <a href="#r">R</a> -
                        <a href="#s">S</a> - <a href="#t">T</a> - <a href="#u">U</a> - <a href="#v">V</a> - <a
                                href="#w">W</a> -
                        <a href="#x">X</a> -
                        <a href="#y">Y</a> - <a href="#z">Z</a>


                    </p>

                </div>
            </div>

            <div class=" row">
                <div class=" col-md-12 vcenter main-right-center">
                    <div class='row ligne-livre'>
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-11 col-md-10 col-centered">

                                    <div id="carousel" class="carousel slide" data-ride="carousel" data-type="multi"
                                         data-interval="5000">
                                        <div class="carousel-inner">
                                            <div class="item active">
                                                <div class="livre carousel-col">
                                                    <div class="photo">
                                                    </div>
                                                    <p class="auteur">Livre 1</p>
                                                    <button class="button button1">Lire</button>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="livre carousel-col">
                                                    <div class="photo">
                                                    </div>
                                                    <p class="auteur">Livre 2</p>
                                                    <button class="button button1">Lire</button>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="livre carousel-col">
                                                    <div class="photo">
                                                    </div>
                                                    <p class="auteur">Livre 3</p>
                                                    <button class="button button1">Lire</button>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="livre carousel-col">
                                                    <div class="photo">
                                                    </div>
                                                    <p class="auteur">Livre 4</p>
                                                    <button class="button button1">Lire</button>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="livre carousel-col">
                                                    <div class="photo">
                                                    </div>
                                                    <p class="auteur">Livre 5</p>
                                                    <button class="button button1">Lire</button>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="livre carousel-col">
                                                    <div class="photo">
                                                    </div>
                                                    <p class="auteur">Livre 6</p>
                                                    <button class="button button1">Lire</button>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Controls -->
                                        <div class="left carousel-control">
                                            <a href="#carousel" role="button" data-slide="prev">
                                                    <span class="glyphicon glyphicon-chevron-left"
                                                          aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                        </div>
                                        <div class="right carousel-control">
                                            <a href="#carousel" role="button" data-slide="next">
                                                    <span class="glyphicon glyphicon-chevron-right"
                                                          aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                    </div>


                    <div class='row ligne-livre'>
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-11 col-md-10 col-centered">

                                    <div id="carousels" class="carousel slide" data-ride="carousel"
                                         data-type="multi" data-interval="5000">
                                        <div class="carousel-inner">
                                            <div class="item active">
                                                <div class="livre carousel-col">
                                                    <div class="photo">
                                                    </div>
                                                    <p class="auteur">Livre 6</p>
                                                    <button class="button button1">Lire</button>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="livre carousel-col">
                                                    <div class="photo">
                                                    </div>
                                                    <p class="auteur">Livre 6</p>
                                                    <button class="button button1">Lire</button>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="livre carousel-col">
                                                    <div class="photo">
                                                    </div>
                                                    <p class="auteur">Livre 6</p>
                                                    <button class="button button1">Lire</button>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="livre carousel-col">
                                                    <div class="photo">
                                                    </div>
                                                    <p class="auteur">Livre 6</p>
                                                    <button class="button button1">Lire</button>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Controls -->
                                        <div class="left carousel-control">
                                            <a href="#carousels" role="button" data-slide="prev">
                                                    <span class="glyphicon glyphicon-chevron-left"
                                                          aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                        </div>
                                        <div class="right carousel-control">
                                            <a href="#carousels" role="button" data-slide="next">
                                                    <span class="glyphicon glyphicon-chevron-right"
                                                          aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12  vcenter main-right-bot">
                    <div class='previous-next'>

                        <a href="#" class="round previous">Précédent</a>

                        <a href="#" class="round next">Suivant</a>

                    </div>
                </div>
            </div>


        </div>
    </div>
</div>


<script>


    $(document).ready(function () {
        $('.carousel[data-type="multi"] .item').each(function () {
            var next = $(this).next();
            console.log(next)
            if (!next.length) {
                next = $(this).siblings(':first');
            }
            next.children(':first-child').clone().appendTo($(this));

            for (var i = 0; i < 5; i++) {
                next = next.next();
                if (!next.length) {
                    next = $(this).siblings(':first');
                }

                next.children(':first-child').clone().appendTo($(this));
            }
        });
    });
</script>
</body>


</html>