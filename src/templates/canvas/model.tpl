{* Smarty *}
{extends file="../canvas/base.tpl"}
{block name=css append}
    <link rel="stylesheet" href="{$cssDir}navBar.css">
    <link rel="stylesheet" href="{$cssDir}footer.css">
{/block}
{block name=navbar}{include file="../element/navBar.tpl"}{/block}
{block name=content}{/block}
{block name=footer}{include file="../element/footer.tpl"}{/block}
{block name=script}
    <script src="{$jsDir}navBar.js"></script>
{/block}
