{* Smarty *}
<!DOCTYPE html>
<html lang="en">
<head>
    {block name=head}{/block}
</head>
<body>
<main>
    {block name=navbar}{/block}
    {block name=content}{/block}
    {block name=footer}{/block}
</main>
{block name=script}{/block}
</body>
</html>