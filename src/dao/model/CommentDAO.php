<?php

namespace ProjetSynthese\DAO\Model;

use ProjetSynthese\DAO\DAO;
use ProjetSynthese\Model\{Book, User};

interface CommentDAO extends DAO{

    public function getByBook(Book $book);

    public function getByAuthor(User $user);

    
}