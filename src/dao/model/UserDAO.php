<?php

namespace ProjetSynthese\DAO\Model;

use ProjetSynthese\DAO\DAO;
use ProjetSynthese\Model\Role;

interface UserDAO extends DAO{

    public function getByRole(Role $role);

    public function getByLog(string $username, string $password);
}
