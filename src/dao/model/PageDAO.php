<?php

namespace ProjetSynthese\DAO\Model;

use ProjetSynthese\Model\Book;
use ProjetSynthese\DAO\DAO;
use ProjetSynthese\Model\Page;

interface PageDAO extends DAO{

    public function getByBook(Book $book);

    public function getFirstPageFromBook(Book $book) :Page;

}