<?php

namespace ProjetSynthese\DAO\Model;

use ProjetSynthese\DAO\DAO;
use ProjetSynthese\Model\Page;

interface ChoiceDAO extends DAO{

    public function getBySourcePage(Page $pageSrc);
    public function countChoiceBySourcePage(Page $pageSrc);
    public function getByDestinationPage(Page $pageDst);
}