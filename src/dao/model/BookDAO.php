<?php

namespace ProjetSynthese\DAO\Model;

use ProjetSynthese\DAO\DAO;
use ProjetSynthese\Model\Book;
use ProjetSynthese\Model\User;
use ProjetSynthese\Model\Status;

interface BookDAO extends DAO{

    public function getByAuthor(User $user);

    public function getByStatus(Status $status);

    public function getByStatusAndAuthor(User $user,Status $status );
    
    public function getLastId();
    
    public function getNewBook();

    public function getNbPageFromBook(Book $book) : int;
    
}