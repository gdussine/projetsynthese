<?php

namespace ProjetSynthese\DAO;
// DAO Interface
use ProjetSynthese\Connection\Persistance;
use ProjetSynthese\DAO\Model\{CommentDAO, StatusDAO, UserDAO, RoleDAO, BookDAO, PageDAO, ChoiceDAO};
use ProjetSynthese\DAO\MySQL\{CommentMySQLDAO,
    StatusMySQLDAO,
    UserMySQLDAO,
    RoleMySQLDAO,
    BookMySQLDAO,
    PageMySQLDAO,
    ChoiceMySQLDAO};
use ProjetSynthese\DAO\DAO;
class DAOFactory
{

    const PERSISTANCE = Persistance::MYSQL;

    private function __construct()
    {}

    private static function getDAO($typeDAO): DAO
    {
        switch (self::PERSISTANCE) {
            case Persistance::MYSQL:
                switch ($typeDAO){
                    case 'Status': return StatusMySQLDAO::getInstance();
                    case 'User': return UserMySQLDAO::getInstance();
                    case 'Role': return RoleMySQLDAO::getInstance();
                    case 'Book': return BookMySQLDAO::getInstance();
                    case 'Page': return PageMySQLDAO::getInstance();
                    case 'Choice': return ChoiceMySQLDAO::getInstance();
                    case 'Comment': return CommentMySQLDAO::getInstance();
                    default: return null;
                }

            default:
                return null;
        }
    }

    public static function getStatusDAO(): StatusDAO
    {
        return self::getDAO('Status');
    }
    public static function getUserDAO(): UserDAO
    {
        return self::getDAO('User');
    }
    public static function getRoleDAO(): RoleDAO
    {
        return self::getDAO('Role');
    }
    public static function getBookDAO(): BookDAO
    {
        return self::getDAO('Book');
    }
    public static function getChoiceDAO(): ChoiceDAO
    {
        return self::getDAO('Choice');
    }
    public static function getCommentDAO(): CommentDAO
    {
        return self::getDAO('Comment');
    }
    public static function getPageDAO(): PageDAO
    {
        return self::getDAO('Page');
    }
}
