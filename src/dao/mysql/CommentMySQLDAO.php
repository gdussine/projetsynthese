<?php

namespace ProjetSynthese\DAO\MySQL;
use ProjetSynthese\DAO\Model\CommentDAO;
use ProjetSynthese\Model\{Book, User, Comment};
use ProjetSynthese\Connection\Connection;
use ProjetSynthese\DAO\DAOFactory;
use DateTime;

class CommentMySQLDAO implements CommentDAO
{
    private static $instance;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (is_null(self::$instance))
            self::$instance = new self();
        return self::$instance;
    }

    public function getByAuthor(User $user){
        $allComment = [];
        $stmt = Connection::getInstanceBdd()->prepare('SELECT * FROM Comment WHERE idUser = :idUser');
        $stmt->bindValue(':idBook', $user->getIdUser());
        $stmt->execute();
        while ($data = $stmt->fetch()) {
            array_push($allComment,
                new Comment(DAOFactory::getUserDAO()->getById($data['idUser']),
                    DAOFactory::getBookDAO()->getById($data['idBook']),
                    $data['comment'],
                    DateTime::createFromFormat('Y-m-d H:i:s', $data['dateComment']))
            );
        }
        return $allComment;
    }

    public function getByBook(Book $book)
    {
        $allComment = [];
        $stmt = Connection::getInstanceBdd()->prepare('SELECT * FROM Comment WHERE idBook = :idBook');
        $stmt->bindValue(':idBook', $book->getIdBook());
        $stmt->execute();
        while ($data = $stmt->fetch()) {
            array_push($allComment,
                new Comment(DAOFactory::getUserDAO()->getById($data['idUser']),
                    DAOFactory::getBookDAO()->getById($data['idBook']),
                    $data['comment'],
                    DateTime::createFromFormat('Y-m-d H:i:s', $data['dateComment']))
            );
        }
        return $allComment;
    }

    public function getAll()
    {
        $allComment = [];
        $stmt = Connection::getInstanceBdd()->query('SELECT * FROM Comment');
        while ($data = $stmt->fetch()) {
            array_push($allComment,
                new Comment(DAOFactory::getUserDAO()->getById($data['idUser']),
                    DAOFactory::getBookDAO()->getById($data['idBook']),
                    $data['comment'],
                    DateTime::createFromFormat('Y-m-d H:i:s', $data['dateComment']))
            );
        }
        return $allComment;
    }

    public function getById(int $id)
    {
    }

    /**
     * @param Comment $comment
     */
    public function create($comment)
    {
        $stmt = Connection::getInstanceBdd()->prepare("INSERT INTO Comment VALUES (:idUser, :idBook, :comment, :dateComment)");
        $stmt->bindValue(':idUser', $comment->getAuthor()->getIdUser());
        $stmt->bindValue(':idBook', $comment->getBook()->getIdBook());
        $stmt->bindValue(':comment', $comment->getComment());
        $stmt->bindValue(':dateComment', date_format($comment->getDate(),'Y-m-d H:i:s'));
        $stmt->execute();
    }


    /**
     * @param Comment $comment
     */
    public function update($comment)
    {
        $stmt = Connection::getInstanceBdd()->prepare("UPDATE Comment SET comment = :comment WHERE idUser = :idUser AND idBook = :idBook AND dateComment = :dateComment");
        $stmt->bindValue(':idUser', $comment->getAuthor()->getIdUser());
        $stmt->bindValue(':idBook', $comment->getBook()->getIdBook());
        $stmt->bindValue(':comment', $comment->getComment());
        $stmt->bindValue(':dateComment', $comment->getDate());
        $stmt->execute();
    }

    /**
     * @param Comment $comment
     */
    public function delete($comment)
    {
        $stmt = Connection::getInstanceBdd()->prepare("DELETE FROM Comment WHERE idUser = :idUser AND idBook = :idBook AND dateComment = :dateComment");
        $stmt->bindValue(':idUser', $comment->getAuthor()->getIdUser());
        $stmt->bindValue(':idBook', $comment->getBook()->getIdBook());
        $stmt->bindValue(':dateComment', $comment->getDate());
        $stmt->execute();
    }


}