<?php

namespace ProjetSynthese\DAO\MySQL;

use ProjetSynthese\DAO\Model\StatusDAO;
use ProjetSynthese\Connection\Connection;
use ProjetSynthese\Model\Status;

class StatusMySQLDAO implements StatusDAO
{
    private static $instance;
    
    private function __construct(){}
    
    public static function getInstance(){
        if(is_null(self::$instance))
            self::$instance = new self();
        return self::$instance;
    }
    
    public function getAll() : array
    {
        $allStatus = [];
        $stmt = Connection::getInstanceBdd()->query('SELECT * FROM Status');
        while( $data = $stmt->fetch()){
            array_push($allStatus, new Status($data['idStatus'], $data['lbStatus']));
        }
        return $allStatus;
    }

    public function getById(int $id) : Status
    {
        $stmt = Connection::getInstanceBdd()->prepare('SELECT * FROM Status WHERE idStatus = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $data = $stmt->fetch();
        return new Status($data['idStatus'], $data['lbStatus']);
    }

    public function create($status)
    {
        $stmt = Connection::getInstanceBdd()->prepare("INSERT INTO Status VALUES (null,:lbStatus)");
        $stmt->bindValue(':lbStatus', $status->getLbStatus());
        $stmt->execute();
        $status->setIdStatus(Connection::getInstanceBdd()->lastInsertId());        
    }

    public function update($status)
    {
        $stmt = Connection::getInstanceBdd()->prepare("UPDATE  Status SET lbStatus = :lbStatus WHERE idStatus = :idStatus");
        $stmt->bindValue(':lbStatus', $status->getLbStatus());
        $stmt->bindValue(':idStatus', $status->getIdStatus());
        $stmt->execute();
    }

    public function delete($status)
    {
        $stmt = Connection::getInstanceBdd()->prepare("DELETE FROM Status WHERE idStatus = :idStatus");
        $stmt->bindValue(':idStatus', $status->getIdStatus());
        $stmt->execute();
    }



}