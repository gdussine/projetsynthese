<?php

namespace ProjetSynthese\DAO\MySQL;

use ProjetSynthese\DAO\Model\ChoiceDAO;
use ProjetSynthese\Model\{Choice, Page};
use ProjetSynthese\Connection\Connection;
use ProjetSynthese\DAO\DAOFactory;


class ChoiceMySQLDAO implements ChoiceDAO
{


    private static $instance;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (is_null(self::$instance))
            self::$instance = new self();
        return self::$instance;
    }

    public function getBySourcePage(Page $pageSrc)
    {
        $allChoice = [];
        $stmt = Connection::getInstanceBdd()->prepare('SELECT * FROM Choice WHERE idSourcePage = :idSourcePage');
        $stmt->bindValue(':idSourcePage', $pageSrc->getIdPage());
        $stmt->execute();
        while ($data = $stmt->fetch()) {
            array_push($allChoice,
                new Choice($data['idChoice'],
                    $data['description'],
                    DAOFactory::getPageDAO()->getById($data['idSourcePage']),
                    DAOFactory::getPageDAO()->getById($data['idDestinationPage'])));
        }
        return $allChoice;
    }

    public function countChoiceBySourcePage(Page $pageSrc): int
    {
        $stmt = Connection::getInstanceBdd()->prepare("SELECT COUNT(*) as nbChoice FROM Choice WHERE idSourcePage = :idSourcePage");
        $stmt->bindValue(":idSourcePage", $pageSrc->getIdPage());
        $stmt->execute();
        if($data = $stmt->fetch()){
            return $data["nbChoice"];
        }
        return 0;
    }

    public function getByDestinationPage(Page $pageDst)
    {
        $allChoice = [];
        $stmt = Connection::getInstanceBdd()->prepare('SELECT * FROM Choice WHERE idDestinationPage = :idDestinationPage');
        $stmt->execute();
        $stmt->bindValue(':idDestinationPage', $pageDst->getIdPage());
        while ($data = $stmt->fetch()) {
            array_push($allChoice,
                new Choice($data['idChoice'],
                    $data['description'],
                    DAOFactory::getPageDAO()->getById($data['idSourcePage']),
                    DAOFactory::getPageDAO()->getById($data['idDestinationPage'])));
        }
        return $allChoice;
    }

    public function getAll()
    {
        $allChoice = [];
        $stmt = Connection::getInstanceBdd()->query('SELECT * FROM Choice');
        while ($data = $stmt->fetch()) {
            array_push($allChoice,
                new Choice($data['idChoice'],
                    $data['description'],
                    DAOFactory::getPageDAO()->getById($data['idSourcePage']),
                    DAOFactory::getPageDAO()->getById($data['idDestinationPage'])));
        }
        return $allChoice;
    }

    public function getById(int $id)
    {
        $stmt = Connection::getInstanceBdd()->prepare('SELECT * FROM Choice WHERE idChoice = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $data = $stmt->fetch();
        $srcPage =  DAOFactory::getPageDAO()->getById($data['idSourcePage']);
        $book = $srcPage->getBook();
        if($data['idDestinationPage'] == null){
            return new Choice($data['idChoice'],
                $data['description'],
                $srcPage,
                Page::getNullPage($book));
        }
        return new Choice($data['idChoice'],
            $data['description'],
            DAOFactory::getPageDAO()->getById($data['idSourcePage']),
            DAOFactory::getPageDAO()->getById($data['idDestinationPage']));
    }

    /**
     * @param Choice $choice
     */
    public function create($choice)
    {

        if($choice->getDestinationPage() == null){
            $stmt = Connection::getInstanceBdd()->prepare("INSERT INTO Choice (idChoice, description, idSourcePage) VALUES (null,:description, :idSourcePage)");
        }
        else{
            $stmt = Connection::getInstanceBdd()->prepare("INSERT INTO Choice VALUES (null,:description, :idSourcePage, :idDestinationPage)");
            $stmt->bindValue(':idDestinationPage', $choice->getDestinationPage());
        }
        $stmt->bindValue(':description', $choice->getDescription());
        $stmt->bindValue(':idSourcePage', $choice->getSourcePage()->getIdPage());
        $stmt->execute();
        $choice->setIdChoice(Connection::getInstanceBdd()->lastInsertId());
    }


    /**
     * @param Choice $choice
     */
    public function update($choice)
    {
        $stmt = Connection::getInstanceBdd()->prepare("UPDATE  Choice SET description = :description, idSourcePage = :idSourcePage, idDestinationPage = :idDestinationPage WHERE idChoice = :idChoice");
        $stmt->bindValue(':description', $choice->getDescription());
        $stmt->bindValue(':idSourcePage', $choice->getSourcePage());
        $stmt->bindValue(':idDestinationPage', $choice->getDestinationPage());
        $stmt->bindValue(':idChoice', $choice->idChoice());
        $stmt->execute();
    }

    /**
     * @param Choice $choice
     */
    public function delete($choice)
    {
        $stmt = Connection::getInstanceBdd()->prepare("DELETE FROM Choice WHERE idChoice = :idChoice");
        $stmt->bindValue(':idChoice', $choice->getIdChoice());
        $stmt->execute();
    }


}