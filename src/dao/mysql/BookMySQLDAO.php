<?php

namespace ProjetSynthese\DAO\MySQL;

use ProjetSynthese\DAO\DAOFactory;
use ProjetSynthese\DAO\Model\BookDAO;
use ProjetSynthese\Model\User;
use ProjetSynthese\Model\Status;
use ProjetSynthese\Model\Book;
use ProjetSynthese\Connection\Connection;

class BookMySQLDAO implements BookDAO
{
    private static $instance;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (is_null(self::$instance))
            self::$instance = new self();
        return self::$instance;
    }

    public function getAll()
    {
        $allBook = [];
        $stmt = Connection::getInstanceBdd()->query('SELECT * FROM Book ORDER BY idBook DESC');
        while ($data = $stmt->fetch()) {
            array_push($allBook,
                new Book($data['idBook'],
                    $data['title'],
                    $data['abstract'],
                    DAOFactory::getUserDAO()->getById($data['idAuthor']),
                    DAOFactory::getStatusDAO()->getById($data['idStatus'])
                ));
        }
        return $allBook;
    }

    public function getNbPageFromBook(Book $book) : int
    {
        $stmt = Connection::getInstanceBdd()->prepare('SELECT COUNT(*) as nb FROM Page WHERE idBook = :idBook');
        $stmt->bindValue(':idBook', $book->getIdBook());
        $stmt->execute();
        return ($stmt->fetch())['nb'];
    }


    public function getNewBook()
    {
        $allBook = [];
        $stmt = Connection::getInstanceBdd()->query('SELECT * FROM Book ORDER BY idBook DESC LIMIT 0, 6');
        while ($data = $stmt->fetch()) {
            array_push($allBook,
                new Book($data['idBook'],
                    $data['title'],
                    $data['abstract'],
                    DAOFactory::getUserDAO()->getById($data['idAuthor']),
                    DAOFactory::getStatusDAO()->getById($data['idStatus'])
                ));
        }
        return $allBook;
    }

    public function getById($id): Book
    {
        if($id == null){
            return null;
        }
        $stmt = Connection::getInstanceBdd()->prepare('SELECT * FROM Book WHERE idBook = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $data = $stmt->fetch();
        return new Book($data['idBook'],
            $data['title'],
            $data['abstract'],
            DAOFactory::getUserDAO()->getById($data['idAuthor']),
            DAOFactory::getStatusDAO()->getById($data['idStatus'])
        );
    }

    public function getByAuthor(User $user): array
    {
        $stmt = Connection::getInstanceBdd()->prepare('SELECT * FROM Book WHERE idAuthor = :idAuthor');
        $stmt->bindValue(':idAuthor', $user->getIdUser());
        $stmt->execute();
        $userBooks = [];
        while ($data = $stmt->fetch()) {
            array_push($userBooks,
                new Book($data['idBook'],
                    $data['title'],
                    $data['abstract'],
                    DAOFactory::getUserDAO()->getById($data['idAuthor']),
                    DAOFactory::getStatusDAO()->getById($data['idStatus'])
                ));
        }
        return $userBooks;
    }

    public function getByStatus(Status $status){
        $stmt = Connection::getInstanceBdd()->prepare('SELECT * FROM Book WHERE idBook = :idBook');
        $stmt->bindValue(':idBook', $status->getIdStatus());
        $stmt->execute();
        $statusBooks = [];
        while ($data = $stmt->fetch()) {
            array_push($statusBooks,
                new Book($data['idBook'],
                    $data['title'],
                    $data['abstract'],
                    DAOFactory::getUserDAO()->getById($data['idAuthor']),
                    DAOFactory::getStatusDAO()->getById($data['idStatus'])
                ));
        }
        return $statusBooks;
    }

    public function getLastId(){
        $stmt = Connection::getInstanceBdd()->prepare('SELECT * FROM Book ORDER BY idBook DESC LIMIT 1');
        $stmt->execute();
        $data = $stmt->fetch();
        return $data['idBook'];
    }

    public function getByStatusAndAuthor(User $user,Status $status ){
        $stmt = Connection::getInstanceBdd()->prepare('SELECT * FROM Book WHERE idAuthor = :idAuthor AND idBook = :idBook');
        $stmt->bindValue(':idAuthor', $user->getIdUser());
        $stmt->bindValue(':idBook', $status->getIdStatus());
        $stmt->execute();
        $userAndstatusBooks = [];
        while ($data = $stmt->fetch()) {
            array_push($userAndstatusBooks,
                new Book($data['idBook'],
                    $data['title'],
                    $data['abstract'],
                    DAOFactory::getUserDAO()->getById($data['idAuthor']),
                    DAOFactory::getStatusDAO()->getById($data['idStatus'])
                ));
        }
        return $userAndstatusBooks;
    }

    /**
     * @param Book $book
     */
    public function create($book)
    {
        $stmt = Connection::getInstanceBdd()->prepare("INSERT INTO Book VALUES (null,:title, :abstract, :idAuthor, :idStatus)");
        $stmt->bindValue(':title', $book->getTitle());
        $stmt->bindValue(':abstract', $book->getAbstract());
        $stmt->bindValue(':idAuthor', $book->getAuthor()->getIdUser());
        $stmt->bindValue(':idStatus', $book->getStatus()->getIdStatus());
        $stmt->execute();
        $book->setIdBook(Connection::getInstanceBdd()->lastInsertId());

    }

    /**
     * @param Book $book
     */
    public function update($book)
    {
        $stmt = Connection::getInstanceBdd()->prepare("UPDATE  Book SET title = :title, abstract = :abstract, idAuthor = :idAuthor, idStatus = :idStatus WHERE idBook = :idBook");
        $stmt->bindValue(':title', $book->getTitle());
        $stmt->bindValue(':abstract', $book->getAbstract());
        $stmt->bindValue(':idAuthor', $book->getAuthor()->getIdUser());
        $stmt->bindValue(':idStatus', $book->getStatus()->getIdStatus());
        $stmt->bindValue(':idBook', $book->getIdBook());
        $stmt->execute();
    }

    /**
     * @param Book $book
     */
    public function delete($book)
    {
        $stmt = Connection::getInstanceBdd()->prepare("DELETE FROM Book WHERE idBook = :idBook");
        $stmt->bindValue(':idBook', $book->getIdBook());
        $stmt->execute();
    }


}