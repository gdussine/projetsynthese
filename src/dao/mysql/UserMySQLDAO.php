<?php

namespace ProjetSynthese\DAO\MySQL;

use ProjetSynthese\DAO\Model\UserDAO;
use ProjetSynthese\Model\Role;
use ProjetSynthese\DAO\DAOFactory;
use ProjetSynthese\Connection\Connection;
use ProjetSynthese\Model\User;

class UserMySQLDAO implements UserDAO{
    
    private static $instance;
    
    private function __construct(){}
    
    public static function getInstance(){
        if(is_null(self::$instance))
            self::$instance = new self();
            return self::$instance;
    }

    public function getByRole(Role $role){
        $roleUsers = [];
        $stmt = Connection::getInstanceBdd()->prepare('SELECT * FROM User WHERE idRole = :idRole');
        $stmt->bindValue(':idRole', $role->getIdRole());
        $stmt->execute();
        while( $data = $stmt->fetch()){
            array_push($roleUsers, new User(
                $data['idUser'],
                $data['login'],
                $data['password'],
                $data['email'],
                DAOFactory::getRoleDao()->getById($data['idRole'])));
        }
        return $roleUsers;
    }
    
    public function getAll()
    {
        $allUsers = [];
        $stmt = Connection::getInstanceBdd()->query('SELECT * FROM User');
        while( $data = $stmt->fetch()){
            array_push($allUsers, new User(
                $data['idUser'],
                $data['login'],
                $data['password'],
                $data['email'],
                DAOFactory::getRoleDao()->getById($data['idRole'])));
        }
        return $allUsers;
    }

    public function getById(int $id) :User
    {
        $stmt = Connection::getInstanceBdd()->prepare('SELECT * FROM User WHERE idUser = :idUser');
        $stmt->bindValue(':idUser', $id);
        $stmt->execute();
        $data = $stmt->fetch();
        return new User(
                $data['idUser'],
                $data['login'],
                $data['password'],
                $data['email'],
                DAOFactory::getRoleDao()->getById($data['idRole']));
    }
    public function getByLog(string $login, string $password){
        $stmt = Connection::getInstanceBdd()->prepare('SELECT * FROM User WHERE login = :login AND password = :password ');
        $stmt->bindValue(':login', $login);
        $stmt->bindValue(':password', $password);
        $stmt->execute();
       if( $data = $stmt->fetch()){
           return new User(
               $data['idUser'],
               $data['login'],
               $data['password'],
               $data['email'],
               DAOFactory::getRoleDao()->getById($data['idRole']));
       }
       return 0;

    }
    /**
     * @param User $objet
     */
    public function create($user)
    {
        $stmt = Connection::getInstanceBdd()->prepare("INSERT INTO User VALUES (0,:login, :password, :email, :idRole)");
        $stmt->bindValue(':login', $user->getLogin());
        $stmt->bindValue(':password', $user->getPassword());
        $stmt->bindValue(':email', $user->getEmail());
        $stmt->bindValue(':idRole', $user->getRole()->getIdRole());
        $stmt->execute();
        $user->setIdUser(Connection::getInstanceBdd()->lastInsertId());
    }


    /**
     * @param User $objet
     */
    public function update($user)
    {
        $stmt = Connection::getInstanceBdd()->prepare("UPDATE  User SET login = :login, password = :password, email = :email, idRole = :idRole WHERE idUser = :idUser");
        $stmt->bindValue(':idUser', $user->getIdUser());

        $stmt->bindValue(':login', $user->getLogin());
        $stmt->bindValue(':password', $user->getPassword());
        $stmt->bindValue(':email', $user->getEmail());
        $stmt->bindValue(':idRole', $user->getRole()->getIdRole());
        $stmt->execute();
    }

    /**
     * @param User $objet
     */
    public function delete($user)
    {
        $stmt = Connection::getInstanceBdd()->prepare("DELETE FROM User WHERE idUser = :idUser");
        $stmt->bindValue(':idUser', $user->getIdUser());
        $stmt->execute();
    }
}