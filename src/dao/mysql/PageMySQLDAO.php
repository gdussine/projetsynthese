<?php

namespace ProjetSynthese\DAO\MySQL;

use ProjetSynthese\DAO\Model\PageDAO;
use ProjetSynthese\Connection\Connection;
use ProjetSynthese\Model\Book;
use ProjetSynthese\Model\Page;
use ProjetSynthese\DAO\DAOFactory;
use ProjetSynthese\Model\Status;
use ProjetSynthese\Model\User;

class PageMySQLDAO implements PageDAO {

    private static $instance;

    private function __construct(){}

    public static function getInstance(){
        if(is_null(self::$instance))
            self::$instance = new self();
        return self::$instance;
    }

    public function getByBook(Book $book){
        $allPage = [];
        $stmt = Connection::getInstanceBdd()->prepare('SELECT * FROM Page WHERE idBook = :idBook');
        $stmt->bindValue(':idBook', $book->getIdBook());
        $stmt->execute();
        while( $data = $stmt->fetch()){
            array_push($allPage,
                new Page($data['idPage'],
                    $data['numPage'],
                    DAOFactory::getBookDAO()->getById($data['idBook']),
                    $data['text']));
        }
        return $allPage;
    }




    public function getAll()
    {
        $allPage = [];
        $stmt = Connection::getInstanceBdd()->query('SELECT * FROM Page');
        while( $data = $stmt->fetch()){
            array_push($allPage,
                new Page($data['idPage'],
                    $data['numPage'],
                    DAOFactory::getBookDAO()->getById($data['idBook']),
                    $data['text']));
        }
        return $allPage;
    }

    public function getById($id)
    {
        if($id == null){
            return null;
        }
        $stmt = Connection::getInstanceBdd()->prepare('SELECT * FROM Page WHERE idPage = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $data = $stmt->fetch();
        if( $data['idBook'] === null) {
            return null;
        }
        return new Page($data['idPage'],
            $data['numPage'],
            DAOFactory::getBookDAO()->getById($data['idBook']),
            $data['text']);
    }

    /**
     * @param Page $page
     */
    public function create($page)
    {
        $stmt = Connection::getInstanceBdd()->prepare("INSERT INTO Page VALUES (null,:numPage, :idBook, :text)");
        $stmt->bindValue(':numPage', $page->getNumPage());
        $stmt->bindValue(':idBook', $page->getBook()->getIdBook());
        $stmt->bindValue(':text', $page->getText());
        $stmt->execute();
        $page->setIdPage(Connection::getInstanceBdd()->lastInsertId());
    }
    /**
     * @param Page $page
     */
    public function update($page)
    {
        $stmt = Connection::getInstanceBdd()->prepare("UPDATE Page SET numPage = :numPage, idBook = :idBook, text = :text WHERE idPage = :idPage");
        $stmt->bindValue(':numPage', $page->getNumPage());
        $stmt->bindValue(':idBook', $page->getBook()->getIdBook());
        $stmt->bindValue(':text', $page->getText());
        $stmt->bindValue(':idPage', $page->getIdPage());
        $stmt->execute();
    }
    /**
     * @param Page $page
     */
    public function delete($page)
    {
        $stmt = Connection::getInstanceBdd()->prepare("DELETE FROM Page WHERE idPage = :idPage");
        $stmt->bindValue(':idPage', $page->getIdPage());
        $stmt->execute();
    }

    public function getFirstPageFromBook(Book $book) : Page{
        $stmt = Connection::getInstanceBdd()->prepare('SELECT * FROM Page WHERE idPage IN (SELECT MIN(idPage) FROM Page WHERE idBook = :idBook)');
        $stmt->bindValue(':idBook', $book->getIdBook());
        $stmt->execute();
        $data = $stmt->fetch();
        return new Page(
            $data['idPage'],
            $data['numPage'],
            DAOFactory::getBookDAO()->getById($data['idBook']),
            $data['text']);
    }


}