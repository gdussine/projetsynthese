<?php

namespace ProjetSynthese\DAO\MySQL;

use ProjetSynthese\DAO\Model\RoleDAO;
use ProjetSynthese\Connection\Connection;
use ProjetSynthese\Model\Role;

class RoleMySQLDAO implements RoleDAO
{
    private static $instance;

    private function __construct(){}

    public static function getInstance(){
        if(is_null(self::$instance))
            self::$instance = new self();
        return self::$instance;
    }

    public function getAll() : array
    {
        $allRole = [];
        $stmt = Connection::getInstanceBdd()->query('SELECT * FROM Role');
        while( $data = $stmt->fetch()){
            array_push($allRole, new Role($data['idRole'], $data['lbRole']));
        }
        return $allRole;
    }

    public function getById(int $id) :Role
    {
        $stmt = Connection::getInstanceBdd()->prepare('SELECT * FROM Role WHERE idRole = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $data = $stmt->fetch();
        return new Role($data['idRole'], $data['lbRole']);
    }

    /**
     * @param Role $role
     */
    public function create($role)
    {
        $stmt = Connection::getInstanceBdd()->prepare("INSERT INTO Role VALUES (null,:lbRole)");
        $stmt->bindValue(':lbRole', $role->getLbRole());
        $stmt->execute();
        $role->setIdRole(Connection::getInstanceBdd()->lastInsertId());
    }

    /**
     * @param Role $role
     */
    public function update($role)
    {
        $stmt = Connection::getInstanceBdd()->prepare("UPDATE  Role SET lbRole = :lbRole WHERE idRole = :idRole");
        $stmt->bindValue(':lbRole', $role->getLbRole());
        $stmt->bindValue(':idRole', $role->getIdRole());
        $stmt->execute();
    }

    /**
     * @param Role $role
     */
    public function delete($role)
    {
        $stmt = Connection::getInstanceBdd()->prepare("DELETE FROM Role WHERE idRole = :idRole");
        $stmt->bindValue(':idRole', $role->getIdRole());
        $stmt->execute();
    }



}