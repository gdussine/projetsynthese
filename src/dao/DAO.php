<?php

namespace ProjetSynthese\DAO;

interface DAO{

    public function create($objet);

    public function getById(int $id);

    public function getAll();

    public function update($objet);

    public function delete($objet);

}
