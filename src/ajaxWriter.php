<?php
require '../vendor/autoload.php';

use ProjetSynthese\DAO\DAOFactory;
use ProjetSynthese\Model\{Page, Choice};


/**
 * Renvoie l'id des pages d'un livre
 */
if (isset($_POST['loadBook']) && isset($_POST['idBook'])) {
    $idBook = $_POST['idBook'];
    $listePage = DAOFactory::getPageDAO()->getByBook(DAOFactory::getBookDAO()->getById(intval($idBook)));
    $res = [];
    foreach ($listePage as $page) {
        $pageInArray = null;
        $pageInArray = ['idPage' => $page->getIdPage(), 'text' => $page->getText()];
        array_push($res, $pageInArray);
    }
    echo json_encode($res);
}

if(isset($_POST['deletePage']) && isset($_POST['idPage'])){
    $listeChoiceSrc = DAOFactory::getChoiceDAO()->getBySourcePage(DAOFactory::getPageDAO()->getById(intval($_POST['idPage'])));
    foreach($listeChoiceSrc as $choix){
        DAOFactory::getChoiceDAO()->delete($choix);
    }
    DAOFactory::getPageDAO()->delete(DAOFactory::getPageDAO()->getById(intval($_POST['idPage'])));
    echo 1;
}

/**
 * Renvoie tout les choix d'une page
 */
if (isset($_POST['loadChoix']) && isset($_POST['idPage'])) {
    $id = $_POST['idPage'];
    $listeChoice = DAOFactory::getChoiceDAO()->getBySourcePage(DAOFactory::getPageDAO()->getById(intval($id)));
    $res = [];
    foreach ($listeChoice as $choice) {
        $choiceInArray = null;
        $choiceInArray = [
            'idChoice' => $choice->getIdChoice(),
            'description' => $choice->getDescription(),
            'idSourcePage' => $choice->getSourcePage() === null ? -1 : $choice->getSourcePage()->getIdPage(),
            'idDestinationPage' => $choice->getDestinationPage() === null ?-1 : $choice->getDestinationPage()->getIdPage()
        ];
        array_push($res, $choiceInArray);
    }
    echo json_encode($res);
}

/**
 * Renvoie le contenu d'une page
 */
if (isset($_POST['loadPage']) && isset($_POST['idPage'])) {
    $id = $_POST['idPage'];
    $text = DAOFactory::getPageDAO()->getById($id)->getText();
    echo $text;
}

/**
 * Sauver le contenu d'une page
 */

if (isset($_POST['savePage']) && isset($_POST['idPage']) && isset($_POST['text'])) {
    $id = $_POST['idPage'];
    $text = $_POST['text'];

    $page = DAOFactory::getPageDAO()->getById(intval($id));
    $page->setText($text);
    DAOFactory::getPageDAO()->update($page);
    echo 1;
}

/**
 * Sauver les choix d'une page
 */

if (isset($_POST['saveChoice']) && isset($_POST['idPage']) && isset($_POST['textChoix1']) && isset($_POST['textChoix2']) && isset($_POST['textChoix3']) && isset($_POST['textChoix4'])
    && isset($_POST['idPageChoix1']) && isset($_POST['idPageChoix2']) && isset($_POST['idPageChoix3']) && isset($_POST['idPageChoix4'])) {
    $listeChoice = DAOFactory::getChoiceDAO()->getBySourcePage(DAOFactory::getPageDAO()->getById((intval($_POST['idPage']))));
    foreach ($listeChoice as $choice) {
        DAOFactory::getChoiceDAO()->delete($choice);
    }
    if ($_POST['textChoix1'] != '') {
        if($_POST['idPageChoix1'] == -1) {
            $destPage = null;
        }
        else{
            $destPage = DAOFactory::getPageDAO()->getById($_POST['idPageChoix1']);
        }

        DAOFactory::getChoiceDAO()->create(new Choice(
            -1, $_POST['textChoix1'],
            DAOFactory::getPageDAO()->getById(intval($_POST['idPage'])),$destPage
            ));
    }
    if ($_POST['textChoix2'] != '') {
        DAOFactory::getChoiceDAO()->create(new Choice(
            -1, $_POST['textChoix2'],
            DAOFactory::getPageDAO()->getById(intval($_POST['idPage'])),
            DAOFactory::getPageDAO()->getById($_POST['idPageChoix2'])));

    }
    if ($_POST['textChoix3'] != '') {
        DAOFactory::getChoiceDAO()->create(new Choice(
            -1, $_POST['textChoix3'],
            DAOFactory::getPageDAO()->getById(intval($_POST['idPage'])),
            DAOFactory::getPageDAO()->getById($_POST['idPageChoix3'])));


    }
    if ($_POST['textChoix4'] != '') {
        DAOFactory::getChoiceDAO()->create(new Choice(
            -1, $_POST['textChoix4'],
            DAOFactory::getPageDAO()->getById(intval($_POST['idPage'])),
            DAOFactory::getPageDAO()->getById($_POST['idPageChoix4'])));
    }
    echo 1;
}


if(isset($_POST['newPage']) && isset($_POST['idBook']) && isset($_POST['num'])){
    $page = new Page(-1, intval($_POST['num']), DAOFactory::getBookDAO()->getById(intval($_POST['idBook'])), '');
    DAOFactory::getPageDAO()->create($page);
    echo $page->getIdPage();
}