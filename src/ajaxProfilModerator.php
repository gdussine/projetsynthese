<?php
require '../vendor/autoload.php';
use ProjetSynthese\DAO\DAOFactory;
use ProjetSynthese\Model\Book;
use ProjetSynthese\Model\Status;
use ProjetSynthese\Model\User;
use ProjetSynthese\Model\Role;

/**
 * Renvoie tous les livres
 */
$checkImage = false;
$user = DAOFactory::getUserDAO()->getById($_POST['idUser']);
$users = DAOFactory::getUserDAO()->getAll();

$listUsers = [];
foreach ($users as $user) {
    $userInArray = null;
    if ($user->getRole()->getIdRole() == Role::ROLE_ID_USER) {
        $userInArray = [
            'idUser' => $user->getIdUser(),
            'login' => ucfirst($user->getLogin()),
        ];
        array_push($listUsers, $userInArray);
    }
}

echo json_encode($listUsers);
