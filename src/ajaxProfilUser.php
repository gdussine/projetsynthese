<?php
require '../vendor/autoload.php';
use ProjetSynthese\DAO\DAOFactory;
use ProjetSynthese\Model\Book;
use ProjetSynthese\Model\Status;
use ProjetSynthese\Model\User;

/**
 * Renvoie tous les livres
 */
$checkImage = false;
$user = DAOFactory::getUserDAO()->getById($_POST['idUser']);
$books = DAOFactory::getBookDAO()->getByAuthor($user);
if (isset($_POST["edit"])) {
    echo $user->getIdUser();
    $userEdit = new User($user->getIdUser(), $_POST["login"], $_POST["password"], $_POST["email"], $user->getRole());
    DAOFactory::getUserDAO()->update($userEdit);

} else if (isset($_POST["update"])) {
    $folder = "view/upload/";
    $filename = $_FILES['image']['name'];
    $ext = pathinfo($filename, PATHINFO_EXTENSION);
    $imageName = strval($_POST["idBook"]);
    $image = $imageName . "." . "jpg";
    $path = $folder . $image;
    $target_file = $folder . basename($_FILES["image"]["name"]);
    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
    $allowed = array('jpeg', 'png', 'jpg');
    if (!empty($filename)) {
        unlink("view/upload/" . $_POST["idBook"] . '.jpg');
        move_uploaded_file($_FILES['image']['tmp_name'], $path);
    }
    $book = DAOFactory::getBookDAO()->getById($_POST["idBook"]);
    $bookEdit = new Book($book->getIdBook(), $_POST["titre"], "", $user, $book->getStatus());
    DAOFactory::getBookDAO()->update($bookEdit);


} else if (isset($_POST["delete"])) {
    $id = $_POST["idBook"];
    $deleteBook = DAOFactory::getBookDAO()->getById($id);
    unlink("view/upload/" . $id . '.jpg');
    DAOFactory::getBookDAO()->delete($deleteBook);


} else {
    $listBooks = [];
    foreach ($books as $book) {
        $bookInArray = null;
        $user = DAOFactory::getUserDAO()->getById($book->getAuthor()->getIdUser());
        if (DAOFactory::getBookDAO()->getNbPageFromBook($book) > 0) {
            $bookInArray = [
                'idBook' => $book->getIdBook(),
                'title' => ucfirst($book->getTitle()),
                'abstract' => $book->getAbstract(),
                'status' => $book->getStatus()->getIdStatus(),
            ];
            array_push($listBooks, $bookInArray);
        }
    }
    echo json_encode($listBooks);
}
