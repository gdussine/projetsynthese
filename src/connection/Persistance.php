<?php

namespace ProjetSynthese\Connection;

class Persistance{
    
    const MYSQL = 0;
    const XML = 1;
    const JSON = 2;
    
    private function __construct(){}
    
}