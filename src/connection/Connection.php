<?php

namespace ProjetSynthese\Connection;

use PDO;

class Connection extends PDO
{
    const HOST = 'devbdd.iutmetz.univ-lorraine.fr';
    const DBNAME = 'ganglof14u_pjSynthese';
    const CHARSET = 'utf8';
    const USERNAME = 'ganglof14u_appli';
    const PASSWD = '31202668';

    /* @var Connection $instance */
    private static $instance = null;

    private function __construct()
    {
        parent::__construct('mysql:host=' . self::HOST . ';dbname=' . self::DBNAME . ';charset=' . self::CHARSET, self::USERNAME, self::PASSWD);
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public static function getInstanceBdd()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}