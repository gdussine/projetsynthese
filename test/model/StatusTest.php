<?php

use PHPUnit\Framework\TestCase;

require_once __DIR__.'/../../src/model/Status.php';

class StatusTest extends TestCase
{
    public function statusProvider(){
        return [
            [Status::STATUS_IN_WRITING],
            [Status::STATUS_VALID],
            [Status::STATUS_INVALID],
            [Status::STATUS_WAITING]
        ];
    }

    /**
     * @expectedException TypeError
     */
    public function testSetIdStatus_TypeError(){
        $stat = new Status(0,Status::STATUS_IN_WRITING);
        $value = "ST01";
        $stat->setIdStatus($value);
    }

    /**
     * @expectedException TypeError
     */
    public function testSetLbStatus_TypeError(){
        $stat = new Status(1,Status::STATUS_IN_WRITING);
        $value = null;
        $stat->setLbStatus($value);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testSetLbStatus_IncorrectValue(){
        $stat = new Status(1,Status::STATUS_IN_WRITING);
        $stat->setLbStatus("non");
    }


    public function testGetIdStatus_correctValue(){
        $value = 1;
        $stat = $stat = new Status($value,Status::STATUS_IN_WRITING);
        $this->assertEquals($value, $stat->getIdStatus());
    }

    /**
     * @param string $constStat
     * @dataProvider statusProvider
     */
    public function testGetLbStatus_correctValue(string $constStat){
        $value = $constStat;
        $stat = $stat = new Status(1,$value);
        $this->assertEquals($value, $stat->getLbStatus());
    }

}