<?php

use PHPUnit\Framework\TestCase;

require_once __DIR__.'/../../src/dao/DAOFactory.php';

class DAOFactoryTest extends TestCase
{
    public function testGetStatusDAO_correctValue(){
        $value = DAOFactory::getStatusDAO();
        $this->assertTrue($value instanceof StatusDAO);
    }

    public function testGetRoleDAO_correctValue(){
        $value = DAOFactory::getRoleDAO();
        $this->assertTrue($value instanceof RoleDAO);
    }


    public function testGetChoiceDAO_correctValue(){
        $value = DAOFactory::getChoiceDAO();
        $this->assertTrue($value instanceof ChoiceDAO);
    }

    public function testGetBookDAO_correctValue(){
        $value = DAOFactory::getBookDAO();
        $this->assertTrue($value instanceof BookDAO);
    }

    public function testGetUserDAO_correctValue(){
        $value = DAOFactory::getUserDAO();
        $this->assertTrue($value instanceof UserDAO);
    }


    public function testGetPageDAO_correctValue(){
        $value = DAOFactory::getPageDAO();
        $this->assertTrue($value instanceof PageDAO);
    }

    public function testGetCommentDAO_correctValue(){
        $value = DAOFactory::getCommentDAO();
        $this->assertTrue($value instanceof CommentDAO);
    }





}