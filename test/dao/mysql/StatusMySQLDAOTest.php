<?php

use PHPUnit\Framework\TestCase;

require_once __DIR__.'/../../../src/dao/mysql/StatusMySQLDAO.php';

class StatusMySQLDAOTest extends TestCase{

    public function StatusProvider(){
        $status1 = new Status(0,Status::STATUS_WAITING);
        $status2 = new Status(0,Status::STATUS_INVALID);
        $status3 = new Status(0,Status::STATUS_VALID);
        $status4 = new Status(0,Status::STATUS_IN_WRITING);
        return [
            [$status1],
            [$status2],
            [$status3],
            [$status4]
        ];
    }

    public function testGetAll_NotEmpty(){
        $this->assertTrue(count(StatusMySQLDAO::getInstance()->getAll())>0);
    }

    /**
     * @dataProvider StatusProvider
     */
    public function testCreateDelete_AutoGeneratedKey($status){
        StatusMySQLDAO::getInstance()->create($status);
        $this->assertNotEquals(0, $status->getIdStatus());
        StatusMySQLDAO::getInstance()->delete($status);
    }

    /**
     * @dataProvider StatusProvider
     */
    public function testGetById_CorrectValue(Status $status){
        StatusMySQLDAO::getInstance()->create($status);
        $this->assertEquals(StatusMySQLDAO::getInstance()->getById($status->getIdStatus()),$status);
        StatusMySQLDAO::getInstance()->delete($status);
    }

    /**
     * @dataProvider StatusProvider
     */
    public function testCreate_CorrectNumber(Status $status){
        $value0 = sizeof(StatusMySQLDAO::getInstance()->getAll());
        StatusMySQLDAO::getInstance()->create($status);
        $value1 = sizeof(StatusMySQLDAO::getInstance()->getAll());
        $this->assertEquals($value0+1,$value1);
        StatusMySQLDAO::getInstance()->delete($status);
    }

    /**
     * @dataProvider StatusProvider
     */
    public function testDelete_CorrectNumber(Status $status){
        StatusMySQLDAO::getInstance()->create($status);
        $value1 = sizeof(StatusMySQLDAO::getInstance()->getAll());
        StatusMySQLDAO::getInstance()->delete($status);
        $value2 = sizeof(StatusMySQLDAO::getInstance()->getAll());
        $this->assertEquals($value2+1,$value1);
    }

    /**
     * @dataProvider StatusProvider
     */
    public function testUpdate_CorrectValue(Status  $status){
        StatusMySQLDAO::getInstance()->create($status);
        $status->setLbStatus(Status::STATUS_VALID);
        StatusMySQLDAO::getInstance()->update($status);
        $this->assertEquals(StatusMySQLDAO::getInstance()->getById($status->getIdStatus()),$status);
        StatusMySQLDAO::getInstance()->delete($status);
    }

}