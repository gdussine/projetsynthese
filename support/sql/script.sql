-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Client :  devbdd.iutmetz.univ-lorraine.fr
-- Généré le :  Jeu 04 Avril 2019 à 16:54
-- Version du serveur :  10.3.13-MariaDB
-- Version de PHP :  7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ganglof14u_pjSynthese`
--

-- --------------------------------------------------------

--
-- Structure de la table `Book`
--

CREATE TABLE IF NOT EXISTS `Book` (
  `idBook` int(11) NOT NULL,
  `title` varchar(20) NOT NULL,
  `abstract` text NOT NULL,
  `idAuthor` int(11) NOT NULL,
  `idStatus` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Book`
--

INSERT INTO `Book` (`idBook`, `title`, `abstract`, `idAuthor`, `idStatus`) VALUES
(1, 'Ceth le mercenaire', 'je ne sais pas quoi mettre ici :(', 1, 1),
(16, 'Warcraft', 'La légion Ardente a été bannie d''Azeroth depuis déjà bien des mois. Dans les montagnes de Kalimdor, un mystérieux phénomène ouvre une brèche dans la Trame du Temps. \r\n		 Et l''issue de la Guerre des Anciens risque d''être à jamais changée par l''arrivée dans le passé de trois héros égarés : Krasus, \r\n		 le mage dragon aux pouvoirs inexplicablement diminués, Rhonin, le magicien humain, et Broxigar, le guerrier orc qui rêve d''une mort glorieuse au combat. \r\n		 Mais sans l''aide du demi-dieu Cenarius et de deux elfes jumeaux mais très différents, ils ne pourront empêcher que s''ouvre de nouveau le portail de la Légion Ardente. \r\n		 Et cette fois, les luttes du passé risquent fort de se déverser dans le futur... ', 2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `Choice`
--

CREATE TABLE IF NOT EXISTS `Choice` (
  `idChoice` int(11) NOT NULL,
  `description` text NOT NULL,
  `idSourcePage` int(11) NOT NULL,
  `idDestinationPage` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Choice`
--

INSERT INTO `Choice` (`idChoice`, `description`, `idSourcePage`, `idDestinationPage`) VALUES
(1, 'Vous essayez d’appeler quelqu’un pour voir si vous êtes seul ou non  ', 1, 5),
(2, 'Vous essayez d’ouvrir la porte de la cellule ', 1, 6),
(3, 'Aller à la rencontre de Cénarius pour qu''il nous aide.', 3, 4),
(4, 'Se rendre sur le lieu de la brèche pour en apprendre plus.', 4, 5),
(5, 'Partir aux grottes du temps afin d''aider Krasus à regagner ses pouvoirs.', 2, 1),
(7, 'Vous devez ouvrir cette porte.', 5, 6);

-- --------------------------------------------------------

--
-- Structure de la table `Comment`
--

CREATE TABLE IF NOT EXISTS `Comment` (
  `idUser` int(11) NOT NULL,
  `idBook` int(11) NOT NULL,
  `comments` varchar(1000) NOT NULL,
  `dateComment` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Comment`
--

INSERT INTO `Comment` (`idUser`, `idBook`, `comments`, `dateComment`) VALUES
(1, 1, 'très très bon livre je lui donne 20/20', '2019-04-02 13:12:42'),
(1, 16, 'Livre à revoir ', '2019-04-02 23:33:13'),
(2, 1, 'Résumé à revoir', '2019-04-02 06:15:21');

-- --------------------------------------------------------

--
-- Structure de la table `Page`
--

CREATE TABLE IF NOT EXISTS `Page` (
  `idPage` int(11) NOT NULL,
  `numPage` int(11) NOT NULL,
  `idBook` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Page`
--

INSERT INTO `Page` (`idPage`, `numPage`, `idBook`, `text`) VALUES
(1, 1, 1, 'Dans cette histoire, vous incarnez Ceth un mercenaire…mystérieux.\nVous vous réveillez, vous prenez le temps de jeter un œil sur le monde qui vous entoure. Vous êtes dans une prison mais ce qui est étrange c’est qu’il n’y a personne, pas un seul garde, pas un seul prisonnier a part vous mais vous ne vous sentez pas seul, vous avez la sensation que quelqu’un ou que quelque chose vous observe… \nVous vous levez et vous dirigez vers la porte de la cellule. Que voulez-vous faire ?\n'),
(2, 5, 16, 'Il était clair qu''Azshara et ses principaux serviteurs Bien nés avaient été entraînés au fond des océans déchaînés. Pourtant, il restait de nombreux Bien nés parmi les survivants qui gagnèrent les rives de la nouvelle terre. Furion n''avait pas confiance dans les intentions des Bien nés, mais il était rassuré par leur incapacité à causer du tort sans les énergies du Puits.'),
(3, 6, 16, 'Illidan, qui avait également survécu à la catastrophe, avait atteint le sommet du mont Hyjal bien avant Furion et les Elfes de la nuit. Dans son désir dément de maintenir le flux de la magie dans le monde, Illidan avait versé le contenu de ses fioles, pleines des eaux précieuses du Puits d''éternité, dans le lac de la montagne. Les puissantes énergies enchantées se répandirent rapidement, créant une sorte de nouveau Puits d''éternité.'),
(4, 4, 16, 'Tandis que le monde tremblait, ravagé par l''implosion du Puits, les mers se précipitèrent pour combler les crevasses creusées dans les terres. Presque quatre-vingt pour cent des terres de Kalimdor avaient été fissurées, créant des continents séparés entourés par de nouvelles mers furieuses. Au centre d''une nouvelle mer, où le Puits s''était naguère tenu, une tempête d''énergies chaotiques faisait rage. Ce terrible phénomène, connu sous le nom de Maelström, était destiné à durer éternellement dans un tourbillon furieux. Il allait devenir un rappel permanent de la terrible catastrophe qui avait mis fin à l''âge d''or...'),
(5, 2, 1, 'Vous criez « es ce qu’il y a quelqu’un de vivant ici ? »…. Aucune réponse ainsi que aucun mouvement. Il ne reste qu’une chose à faire.'),
(6, 3, 1, 'Vous essayez d’ouvrir la porte. La porte s’ouvre comme si quelqu’un aurait oublié de la fermer, c’est étrange…\r\nDevant vous ce présente un escalier, il doit certainement amener à la surface ou bien à l’intérieur du château mais  sans arme ce chemin est risqué mais ce serai le chemin le plus rapide pour pouvoir fuir.\r\nIl reste une possibilité en fouillant la prison pour pouvoir trouver une entrée dérobée ou bien un passage permettant de sortir mais cela risque de prendre du temps…\r\nLe temps presse vous devez décider ce que vous devez faire rapidement avant que les gardes ne reviennent.\r\n');

-- --------------------------------------------------------

--
-- Structure de la table `Role`
--

CREATE TABLE IF NOT EXISTS `Role` (
  `idRole` int(11) NOT NULL,
  `lbRole` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Role`
--

INSERT INTO `Role` (`idRole`, `lbRole`) VALUES
(1, 'utilisateur'),
(2, 'moderateur'),
(3, 'administrateur');

-- --------------------------------------------------------

--
-- Structure de la table `State`
--

CREATE TABLE IF NOT EXISTS `State` (
  `idStatus` int(11) NOT NULL,
  `lbStatus` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `State`
--

INSERT INTO `State` (`idStatus`, `lbStatus`) VALUES
(1, 'en attente de validation'),
(2, 'validé'),
(3, 'non validé');

-- --------------------------------------------------------

--
-- Structure de la table `User`
--

CREATE TABLE IF NOT EXISTS `User` (
  `idUser` int(11) NOT NULL,
  `login` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(15) NOT NULL,
  `idRole` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `User`
--

INSERT INTO `User` (`idUser`, `login`, `email`, `password`, `idRole`) VALUES
(1, 'londot', 'kevinlondot@hotmail.fr', 'tueurred', 3),
(2, 'Lucas', 'lucas.arnould@outlook.com', '20081996', 2);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Book`
--
ALTER TABLE `Book`
  ADD PRIMARY KEY (`idBook`),
  ADD KEY `idAuthor` (`idAuthor`),
  ADD KEY `idStatus` (`idStatus`);

--
-- Index pour la table `Choice`
--
ALTER TABLE `Choice`
  ADD PRIMARY KEY (`idChoice`),
  ADD KEY `idSourcePage` (`idSourcePage`),
  ADD KEY `idDestinationPage` (`idDestinationPage`);

--
-- Index pour la table `Comment`
--
ALTER TABLE `Comment`
  ADD PRIMARY KEY (`idUser`,`idBook`,`dateComment`),
  ADD KEY `idUser` (`idUser`,`idBook`),
  ADD KEY `idBook` (`idBook`);

--
-- Index pour la table `Page`
--
ALTER TABLE `Page`
  ADD PRIMARY KEY (`idPage`),
  ADD KEY `idBook` (`idBook`);

--
-- Index pour la table `Role`
--
ALTER TABLE `Role`
  ADD PRIMARY KEY (`idRole`);

--
-- Index pour la table `State`
--
ALTER TABLE `State`
  ADD PRIMARY KEY (`idStatus`);

--
-- Index pour la table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`idUser`),
  ADD KEY `addRole` (`idRole`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Book`
--
ALTER TABLE `Book`
  MODIFY `idBook` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT pour la table `Choice`
--
ALTER TABLE `Choice`
  MODIFY `idChoice` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `Page`
--
ALTER TABLE `Page`
  MODIFY `idPage` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `Role`
--
ALTER TABLE `Role`
  MODIFY `idRole` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `State`
--
ALTER TABLE `State`
  MODIFY `idStatus` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `User`
--
ALTER TABLE `User`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Book`
--
ALTER TABLE `Book`
  ADD CONSTRAINT `Book_ibfk_1` FOREIGN KEY (`idAuthor`) REFERENCES `User` (`idUser`);

--
-- Contraintes pour la table `Choice`
--
ALTER TABLE `Choice`
  ADD CONSTRAINT `Choice_ibfk_1` FOREIGN KEY (`idSourcePage`) REFERENCES `Page` (`idPage`),
  ADD CONSTRAINT `Choice_ibfk_2` FOREIGN KEY (`idDestinationPage`) REFERENCES `Page` (`idPage`);

--
-- Contraintes pour la table `Comment`
--
ALTER TABLE `Comment`
  ADD CONSTRAINT `Comment_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `User` (`idUser`),
  ADD CONSTRAINT `Comment_ibfk_2` FOREIGN KEY (`idBook`) REFERENCES `Book` (`idBook`);

--
-- Contraintes pour la table `Page`
--
ALTER TABLE `Page`
  ADD CONSTRAINT `Page_ibfk_1` FOREIGN KEY (`idBook`) REFERENCES `Book` (`idBook`);

--
-- Contraintes pour la table `User`
--
ALTER TABLE `User`
  ADD CONSTRAINT `User_ibfk_1` FOREIGN KEY (`idRole`) REFERENCES `Role` (`idRole`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
