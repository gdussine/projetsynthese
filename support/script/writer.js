$(document).ready(function () {
  var livre = [];
  var lastEventAdd = true;
  var inputTitre = document.getElementById("titre-page")
  var inputChoix1 = document.getElementById("choix-1")
  var inputChoix2 = document.getElementById("choix-2")
  var inputChoix3 = document.getElementById("choix-3")
  var inputChoix4 = document.getElementById("choix-4")
  var elementSelected;
  var indexPage = 0;

  $(document.getElementById("main-center")).css('visibility', 'hidden');

  $(".btn-supprimer-page").hide()
  $(".btn-enregistrer-page").hide()

  $('#summernote').summernote({
    height: 550,
    maxHeight: 385,
    placeholder: "Saisissez le contenu de votre page ici...",
    toolbar: [
      ['view', ['undo', 'redo']],
      ['style', ['bold', 'italic', 'underline']],
      ['font', ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New']],
      ['fontsize', ['fontsize']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['height', ['height']],
      ['view', ['fullscreen', 'codeview']]
    ]
  });


  $(".btn-ajouter-page").click(function () {
    let element = elementSelected;
    let id = $(element).attr('id')
    let getTitre = inputTitre.value
    let getChoix1 = inputChoix1.value
    let getChoix2 = inputChoix2.value
    let getChoix3 = inputChoix3.value
    let getChoix4 = inputChoix4.value
    let getPage = $('#summernote').summernote('code');

    if (livre[id]) {
      livre[id].titre = getTitre;
      livre[id].text = getPage;
      livre[id].choix1 = getChoix1;
      livre[id].choix2 = getChoix2;
      livre[id].choix3 = getChoix3;
      livre[id].choix4 = getChoix4;
      if(getTitre != ""){
        $("#label"+id+"").text(getTitre);
        }
    }

    if (!livre[indexPage]) {

      livre[indexPage] = {
        id: indexPage,
        titre: "",
        text: "",
        choix1: "",
        choix2: "",
        choix3: "",
        choix4: "",
      }
      if (indexPage == 1) {
        livre[0].titre = getTitre;
        livre[0].text = getPage;
        livre[0].choix1 = getChoix1;
        livre[0].choix2 = getChoix2;
        livre[0].choix3 = getChoix3;
        livre[0].choix4 = getChoix4;
        if(getTitre != ""){
          $("#label"+0+"").text(getTitre);
          }
      }
      let newPage = $("<div/>")
        .addClass("page col-xs-12 col-md-12 col-lg-12 focus")
        .html("<input type='radio' name='list' class='checkbox-page' id='" + indexPage + "'><label id='label"+indexPage+"' for='" + indexPage + "'>Page " + (indexPage + 1) + "</label>");
      $("#div-view .btn-ajouter-page:last").before(newPage);
      $("#div-view #" + indexPage + "").prop("checked", true);
      $('#summernote').summernote('reset');
      $(inputTitre).val('')
      $(inputChoix1).val('')
      $(inputChoix2).val('')
      $(inputChoix3).val('')
      $(inputChoix4).val('')
      $(document.getElementById("main-center")).css('visibility', 'visible');
      $(".btn-supprimer-page").show()
      $(".btn-enregistrer-page").show()

      indexPage++;
      lastEventAdd = true;
    }

  });

  $(document).on('change', 'input:radio', function () {
    $(document.getElementById("main-center")).css('visibility', 'visible');
    let idSelected = $(this).attr('id')
    let idBefore = $(elementSelected).attr('id')
    let getTitre = inputTitre.value
    let getChoix1 = inputChoix1.value
    let getChoix2 = inputChoix2.value
    let getChoix3 = inputChoix3.value
    let getChoix4 = inputChoix4.value
    let getPage = $('#summernote').summernote('code');
    if (idBefore && lastEventAdd == false) {
      livre[idBefore].titre = getTitre;
      livre[idBefore].text = getPage;
      livre[idBefore].choix1 = getChoix1;
      livre[idBefore].choix2 = getChoix2;
      livre[idBefore].choix3 = getChoix3;
      livre[idBefore].choix4 = getChoix4;
      if(getTitre != ""){
        $("#label"+idBefore+"").text(getTitre);
        }
    }



    $('#summernote').summernote('reset');
    $(inputTitre).val('')
    $(inputChoix1).val('')
    $(inputChoix2).val('')
    $(inputChoix3).val('')
    $(inputChoix4).val('')


    $(".btn-supprimer-page").show()
    $(".btn-enregistrer-page").show()


    if (livre[idSelected]) {
      let text = livre[idSelected].text;
      $('#summernote').summernote('code', text);
      let titre = livre[idSelected].titre;
      console.log($("#label"+idSelected+""))
      if(titre != ""){
      $("#label"+idSelected+"").text(titre);
      }
      $(inputTitre).val(titre)
      let choix1 = livre[idSelected].choix1;
      $(inputChoix1).val(choix1)
      let choix2 = livre[idSelected].choix2;
      $(inputChoix2).val(choix2)
      let choix3 = livre[idSelected].choix3;
      $(inputChoix3).val(choix3)
      let choix4 = livre[idSelected].choix4;
      $(inputChoix4).val(choix4)
    }
    elementSelected = this
    lastEventAdd = false
  });


  $(".btn-supprimer-page").click(function () {
    $('.page input:checked').each(function () {
      let idSelected = $(this).attr('id')
      delete livre[idSelected]
      $(this).parent().remove()
      elementSelected = ""
    });
    $('#summernote').summernote('reset');
  });

  $(".btn-enregistrer-page").click(function () {
    let element = elementSelected;
    let id = $(element).attr('id')
    let getTitre = inputTitre.value
    let getChoix1 = inputChoix1.value
    let getChoix2 = inputChoix2.value
    let getChoix3 = inputChoix3.value
    let getChoix4 = inputChoix4.value
    let getPage = $('#summernote').summernote('code');
    if (!livre[id]) {
      livre[id] = {
        id: id,
        titre: getTitre,
        text: getPage,
        choix1: getChoix1,
        choix2: getChoix2,
        choix3: getChoix3,
        choix4: getChoix4,
      }
    } else {
      livre[id].titre = getTitre;
      livre[id].choix1 = getChoix1;
      livre[id].choix2 = getChoix2;
      livre[id].choix3 = getChoix3;
      livre[id].choix4 = getChoix4;
      livre[id].text = $('#summernote').summernote('code');
    }
  });

});