 $(document).ready(function () {

            $(".div-left").click(function () {
                $(".slide-left", this).slideToggle("slow");
            });

            $("#btnLogin").click(function () {
                $("#ModalLogin").modal();
            });

            $("#btnSignUp").click(function () {
                $("#ModalSignUp").modal();
            });

            $("#ModalSignUpToLogin").click(function () {
                $('#ModalSignUp').modal('toggle');
                $("#ModalLogin").modal();
            });

            $("#ModalLoginToSignUp").click(function () {
                $('#ModalLogin').modal('toggle');
                $("#ModalSignUp").modal();
            });

            $(".animer").click(function () {
                $(".main-left").animate({
                    width: 'toggle'
                });
                $(this).find('i').toggleClass('glyphicon-triangle-left').toggleClass(
                    'glyphicon-triangle-right');
            });
        });